﻿using UnityEngine;
using System.Collections;

public class CameraShot : MonoBehaviour
{
    public int count = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            OnShot();
        }
    }

    public void OnShot()
    {
        Application.CaptureScreenshot("Img_"+ count + ".png");
        count++;
    }
}
