﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using admob;

public class ControlManager : MonoBehaviour
{
    public GameObject objGameOver;
    public GameObject objTable;
    public SceneTransition win;
    public SceneTransition lose;

    public Text txtLevel;
    public Text txtPoint;
    public Text txtTarget_1;
    public Text txtTarget_2;
    public Text txtTarget_3;

    private bool check = true;
    private ConfigLevel.Row config;
    private GoogleAdmob admob;
    private void Awake()
    {
        config = GUIManager.instance.configLevel.Find_ID(GUIManager.instance.level.ToString());
        BackgroundMusic music = FindObjectOfType<BackgroundMusic>();
        music.PlayMusic(SoundEffect.INGAME);

        txtLevel.text = GUIManager.instance.level.ToString();
        txtTarget_1.text = "1: " + config.Socre_1.ToString();
        txtTarget_2.text = "2: " + config.Socre_2.ToString();
        txtTarget_3.text = "3: " + config.Socre_3.ToString();

        admob = FindObjectOfType<GoogleAdmob>();
    }

    void Update()
    {
        //Display

        txtPoint.text = GUIManager.instance.point.ToString();

        if (GUIManager.instance.point >= int.Parse(config.Socre_3) && check)
        {
            WinGame();
            check = false;
        }

        //Admob
        admob.OnGoogleAdmob();
    }

    public void Left()
    {
        TetrisObject tetris = FindObjectOfType<TetrisObject>();
        if (tetris != null)
            tetris.OnLeftDown();
    }

    public void LeftUp()
    {
        TetrisObject tetris = FindObjectOfType<TetrisObject>();
        if (tetris != null)
            tetris.OnLeftUp();
    }

    public void Right()
    {
        TetrisObject tetris = FindObjectOfType<TetrisObject>();
        if (tetris != null)
            tetris.OnRightDown();
    }

    public void RightUp()
    {
        TetrisObject tetris = FindObjectOfType<TetrisObject>();
        if (tetris != null)
            tetris.OnRightUp();
    }

    public void Rotate()
    {
        TetrisObject tetris = FindObjectOfType<TetrisObject>();
        if (tetris != null)
            tetris.BtnRotate();
    }

    public void Down()
    {
        TetrisObject tetris = FindObjectOfType<TetrisObject>();
        if (tetris != null)
            tetris.OnDown();
    }

    public void DownUp()
    {
        TetrisObject tetris = FindObjectOfType<TetrisObject>();
        if (tetris != null)
            tetris.OnDownUp();
    }

    public void WinGame()
    {
        GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
        admob.Requestinterstitial();
        win.PerformTransition();
    }

    public void LoseGame()
    {
        GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
        admob.Requestinterstitial();
        lose.PerformTransition();
    }
}
