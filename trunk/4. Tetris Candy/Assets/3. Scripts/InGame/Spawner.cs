﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public GameObject[] tetrisObjects;
    public int count;
    public GameObject[]  objNexts;

	// Use this for initialization
	void Start ()
    {
        count = Random.Range(0, tetrisObjects.Length);
      //  SpawnRandom(UnityEngine.Random.Range(0, tetrisObjects.Length));
        OnNext();

      SpawnRandom(UnityEngine.Random.Range(0, tetrisObjects.Length));
    }
	
	public void SpawnRandom(int index)
    {
        GameObject objTetris = tetrisObjects[index];
        GameObject obj = Instantiate(objTetris, transform.position, Quaternion.identity) as GameObject;
        obj.transform.position = new Vector3(4, 22, 0);
    }

    public void OnNext()
    {
        objNexts[0].SetActive(false);
        objNexts[1].SetActive(false);
        objNexts[2].SetActive(false);
        objNexts[3].SetActive(false);
        objNexts[4].SetActive(false);
        objNexts[5].SetActive(false);
        objNexts[6].SetActive(false);
        int number = count;
        while (count == number)
        {
            number = Random.Range(0, tetrisObjects.Length);
        }
        count = number;
        objNexts[count].SetActive(true);
    }
}
