﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

class SaveLoadManager
{
    public static void SaveLevel(LevelGame levelGame)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/Level.dat", FileMode.Create);

        LevelData data = new LevelData(levelGame);

        bf.Serialize(stream, data);
        stream.Close();
    }

    public static LevelData LoadLevel()
    {
        LevelData data = new LevelData();

        if (File.Exists(Application.persistentDataPath + "/Level.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/Level.dat", FileMode.Open);

            data = bf.Deserialize(stream) as LevelData;

            stream.Close();
            return data;
        }

        return data;
    }
}



[Serializable]
public class LevelData
{
    public int lv;
    public List<int> star = new List<int>();

    public LevelData(LevelGame levelGame)
    {
        lv = levelGame.lv;
        star = levelGame.star;
    }

    public LevelData()
    {
         
    }
}

