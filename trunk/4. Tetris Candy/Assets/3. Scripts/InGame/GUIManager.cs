﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIManager : MonoBehaviour
{
    
    public static GUIManager instance = null;
    public ConfigLevel configLevel;
    public LevelData data;
    public int level;
    public int point;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
           Destroy(gameObject);

        LoadCSV();

        SetInforGame();

        DontDestroyOnLoad(gameObject);
    }

    public void SetInforGame()
    {
        data = SaveLoadManager.LoadLevel();
        level = data.lv;

       /* for (int i = 0; i < 35; i++)
        {
            data.star[i] = 3;
        }
        data.lv = 34;*/
    }

    public void LoadCSV()
    {
        TextAsset configHeroAsset = Resources.Load("Config/ConfigLevel") as TextAsset;
        configLevel = new ConfigLevel();
        configLevel.Load(configHeroAsset);
    }
}
