﻿using UnityEngine;
using System.Collections;

public class MatrixGrid : MonoBehaviour
{
    public int row = 10;
    public int column = 25;

    public Transform[,] grid;

    private void Awake()
    {
        grid = new Transform[row, column];
    }

    public Vector2 RoundVector(Vector2 v)
    {
        return new Vector2(Mathf.Round(v.x), Mathf.Round(v.y));
    }

    public bool IsInsideBorder(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < row && (int)pos.y >= 0);
    }

    public void DeleteRow(int y)
    {
        GUIManager.instance.point++;
        BackgroundMusic music = FindObjectOfType<BackgroundMusic>();
        music.PlayEffice(SoundEffect.HIT);
        for (int x = 0; x < row; ++x)
        {
            GameObject.Destroy(grid[x, y].gameObject);
            grid[x, y] = null;
        }
    }

    public void DecreaseRow(int y)
    {
        for (int x = 0; x < row; ++x)
        {
            if (grid[x, y] != null)
            {
                grid[x, y - 1] = grid[x, y];
                grid[x, y] = null;

                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    public void DecreaseRowsAbove(int y)
    {
        for (int i = y; i < column; ++i)
        {
            DecreaseRow(i);
        }
    }

    public bool IsRowFull(int y)
    {
        for (int x = 0; x < row; ++x)
        {
            if (grid[x, y] == null)
                return false;
        }
        return true;
    }

    public void DeleteWholeRows()
    {
        for (int y = 0; y < column; ++y)
        {
            if (IsRowFull(y))
            {
                DeleteRow(y);
                DecreaseRowsAbove(y + 1);
                --y;
            }
        }
    }

    public bool CheckIsAboveGrid(TetrisObject tetris)
    {
        for (int x = 0; x < row; ++x)
        {
            foreach (Transform item in tetris.transform)
            {
                if (item.name != "Pivot")
                {
                    Vector2 pos = RoundVector(item.position);

                    if (pos.y > 19)
                    {
                        return true;
                    }
                }  
            }
        }

        return false;
    }

} 




















































