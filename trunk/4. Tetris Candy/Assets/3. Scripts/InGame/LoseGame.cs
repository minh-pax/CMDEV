﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoseGame : MonoBehaviour
{
    public Text txtLevel;
    public Text txtScore;
    public Text txtTarget_1;
    public Text txtTarget_2;
    public Text txtTarget_3;

    private ConfigLevel.Row config;
    // Use this for initialization
    void Awake ()
    {
        txtLevel.text = avUIManager.ChangeLanguage("3") + GUIManager.instance.level;
        config = GUIManager.instance.configLevel.Find_ID(GUIManager.instance.level.ToString());
        txtTarget_1.text = config.Socre_1;
        txtTarget_2.text = config.Socre_2;
        txtTarget_3.text = config.Socre_3;
        txtScore.text = GUIManager.instance.point.ToString();

        GUIManager.instance.point = 0;
    }
	
}
