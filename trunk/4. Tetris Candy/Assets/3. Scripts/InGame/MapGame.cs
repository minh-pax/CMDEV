﻿using UnityEngine;
using System.Collections;

public class MapGame : MonoBehaviour
{
    public GameObject objShine;
    public GameObject objGroupLevel;
    public GameObject objContent;
    public Sprite starNormal;
    public Sprite starOld;
    public Sprite starCurrent;

    public Sprite btnNormal;
    public Sprite btnOld;
    public Sprite btnCurrent;

    // Use this for initialization
    void Awake ()
    {
        int index = 0;
        foreach (Transform child in objGroupLevel.transform)
        {
            if (index == GUIManager.instance.data.lv)
            {
                child.GetComponent<LevelHandle>().Init(3, starNormal, starCurrent, btnNormal, btnCurrent, true);
                objShine.transform.SetParent(child);
                objShine.transform.localPosition = Vector3.zero;
                objShine.transform.SetParent(objContent.transform);
            }
            else
            {
                child.GetComponent<LevelHandle>().Init(GUIManager.instance.data.star[index], starNormal, starOld, btnNormal, btnOld, false);
            }
            index++;
        }

        BackgroundMusic music = FindObjectOfType<BackgroundMusic>();
        music.PlayMusic(SoundEffect.MAP);
    }
	
	public void LevelGame(GameObject obj)
    {
        GUIManager.instance.level = int.Parse(obj.name.Split('_')[1]);
        GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
        admob.Requestinterstitial();
    }
}
