﻿using UnityEngine;
using System.Collections;

public class TetrisObject : MonoBehaviour
{
    public Transform rotationPivot;
    public static bool checkDown;
    public static bool checkLeft;
    public static bool checkRight;

    public bool allowRotation = true;
    public bool limitRotation = false;

    private float lastFallLeft = 0;
    private float lastFallRight = 0;
    private float lastFallRotate = 0;
    private ConfigLevel.Row config;
    private MatrixGrid matrixGrid;
    private float speed = 0;

    private void Awake()
    {
        config = GUIManager.instance.configLevel.Find_ID(GUIManager.instance.level.ToString());
        matrixGrid = FindObjectOfType<MatrixGrid>();

        if (GUIManager.instance.level <= 3)
            speed = 8.5f;
        else if (GUIManager.instance.level > 3 && GUIManager.instance.level <= 5)
            speed = 7.5f;
        else if (GUIManager.instance.level > 5 && GUIManager.instance.level <= 10)
            speed = 6f;
        else if (GUIManager.instance.level > 10 && GUIManager.instance.level <= 20)
            speed = 5f;
        else if (GUIManager.instance.level > 20 && GUIManager.instance.level <= 30)
            speed = 4;
        else if (GUIManager.instance.level > 30 && GUIManager.instance.level <= 35)
            speed = 4.2f;
    }

    // Update is called once per frame
    void Update()
    {
        //LEFT
        if (checkLeft)
        {
            if (Time.time - lastFallLeft >= 0.1)
            {
                transform.position += new Vector3(-1, 0, 0);
                if (IsValidGridPosition())
                {
                    UpdateMatrixGrid();
                }
                else
                {
                    transform.localPosition += new Vector3(1, 0, 0);
                }

                lastFallLeft = Time.time;
            }
        }

        //RIGHT
        if (checkRight)
        {
            if (Time.time - lastFallRight >= 0.1)
            {
                transform.localPosition += new Vector3(1, 0, 0);

                if (IsValidGridPosition())
                {
                    UpdateMatrixGrid();
                }
                else
                {
                    transform.localPosition += new Vector3(-1, 0, 0);
                }

                lastFallRight = Time.time;
            }
        }


        //DOWN
        if (checkDown)
        {
            if (Time.time - lastFallRotate >= 1 - (float.Parse(config.Speed) * speed))
            {
                transform.localPosition += new Vector3(0, -1, 0);

                if (IsValidGridPosition())
                {
                    UpdateMatrixGrid();
                }
                else
                {
                    transform.localPosition += new Vector3(0, 1, 0);
                    matrixGrid.DeleteWholeRows();
                    Destroy(gameObject.GetComponent<TetrisObject>());

                    if (matrixGrid.CheckIsAboveGrid(this))
                    {
                        if (GUIManager.instance.point < int.Parse(config.Socre_1))
                        {
                            FindObjectOfType<ControlManager>().LoseGame();
                        }
                        else
                        {
                            FindObjectOfType<ControlManager>().WinGame();
                        }  
                    }
                    else
                    {
                        Spawner spawner = FindObjectOfType<Spawner>();
                        spawner.SpawnRandom(spawner.count);
                        spawner.OnNext();
                    }
                }

                lastFallRotate = Time.time;
            }
        }
        else
        {
            if (Time.time - lastFallRotate >= 1 - (float.Parse(config.Speed) * 2.2f))
            {
                transform.localPosition += new Vector3(0, -1, 0);

                if (IsValidGridPosition())
                {
                    UpdateMatrixGrid();
                }
                else
                {
                   
                    transform.localPosition += new Vector3(0, 1, 0);
                    matrixGrid.DeleteWholeRows();
                    Destroy(gameObject.GetComponent<TetrisObject>());

                    if (matrixGrid.CheckIsAboveGrid(this))
                    {
                        
                        if (GUIManager.instance.point < int.Parse(config.Socre_1))
                        {
                            FindObjectOfType<ControlManager>().LoseGame();
                        }
                        else
                        {
                            FindObjectOfType<ControlManager>().WinGame();
                        }
                    }
                    else
                    {
                        Spawner spawner = FindObjectOfType<Spawner>();
                        spawner.SpawnRandom(spawner.count);
                        spawner.OnNext();
                    }
                }

                lastFallRotate = Time.time;
            }
        }
    }

    public void BtnRotate()
    {
        if (allowRotation)
        {
            if (limitRotation)
            {
                if (transform.rotation.eulerAngles.z >= 90)
                {
                    transform.RotateAround(rotationPivot.position, Vector3.forward, -90);
                }
                else
                {
                    transform.RotateAround(rotationPivot.position, Vector3.forward, 90);
                }
            }
            else
            {
                transform.RotateAround(rotationPivot.position, Vector3.forward, 90);
            }

            if (IsValidGridPosition())
            {
                UpdateMatrixGrid();
            }
            else
            {
                if (limitRotation)
                {
                    if (transform.rotation.eulerAngles.z >= 90)
                    {
                        transform.RotateAround(rotationPivot.position, Vector3.forward, -90);
                    }
                    else
                    {
                        transform.RotateAround(rotationPivot.position, Vector3.forward, 90);
                    }
                }
                else
                {
                    transform.RotateAround(rotationPivot.position, Vector3.forward, -90);
                }
            }
        }
    }

    public void OnDown()
    {
        checkDown = true;
    }
    public void OnDownUp()
    {
        checkDown = false;
    }

    public void OnLeftDown()
    {
        checkLeft = true;
    }
    public void OnLeftUp()
    {
        checkLeft = false;
    }

    public void OnRightDown()
    {
        checkRight = true;
    }
    public void OnRightUp()
    {
        checkRight = false;
    }

    bool IsValidGridPosition()
    {
        foreach (Transform child in transform)
        {
            if (child.name != "Pivot")
            {
                Vector2 v = matrixGrid.RoundVector(child.position);

                if (!matrixGrid.IsInsideBorder(v))
                    return false;

                if (matrixGrid.grid[(int)v.x, (int)v.y] != null && matrixGrid.grid[(int)v.x, (int)v.y].parent != transform)
                    return false;
            }
        }
        return true;
    }

    void UpdateMatrixGrid()
    {
        for (int y = 0; y < matrixGrid.column; ++y)
        {
            for (int x = 0; x < matrixGrid.row; ++x)
            {
                if (matrixGrid.grid[x, y] != null)
                {
                    if (matrixGrid.grid[x, y].parent == transform)
                    {
                        matrixGrid.grid[x, y] = null;
                    }
                }
            }
        }

        foreach (Transform child in transform)
        {
            if (child.name != "Pivot")
            {
                Vector2 v = matrixGrid.RoundVector(child.position);
                matrixGrid.grid[(int)v.x, (int)v.y] = child;
            }   
        }
    }
}
