﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Option : MonoBehaviour
{
    public Toggle onLanguage, offLanguage;

    // Use this for initialization
    void Start()
    {
        SetOption();
    }

    public void SetOption()
    {
        if (PlayerPrefs.GetString("Language") == "English")
        {
            onLanguage.isOn = true;
            offLanguage.isOn = false;
        }
        else
        {
            onLanguage.isOn = false;
            offLanguage.isOn = true;
        }
    }

    public void OnLanguage(GameObject obj)
    {
        if (obj.name == "English" && obj.GetComponent<Toggle>().isOn == true)
        {
            PlayerPrefs.SetString("Language", "English");
        }
        else if (obj.name == "VietNam" && obj.GetComponent<Toggle>().isOn == true)
        {
            PlayerPrefs.SetString("Language", "VietNam");
        }
    }
}
