﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum Movement
{
    Left,
    Right,
    Up,
    Down
};

public class MatrixGrid : MonoBehaviour
{
    private int row = 10;
    private int column = 10;
    public bool checkButton = true;
    public GameObject[,] grid;

    private void Awake()
    {
        grid = new GameObject[row, column];
    }

    public bool CheckSpawner(GameObject pos, GameObject target)
    {
        string[] name = pos.name.Split('_');
        int x = int.Parse(name[0]);
        int y = int.Parse(name[1]);
        bool check = true;

        //Check Up
        if (y - 1 >= 0)
        {
            if (grid[x, y - 1] != null)
            {
                if (grid[x, y - 1].transform.childCount > 0)
                {
                    if (grid[x, y - 1].transform.GetChild(0).name == target.name)
                    {
                        if (y - 2 >= 0)
                        {
                            if (grid[x, y - 2].transform.childCount > 0)
                            {
                                if (grid[x, y - 2].transform.GetChild(0).name == target.name)
                                {
                                    check = false;
                                }
                            }
                           
                        }
                    }
                }   
            }
        }

        //Check Down
        if (y + 1 < column)
        {
            if (grid[x, y + 1] != null)
            {
                if (grid[x, y + 1].transform.childCount > 0)
                {
                    if (grid[x, y + 1].transform.GetChild(0).name == target.name)
                    {
                        if (y + 2 < column)
                        {
                            if (grid[x, y + 1].transform.childCount > 0)
                            {
                                if (grid[x, y + 2].transform.GetChild(0).name == target.name)
                                {
                                    check = false;
                                }
                            }
                        }
                    }
                }     
            }
        }

        //Check Right
        if (x + 1 < row)
        {
            if (grid[x + 1, y] != null)
            {
                if (grid[x + 1, y].transform.childCount > 0)
                {
                    if (grid[x + 1, y].transform.GetChild(0).name == target.name)
                    {
                        if (x + 2 < row)
                        {
                            if (grid[x + 2, y].transform.childCount > 0)
                            {
                                if (grid[x + 2, y].transform.GetChild(0).name == target.name)
                                {
                                    check = false;
                                }
                            }
                        }
                    }
                }
                
            }
        }

        //Check Left
        if (x - 1 >= 0)
        {
            if (grid[x - 1, y] != null)
            {
                if (grid[x - 1, y].transform.childCount > 0)
                {
                    if (grid[x - 1, y].transform.GetChild(0).name == target.name)
                    {
                        if (x - 2 >= 0)
                        {
                            if (grid[x - 2, y].transform.childCount > 0)
                            {
                                if (grid[x - 2, y].transform.GetChild(0).name == target.name)
                                {
                                    check = false;
                                }
                            }
                        }
                    }
                }
               
            }
        }

        return check;
    }

    public IEnumerator OnItweenReturn(GameObject change_1, GameObject change_2, Transform normal, Transform current)
    {
        yield return new WaitForSeconds(0.5f);

        change_1.transform.SetParent(normal.transform);
        iTween.MoveTo(change_1, normal.transform.position, avUIManager.timeMoveTo);

        change_2.transform.SetParent(current.transform);
        iTween.MoveTo(change_2, current.transform.position, avUIManager.timeMoveTo);

        checkButton = true;
    }

    public List<GameObject> CheckHit(GameObject pos, GameObject target)
    {
        string[] name = pos.name.Split('_');
        int x = int.Parse(name[0]);
        int y = int.Parse(name[1]);

        List<GameObject> listHit = new List<GameObject>();
        List<GameObject> listTarget = new List<GameObject>();

        int count = 0;

        //Kiem tra hang
        for (int i = 0; i < 10; i++)
        {
            if (grid[i, y].transform.childCount > 0)
            {
                if (grid[i, y].transform.GetChild(0).name == target.name)
                {
                    listTarget.Add(grid[i, y]);
                    if (i == 9)
                    {
                        if (listTarget.Count >= 3)
                        {
                            foreach (GameObject item in listTarget)
                            {
                                listHit.Add(item);
                            }

                            StartCoroutine(OnWaitSound());
                            count++;
                            listTarget.Clear();
                        }
                    }
                }
                else
                {
                    if (listTarget.Count >= 3)
                    {
                        foreach (GameObject item in listTarget)
                        {
                            listHit.Add(item);
                        }

                        StartCoroutine(OnWaitSound());
                        count++;
                    }

                    listTarget.Clear();
                }
            }  
        }

        listTarget.Clear();

        //Kiem tra cot
        for (int i = 0; i < 10; i++)
        {
            if (grid[x, i].transform.childCount > 0)
            {
                if (grid[x, i].transform.GetChild(0).name == target.name)
                {
                    listTarget.Add(grid[x, i]);
                    if (i == 9)
                    {
                        if (listTarget.Count >= 3)
                        {
                            foreach (GameObject item in listTarget)
                            {
                                listHit.Add(item);
                            }

                            StartCoroutine(OnWaitSound());
                            count++;
                            listTarget.Clear();
                        }
                    }
                }
                else
                {
                    if (listTarget.Count >= 3)
                    {
                        foreach (GameObject item in listTarget)
                        {
                            listHit.Add(item);
                        }

                        StartCoroutine(OnWaitSound());
                        count++;
                    }

                    listTarget.Clear();
                }
            }
        }

        if (listHit.Count > 0)
        {
            int from = GUIManager.instance.point;
            int to = GUIManager.instance.point + count;

            Hashtable ht = new Hashtable();
            ht.Add("from", from);
            ht.Add("to", to);
            ht.Add("time", .5f);
            ht.Add("onupdate", "OnUpdatePoint");
            iTween.ValueTo(gameObject, ht);
            iTween.PunchScale(FindObjectOfType<ControlManager>().txtPoint.gameObject, new Vector3(1.5f, 1.5f, 1.5f), 1);

            listHit = listHit.Distinct().ToList();
            StartCoroutine(OnItweenMove(listHit));
        }

        return listHit;
    }

    IEnumerator OnItweenMove(List<GameObject> listHit)
    {
        yield return new WaitForSeconds(0.5f);

        foreach (GameObject item in listHit)
        {
            if (item.transform.childCount > 0)
             iTween.PunchScale(item.transform.GetChild(0).gameObject, new Vector3(1.5f, 1.5f, 1.5f), avUIManager.timePunchScale);
        }

        if (listHit.Count > 0)
        {
            StartCoroutine(OnRemoveItem(listHit));
        }
    }

    void OnUpdatePoint(float newValue)
    {
        GUIManager.instance.point = (int) newValue;
    }

    IEnumerator OnWaitSound()
    {
        yield return new WaitForSeconds(0.5f);

        BackgroundMusic music = FindObjectOfType<BackgroundMusic>();
        music.PlayEffice(SoundEffect.HIT);
    }
    IEnumerator OnRemoveItem(List<GameObject> listHit)
    {
        yield return new WaitForSeconds(0.5f);

        List<int> listX = new List<int>();
        
        foreach (GameObject item in listHit)
        {
            if (item.transform.childCount > 0)
            {
                string[] name = item.name.Split('_');
                int x = int.Parse(name[0]);
                listX.Add(x);

                Destroy(item.transform.GetChild(0).gameObject);
            }
        }

        StartCoroutine(OnUpItem(listX));
    }

    IEnumerator OnUpItem(List<int> listX)
    {
        yield return new WaitForSeconds(0.2f);

        List<GameObject> listHit = new List<GameObject>();
        bool checkFull = true;
        int count = 0;
        listX = listX.Distinct().ToList();

        //Move cuoi
        foreach (int x in listX)
        {
            checkFull = true;
            count = 0;

            for (int i = 9; i >= 0; i--)
            {
                if (grid[x, i].transform.childCount > 0)
                {
                    if (checkFull && count <= 0)
                    {
                        checkFull = false;
                    }
                    else
                    {
                        GameObject obj = grid[x, i].transform.GetChild(0).gameObject;

                        obj.transform.SetParent(grid[x, i + count].transform);
                        iTween.MoveTo(obj, grid[x, i + count].transform.position, avUIManager.timeMoveTo);
                        listHit.Add(grid[x, i + count]);
                        checkFull = false;
                    }
                }
                else
                {
                    count++;
                }
            }
        }

        StartCoroutine(SpawnItem(listX, listHit));
    }

    public IEnumerator SpawnItem(List<int> listX, List<GameObject> listHit)
    {
        yield return new WaitForSeconds(0.3f);

        List<GameObject> listCandy = new List<GameObject>();

        foreach (int x in listX)
        {
            for (int i = 9; i >= 0; i--)
            {
                if (grid[x, i].transform.childCount == 0)
                {
                    listCandy.Add(grid[x, i]);
                }
            }
        }

        foreach (GameObject item in listCandy)
        {
            GameObject[] objCandys = FindObjectOfType<Spawner>().objCandys;

            GameObject objCandy = objCandys[Random.Range(0, objCandys.Length)];

            while (!CheckSpawner(item.gameObject, objCandy))
            {
                objCandy = objCandys[Random.Range(0, objCandys.Length)];
            }

            GameObject obj = Instantiate(objCandy, item.transform) as GameObject;
            obj.transform.SetParent(item.transform); 
            obj.transform.localPosition = Vector3.zero;
            obj.name = objCandy.name;

            if (FindObjectOfType<Spawner>().numRandom == 1)
                obj.transform.localScale = new Vector3(0.85f, 0.85f, 0.85f);
            else if (FindObjectOfType<Spawner>().numRandom == 3)
                obj.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            else if (FindObjectOfType<Spawner>().numRandom == 4 || FindObjectOfType<Spawner>().numRandom == 6)
                obj.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            else if (FindObjectOfType<Spawner>().numRandom == 5)
                obj.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            else
                obj.transform.localScale = Vector3.one;

            iTween.PunchScale(obj, new Vector3(1.5f, 1.5f, 1.5f), avUIManager.timeMoveTo);

            listHit.Add(item);
        }

        foreach (GameObject item in listHit)
        {
            CheckHit(item, item.transform.GetChild(0).gameObject);
        }

        checkButton = true;
        StartCoroutine(OnCheckCandy());
    }

    IEnumerator OnCheckCandy()
    {
        yield return new WaitForSeconds(0.3f);

        foreach (Transform child in FindObjectOfType<Spawner>().group)
        {
            if (child.childCount >= 2)
            {
                Destroy(child.GetChild(1).gameObject);
            }
        }
    }
} 