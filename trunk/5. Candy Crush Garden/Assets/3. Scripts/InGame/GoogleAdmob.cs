﻿using UnityEngine;
using System.Collections;
using admob;
using System;

public class GoogleAdmob : MonoBehaviour
{
	public Admob ad;
	// Use this for initialization
	void Awake()
	{
		RequestBanner();
	}

	void Update()
	{
		OnGoogleAdmob ();
	}

	public void RequestBanner()
	{
		ad = Admob.Instance ();
		ad.initAdmob ("ca-app-pub-3373095007217675/2280728742", "ca-app-pub-3373095007217675/9978792341");
		Admob.Instance ().showBannerRelative (AdSize.SmartBanner, AdPosition.TOP_CENTER, 0);
	}
		
	public void Requestinterstitial()
	{
		ad.loadInterstitial();
	}
		
	public void OnGoogleAdmob()
	{
		if (ad != null)
		{
			if (ad.isInterstitialReady())
			{
				ad.showInterstitial();
			}
		}
	}

	void onInterstitialEvent(string eventName, string msg)
	{
		Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
	}
	void onBannerEvent(string eventName, string msg)
	{
		Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
	}
}
