﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundEffect
{
    MENU,
    MAP,
    INGAME,
    HIT,
}

public class LevelGame
{
    public int lv;
    public List<int> star = new List<int>();
}

public static class avUIManager
{
    public static float timeMoveTo = 0.5f;
    public static float timePunchScale = 0.5f;

    public static ConfigString_Normal LoadString_Normal()
    {
        TextAsset config = Resources.Load("Config/ConfigString_Normal") as TextAsset;
        ConfigString_Normal csv = new ConfigString_Normal();
        csv.Load(config);

        return csv;
    }

    public static string ChangeLanguage(string stringID)
    {
        string text = "";
        string language = PlayerPrefs.GetString("Language");

        switch (language)
        {
            case "English":
                text = avUIManager.LoadString_Normal().Find_ID(stringID).EN;
                break;
            case "VietNam":
                text = avUIManager.LoadString_Normal().Find_ID(stringID).VN;
                break;
            default:
                text = avUIManager.LoadString_Normal().Find_ID(stringID).EN;
                break;
        }

        return text;
    }


}
