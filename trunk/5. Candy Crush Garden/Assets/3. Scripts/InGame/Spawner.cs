﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    public GameObject[] objCandy_1;
    public GameObject[] objCandy_2;
    public GameObject[] objCandy_3;
    public GameObject[] objCandy_4;
    public GameObject[] objCandy_5;
    public GameObject[] objCandy_6;
    public GameObject[] objCandy_7;

    public GameObject[] objCandys;
    public Transform group;

    private MatrixGrid matrixGrid;
    private GameObject objTarget;
    private bool press = false;
    private Vector2 fingerStart;
    private Vector2 fingerEnd;
    private GameObject change_1;
    private GameObject change_2;
    private bool checkButton = true;
    private int numButton = 0;
    public int numRandom = 0;
    // Use this for initialization
    void Start ()
    {
        matrixGrid = FindObjectOfType<MatrixGrid>();

        objCandys = OnRandomCandyObject();

        Spawn(group);
    }

    public void Spawn(Transform trans)
    {
         foreach (Transform child in trans)
         {
             string[] name = child.gameObject.name.Split('_');
             int x = int.Parse(name[0]);
             int y = int.Parse(name[1]);

             GameObject objCandy = objCandys[Random.Range(0, objCandys.Length)];

             while (!matrixGrid.CheckSpawner(child.gameObject, objCandy))
             {
                 objCandy = objCandys[Random.Range(0, objCandys.Length)];
             }

            GameObject obj = Instantiate(objCandy, child) as GameObject;
            obj.transform.SetParent(child);
            obj.transform.localPosition = Vector3.zero;
            obj.name = objCandy.name;

            if (numRandom == 1)
                obj.transform.localScale = new Vector3(0.85f, 0.85f, 0.85f);
            else if (numRandom == 3)
                obj.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            else if (numRandom == 4)
                obj.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            else if (numRandom == 5)
                obj.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            else if (numRandom == 6)
                obj.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            else
                obj.transform.localScale = Vector3.one;

            matrixGrid.grid[x, y] = child.gameObject;
         }
    }

    void Update()
    {
        if (press)
        {
            OnMouseEvent();
        }
    }

    public void OnMouseEvent()
    {
        int tolerance = 0;
        fingerEnd = Input.mousePosition;

        if (Mathf.Abs(fingerEnd.x - fingerStart.x) > tolerance ||
           Mathf.Abs(fingerEnd.y - fingerStart.y) > tolerance)
        {

            if (Mathf.Abs(fingerStart.x - fingerEnd.x) > Mathf.Abs(fingerStart.y - fingerEnd.y))
            {
                //Right Swipe
                if ((fingerEnd.x - fingerStart.x) > 0)
                    OnRight();
                //Left Swipe
                else
                    OnLeft();
            }
            else
            {
                //Upward Swipe
                if ((fingerEnd.y - fingerStart.y) > 0)
                    OnUp();
                //Downward Swipe
                else
                    OnDown();
            }

            fingerStart = fingerEnd;
        }
    }

    public void OnTouchEvent()
    {
        foreach (Touch touch in Input.touches)
        {

            if (touch.phase == TouchPhase.Began)
            {
                fingerStart = touch.position;
                fingerEnd = touch.position;
                press = true;
            }

            if (touch.phase == TouchPhase.Moved)
            {
                fingerEnd = touch.position;

                //There is more movement on the X axis than the Y axis
                if (Mathf.Abs(fingerStart.x - fingerEnd.x) > Mathf.Abs(fingerStart.y - fingerEnd.y))
                {

                    //Right Swipe
                    if ((fingerEnd.x - fingerStart.x) > 0)
                        OnRight();
                    //Left Swipe
                    else
                        OnLeft();

                }

                //More movement along the Y axis than the X axis
                else
                {
                    //Upward Swipe
                    if ((fingerEnd.y - fingerStart.y) > 0)
                        OnUp();
                    //Downward Swipe
                    else
                        OnDown();
                }

                fingerStart = touch.position;
            }


            if (touch.phase == TouchPhase.Ended)
            {
                fingerStart = Vector2.zero;
                fingerEnd = Vector2.zero;
                press = false;
                objTarget = null;
            }
        }
    }

    public void OnUp()
    {
        if (checkButton)
        {
            if (objTarget != null)
            {
                string[] name = objTarget.name.Split('_');
                int x = int.Parse(name[0]);
                int y = int.Parse(name[1]);

                if (y - 1 >= 0)
                {
                    //Add
                    change_1 = matrixGrid.grid[x, y].transform.GetChild(0).gameObject;
                    change_2 = matrixGrid.grid[x, y - 1].transform.GetChild(0).gameObject;

                    //Change
                    change_1.transform.SetParent(matrixGrid.grid[x, y - 1].transform);
                    iTween.MoveTo(change_1, matrixGrid.grid[x, y - 1].transform.position, avUIManager.timeMoveTo);
                    
                    change_2.transform.SetParent(matrixGrid.grid[x, y].transform);
                    iTween.MoveTo(change_2, matrixGrid.grid[x, y].transform.position, avUIManager.timeMoveTo);

                    List<GameObject> hit_1 = matrixGrid.CheckHit(matrixGrid.grid[x, y - 1], change_1);
                    List<GameObject> hit_2 = matrixGrid.CheckHit(matrixGrid.grid[x, y], change_2);

                    //Neu di chuyen khong an duoc thi tra ve vi tri cu
                    if (hit_1.Count <= 0 && hit_2.Count <= 0)
                    {
                        StartCoroutine(matrixGrid.OnItweenReturn(change_2, change_1, matrixGrid.grid[x, y - 1].transform, matrixGrid.grid[x, y].transform));
                    }
                }
            }

            checkButton = false;
        } 
    }

    public void OnDown()
    {
        if (checkButton)
        {
            if (objTarget != null)
            {
                string[] name = objTarget.name.Split('_');
                int x = int.Parse(name[0]);
                int y = int.Parse(name[1]);
                if (y + 1 < 10)
                {
                    //Add
                    change_1 = matrixGrid.grid[x, y].transform.GetChild(0).gameObject;
                    change_2 = matrixGrid.grid[x, y + 1].transform.GetChild(0).gameObject;

                    //Change
                    change_1.transform.SetParent(matrixGrid.grid[x, y + 1].transform);
                    iTween.MoveTo(change_1, matrixGrid.grid[x, y + 1].transform.position, avUIManager.timeMoveTo);

                    change_2.transform.SetParent(matrixGrid.grid[x, y].transform);
                    iTween.MoveTo(change_2, matrixGrid.grid[x, y].transform.position, avUIManager.timeMoveTo);

                    List<GameObject> hit_1 = matrixGrid.CheckHit(matrixGrid.grid[x, y + 1], change_1);
                    List<GameObject> hit_2 = matrixGrid.CheckHit(matrixGrid.grid[x, y], change_2);

                    //Neu di chuyen khong an duoc thi tra ve vi tri cu
                    if (hit_1.Count <= 0 && hit_2.Count <= 0)
                    {
                        StartCoroutine(matrixGrid.OnItweenReturn(change_2, change_1, matrixGrid.grid[x, y + 1].transform, matrixGrid.grid[x, y].transform));
                    }
                }
            }

            checkButton = false;
        }
    }

    public void OnRight()
    {
        if (checkButton)
        {
            if (objTarget != null)
            {
                string[] name = objTarget.name.Split('_');
                int x = int.Parse(name[0]);
                int y = int.Parse(name[1]);
                if (x + 1 < 10)
                {
                    //Add
                    change_1 = matrixGrid.grid[x, y].transform.GetChild(0).gameObject;
                    change_2 = matrixGrid.grid[x + 1, y].transform.GetChild(0).gameObject;

                    //Change
                    change_1.transform.SetParent(matrixGrid.grid[x + 1, y].transform);
                    iTween.MoveTo(change_1, matrixGrid.grid[x + 1, y].transform.position, avUIManager.timeMoveTo);

                    change_2.transform.SetParent(matrixGrid.grid[x, y].transform);
                    iTween.MoveTo(change_2, matrixGrid.grid[x, y].transform.position, avUIManager.timeMoveTo);

                    List<GameObject> hit_1 = matrixGrid.CheckHit(matrixGrid.grid[x + 1, y], change_1);
                    List<GameObject> hit_2 = matrixGrid.CheckHit(matrixGrid.grid[x, y], change_2);

                    //Neu di chuyen khong an duoc thi tra ve vi tri cu
                    if (hit_1.Count <= 0 && hit_2.Count <= 0)
                    {
                        StartCoroutine(matrixGrid.OnItweenReturn(change_2, change_1, matrixGrid.grid[x + 1, y].transform, matrixGrid.grid[x, y].transform));
                    }
                }
            }

            checkButton = false;
        }
    }

    public void OnLeft()
    {
        if (checkButton)
        {
            if (objTarget != null)
            {
                string[] name = objTarget.name.Split('_');
                int x = int.Parse(name[0]);
                int y = int.Parse(name[1]);
                if (x - 1 >= 0)
                {
                    //Add
                    change_1 = matrixGrid.grid[x, y].transform.GetChild(0).gameObject;
                    change_2 = matrixGrid.grid[x - 1, y].transform.GetChild(0).gameObject;

                    //Change
                    change_1.transform.SetParent(matrixGrid.grid[x - 1, y].transform);
                    iTween.MoveTo(change_1, matrixGrid.grid[x - 1, y].transform.position, avUIManager.timeMoveTo);

                    change_2.transform.SetParent(matrixGrid.grid[x, y].transform);
                    iTween.MoveTo(change_2, matrixGrid.grid[x, y].transform.position, avUIManager.timeMoveTo);

                    List<GameObject> hit_1 = matrixGrid.CheckHit(matrixGrid.grid[x - 1, y], change_1);
                    List<GameObject> hit_2 = matrixGrid.CheckHit(matrixGrid.grid[x, y], change_2);

                    //Neu di chuyen khong an duoc thi tra ve vi tri cu
                    if (hit_1.Count <= 0 && hit_2.Count <= 0)
                    {
                        StartCoroutine(matrixGrid.OnItweenReturn(change_2, change_1, matrixGrid.grid[x - 1, y].transform, matrixGrid.grid[x, y].transform));
                    }
                }
            }

            checkButton = false;
        }
    }

    public void OnButtonDown(GameObject obj)
    {
        numButton++;

        if (numButton >= 4)
        {
            matrixGrid.checkButton = true;
            numButton = 0;
        }

        if (matrixGrid.checkButton)
        {
            objTarget = obj;
            fingerStart = Input.mousePosition;
            fingerEnd = Input.mousePosition;
            press = true;
            matrixGrid.checkButton = false;
        }
    }

    public void OnButtonUp()
    {
        fingerStart = Vector2.zero;
        fingerEnd = Vector2.zero;
        press = false;
        checkButton = true;
        change_1 = null;
        change_2 = null;
        objTarget = null;
    }

    public GameObject[] OnRandomCandyObject()
    {
        GameObject[] objCandy = objCandy_1;

        numRandom = Random.Range(1, 7);
        switch (numRandom)
        {
            case 1:
                objCandy = objCandy_1;
                break;
            case 2:
                objCandy = objCandy_2;
                break;
            case 3:
                objCandy = objCandy_3;
                break;
            case 4:
                objCandy = objCandy_4;
                break;
            case 5:
                objCandy = objCandy_5;
                break;
            case 6:
                objCandy = objCandy_6;
                break;
            case 7:
                objCandy = objCandy_7;
                break;
        }

        return objCandy;
    }
}
