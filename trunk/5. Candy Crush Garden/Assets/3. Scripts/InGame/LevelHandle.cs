﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelHandle : MonoBehaviour
{
    public AnimatedButton btn;
    public Animator animator;
    public Image imgBG;
    public GameObject objLevel;
    public GameObject objStar;

    public void Init(int count, Sprite starNormal, Sprite starCurrent, Sprite btnNormal, Sprite btnCurrent, bool check)
    {
       
        if (count == 0)
        {
            objStar.SetActive(false);
            objLevel.SetActive(false);
            btn.enabled = false;
            imgBG.sprite = btnNormal;
        }
       else
        {
            imgBG.sprite = btnCurrent;
            btn.enabled = true;
            int index = 0;
            foreach (Transform item in objStar.transform)
            {
                index++;
                if (index <= count)
                {
                    item.GetComponent<Image>().sprite = starCurrent;
                }
                else
                {
                    item.GetComponent<Image>().sprite = starNormal;   
                }
            }
        }

        animator.enabled = check;
    }
}
