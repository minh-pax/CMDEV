﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlManager : MonoBehaviour
{
    public GameObject btnNext;

    public SceneTransition win;
    public SceneTransition lose;

    public Text txtLevel;
    public Text txtPoint;
    public Text txtTarget_1;
    public Text txtTarget_2;
    public Text txtTarget_3;

    private bool check = true;
    private bool checkButton = true;
    private ConfigLevel.Row config;

    private void Awake()
    {
        config = GUIManager.instance.configLevel.Find_ID(GUIManager.instance.level.ToString());
        BackgroundMusic music = FindObjectOfType<BackgroundMusic>();
        music.PlayMusic(SoundEffect.INGAME);

        txtLevel.text = GUIManager.instance.level.ToString();
        txtTarget_1.text = config.Socre_1.ToString();
        txtTarget_2.text = config.Socre_2.ToString();
        txtTarget_3.text = config.Socre_3.ToString();
    }

    void Update()
    {
        //Display

        txtPoint.text = GUIManager.instance.point.ToString();

        if (GUIManager.instance.point >= int.Parse(config.Socre_1) && checkButton)
        {
            btnNext.SetActive(true);
            checkButton = false;
        }

        if (GUIManager.instance.point >= int.Parse(config.Socre_3) && check)
        {
            WinGame();
            check = false;
        }
    }

    public void WinGame()
    {
		GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
		admob.Requestinterstitial();
        win.PerformTransition();
    }

    public void LoseGame()
    {
		GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
		admob.Requestinterstitial();
        lose.PerformTransition();
    }
}
