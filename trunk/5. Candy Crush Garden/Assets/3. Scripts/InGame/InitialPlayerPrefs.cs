// Copyright (C) 2015, 2016 ricimi - All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement.
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

// Utility class to force the music and sound effects to be enabled on first launch.
public class InitialPlayerPrefs : MonoBehaviour
{
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("music_on"))
        {
            PlayerPrefs.SetInt("music_on", 1);
        }
        
        if (!PlayerPrefs.HasKey("sound_on"))
        {
            PlayerPrefs.SetInt("sound_on", 1);
        }

        if (!PlayerPrefs.HasKey("Language"))
        {
            PlayerPrefs.SetString("Language", "English");
        }

        if (!PlayerPrefs.HasKey("Level"))
        {
            LevelGame data = new LevelGame();
            data.lv = 0;
            data.star = new List<int>();
            for (int i = 0; i < 35; i++)
            {
                data.star.Add(0);
            }
            SaveLoadManager.SaveLevel(data);
            PlayerPrefs.SetInt("Level", 1);
        }

        BackgroundMusic music = FindObjectOfType<BackgroundMusic>();

        music.PlayMusic(SoundEffect.MENU);
    }
}
