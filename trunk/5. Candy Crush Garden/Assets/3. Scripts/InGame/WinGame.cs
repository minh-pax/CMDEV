﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinGame : MonoBehaviour
{

    public Text txtLevel;
    public Text txtScore;
    public Text txtTarget_1;
    public Text txtTarget_2;
    public Text txtTarget_3;
    public GameObject icon_2;
    public GameObject icon_3;
    public GameObject iconFail_2;
    public GameObject iconFail_3;
    public GameObject star_1;
    public GameObject star_2;
    public GameObject star_3;

    private ConfigLevel.Row config;
    // Use this for initialization
    void Awake()
    {
        txtLevel.text = avUIManager.ChangeLanguage("3") + GUIManager.instance.level;
        config = GUIManager.instance.configLevel.Find_ID(GUIManager.instance.level.ToString());
        txtTarget_1.text = config.Socre_1;
        txtTarget_2.text = config.Socre_2;
        txtTarget_3.text = config.Socre_3;
        txtScore.text = GUIManager.instance.point.ToString();

        int count = 1;

        star_1.SetActive(true);

        if (GUIManager.instance.point >= int.Parse(config.Socre_2))
        {
            star_2.SetActive(true);
            icon_2.SetActive(true);
            count++;
        }
        else
            iconFail_2.SetActive(true);

        if (GUIManager.instance.point >= int.Parse(config.Socre_3))
        {
            star_3.SetActive(true);
            icon_3.SetActive(true);
            count++;
        }
        else
            iconFail_3.SetActive(true);

        GUIManager.instance.point = 0;

        if (GUIManager.instance.level > GUIManager.instance.data.lv)
            GUIManager.instance.data.lv = GUIManager.instance.level;
        if (count > GUIManager.instance.data.star[GUIManager.instance.level - 1])
            GUIManager.instance.data.star[GUIManager.instance.level - 1] = count;

        LevelGame lvGame = new LevelGame();
        lvGame.lv = GUIManager.instance.data.lv;
        lvGame.star = GUIManager.instance.data.star;

        SaveLoadManager.SaveLevel(lvGame);
    }

    public void OnNext()
    {
        GUIManager.instance.level += 1;
    }
}
