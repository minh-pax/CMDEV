﻿using UnityEditor;

public class AssetBundles : Editor {

    [MenuItem("Assets/Build AssetBundle")]
    static void BuildAssetBundle()
    {
        BuildPipeline.BuildAssetBundles("AssetBundles", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }
}
