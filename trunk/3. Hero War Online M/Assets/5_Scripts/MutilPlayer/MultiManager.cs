﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MultiManager : MonoBehaviour {

    public GameObject img_Hero;
    public GameObject groupIcon;
    public MenuManager menuManager;
    public PlayerUI playerUI, enemyUI;
    public Tower towerPlayer;
    public Tower towerEnemy;
    public MoneyInGame moneyInGame;
    public Factions factions;
    public GameObject[] lane;
    public List<GameObject> listHero;
    public List<bool> listCheckSlot;

    private bool press = false;
    private bool createImage = false;
    private GameObject createObj; 
    private GameObject objHero;
    private SelectHero selectHero;

    
    void OnApplicationfocus(bool pauseStatus)
    {
        if (pauseStatus)
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("35"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, res =>
            {
                menuManager.controlMulti.LeaveMatchInGame();
                if (res == MessageBox.DialogResult.Ok)
                {
                    menuManager.OpenResetMenu();
                    SceneManager.LoadScene("Menu");
                }

                return true;
            });
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        { 
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("35"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, res =>
            {
                menuManager.controlMulti.LeaveMatchInGame();
                if (res == MessageBox.DialogResult.Ok)
                {
                    menuManager.OpenResetMenu();
                    SceneManager.LoadScene("Menu");
                }

                return true;
            });
        }
    }
    
    public void InitMultiPlayerGame()
    {
        selectHero = menuManager.selectHero;
        GameObject obj = GameObject.Find("Model");
        GameObject objMapGame = GameObject.Find("MapGame");

        for (int i = 0; i < 4; i++)
        {
            lane[i] = obj.transform.GetChild(i).gameObject;
        }

        towerPlayer = obj.transform.Find("Tower_Player").GetComponent<Tower>();
        towerEnemy = obj.transform.Find("Tower_Enemy").GetComponent<Tower>();

        objMapGame.GetComponent<MapGame>().StartRandom(int.Parse(menuManager.controlMulti.dataPlayer["random"]));

        listCheckSlot = new List<bool>();
        listHero = new List<GameObject>();

        for (int i = 0; i < 6; i++)
        {
            bool check = false;
            listCheckSlot.Add(check);
        }

        for (int i = 0; i < int.Parse(selectHero.lvNormalTower.Slot); i++)
        {
            ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();
            item.InitIcon(false, "");
        }

        for (int i = 0; i < selectHero.listIcon.Count; i++)
        {
            ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();
            item.InitIcon(selectHero.listIcon[i], false, AvUIManager.OnHero(selectHero.listIcon[i].name.Split('_')[3]).Price);
            listCheckSlot[i] = false;
        }

        towerPlayer.InitInfoTower(menuManager.controlMulti.dataPlayer["tower"]);
        towerEnemy.InitInfoTower(menuManager.controlMulti.dataEnemy["tower"]);

        playerUI.playerName.text = menuManager.controlMulti.dataPlayer["name"];
        playerUI.txtHealth.text = towerPlayer.tower.HP + "/" + towerPlayer.maxHealth;
        
        enemyUI.playerName.text = menuManager.controlMulti.dataEnemy["name"];
        enemyUI.txtHealth.text = towerEnemy.tower.HP + "/" + towerEnemy.maxHealth;

        moneyInGame.Init(selectHero.inforGame.lv.ToString(), towerPlayer.tower.Level);

        listHero.Add(towerPlayer.gameObject);
        listHero.Add(towerEnemy.gameObject);
    }

    public void RemoveData()
    {
        menuManager.selectHero.ClearIconHero();
        listHero.Clear();
    }

    void Update()
    {
        DragIconChar();
        OnOffIcon();
        playerUI.Init();
        enemyUI.Init();
    }

    public void OnOffIcon()
    {
        for (int i = 0; i < selectHero.listIcon.Count; i++)
        {
            string[] name = selectHero.listIcon[i].name.Split('_');
            ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();

            if (groupIcon.transform.GetChild(i).GetComponent<EventTrigger>().enabled)
            {
                if (int.Parse(moneyInGame.txtPoint.text) < int.Parse(AvUIManager.OnHero(name[3]).Price))
                {
                    item.boder.SetActive(true);
                    item.boder.GetComponent<Image>().fillAmount = 1;
                    listCheckSlot[i] = false;
                }
                else
                {
                    item.boder.SetActive(false);
                    item.boder.GetComponent<Image>().fillAmount = 1;
                    listCheckSlot[i] = true;
                }
            }
        }
    }

    public void DragIconChar()
    {
        if (press)
        {
            if (createImage)
            {
                img_Hero.GetComponent<Image>().sprite = selectHero.listIcon[int.Parse(objHero.name.Split('_')[1]) - 1];
                createObj = Instantiate(img_Hero);
                createObj.transform.localPosition = Vector3.zero;
                createObj.transform.SetParent(objHero.transform);
                createImage = false;
            }

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Lane")))
            {
                if (hit.transform.gameObject.tag == "Lane_1")
                {
                    lane[0].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[0].GetComponent<Lane>().PointExit();
                }

                if (hit.transform.gameObject.tag == "Lane_2")
                {
                    lane[1].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[1].GetComponent<Lane>().PointExit();
                }

                if (hit.transform.gameObject.tag == "Lane_3")
                {
                    lane[2].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[2].GetComponent<Lane>().PointExit();
                }

                if (hit.transform.gameObject.tag == "Lane_4")
                {
                    lane[3].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[3].GetComponent<Lane>().PointExit();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (hit.transform.gameObject.tag == "Lane_1")
                    {
                        AddHero(0);
                    }

                    if (hit.transform.gameObject.tag == "Lane_2")
                    {
                        AddHero(1);
                    }

                    if (hit.transform.gameObject.tag == "Lane_3")
                    {
                        AddHero(2);
                    }

                    if (hit.transform.gameObject.tag == "Lane_4")
                    {
                        AddHero(3);
                    }
                   
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                Destroy(createObj);
                press = false;
            }

            createObj.transform.position = Input.mousePosition;
        }
    }

    public void AddHero(int index)
    {
        string[] name = img_Hero.GetComponent<Image>().sprite.name.Split('_');
        string link = "Prefabs/" + menuManager.selectHero.faction.ToString() + "/" + name[3];
        string position = link + "_" + index + "_" + name[3];

        int price = int.Parse(AvUIManager.OnHero(name[3]).Price);
        int point = int.Parse(moneyInGame.txtPoint.text);

        if (point < price)
        {
            return;
        }

        moneyInGame.point -= price;

        menuManager.controlMulti.CreateHeroPlayer(position, index);

        lane[0].GetComponent<Lane>().PointExit();
        lane[1].GetComponent<Lane>().PointExit();
        lane[2].GetComponent<Lane>().PointExit();
        lane[3].GetComponent<Lane>().PointExit();
    }

    public void CreateHeroPlayer(Dictionary<string, string> data)
    {
        string[] name = data["position"].Split('_');
        string link = name[0];
        int index = int.Parse(name[1]);
        string nameHero = name[2] + "_" + name[3];

        GameObject obj = Instantiate(Resources.Load(link) as GameObject);

        if (obj != null)
        {
            if (obj.tag == CharacterLayer.Tank.ToString())
                obj.tag = CharacterLayer.Player.ToString();
            else
            {
                obj.transform.Find("Body").tag = CharacterLayer.Player.ToString();

                if (name[2] != "Sniper" && name[2] != "Disruptor"
                    && name[2] != "Morgana" && name[2] != "Twitch")
                {
                    obj.GetComponent<CharacterControl>().charAnimation.effice.GetComponent<RangerEffice>().target = CharacterLayer.Enemy;
                }
            }

            obj.layer = (int)CharacterLayer.Player;
            obj.GetComponent<CharacterControl>().target = CharacterLayer.Enemy;
            obj.GetComponent<CharacterControlSingle>().enabled = false;

            //Pos Red
            CharacterMove charMove = obj.GetComponent<CharacterMove>();
            charMove.tranPosBlue.gameObject.SetActive(false);
            charMove.tranPosRed.gameObject.SetActive(true);
            charMove.tranPosRed.localPosition = new Vector3(-0.6f, 25, 0);

            obj.transform.SetParent(lane[index].transform.GetChild(0));
            obj.transform.localPosition = Vector3.zero;

            if (name[2] == "Sniper")
                obj.transform.localRotation = Quaternion.Euler(0, 180, 0);
            else
                obj.transform.localRotation = Quaternion.identity;

            obj.name = nameHero;

            listHero.Add(obj);
        }

        for (int i = 0; i < selectHero.listIcon.Count; i++)
        {
            if (name[2] == selectHero.listIcon[i].name.Split('_')[3])
            {
                ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();
                groupIcon.transform.GetChild(i).GetComponent<EventTrigger>().enabled = false;
                string deplay = GUI_Manager.instance.configHero.Find_Name(name[2]).Deplay;
                listCheckSlot[i] = false;
                item.OpenBoder(float.Parse(deplay), i);
                break;
            }
        }
    }

    public void CreateHeroEnemy(Dictionary<string, string> data)
    {
        string[] name = data["position"].Split('_');
        string link = name[0];
        int index = int.Parse(name[1]);
        string nameHero = name[2] + "_" + name[3];

        GameObject obj = Instantiate(Resources.Load(link) as GameObject);

        if (obj != null)
        {
            if (obj.tag == CharacterLayer.Tank.ToString())
                obj.tag = CharacterLayer.Enemy.ToString();
            else
            {
                obj.transform.Find("Body").tag = CharacterLayer.Enemy.ToString();

                if (name[2] != "Sniper" && name[2] != "Disruptor"
                    && name[2] != "Morgana" && name[2] != "Twitch")
                {
                    obj.GetComponent<CharacterControl>().charAnimation.effice.GetComponent<RangerEffice>().target = CharacterLayer.Player;
                }
            }

            obj.layer = (int)CharacterLayer.Enemy;
            obj.GetComponent<CharacterControl>().target = CharacterLayer.Player;
            obj.GetComponent<CharacterControlSingle>().enabled = false;

            //Pos Blue
            CharacterMove charMove = obj.GetComponent<CharacterMove>();
            charMove.tranPosRed.gameObject.SetActive(false);
            charMove.tranPosBlue.gameObject.SetActive(true);
            charMove.tranPosBlue.localPosition = new Vector3(0.6f, 25, 0);

            obj.transform.SetParent(lane[index].transform.GetChild(1));
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;

            if (name[2] == "Sniper")
                obj.transform.localRotation = Quaternion.Euler(0, 180, 0);
            else
                obj.transform.localRotation = Quaternion.identity;

            obj.name = nameHero;

            listHero.Add(obj);
        }
    }

    public void PointDown(GameObject obj)
    {
        int index = int.Parse(obj.name.Split('_')[1]);
        if (obj.name == "Slot_" + index && listCheckSlot[index - 1])
        {
            objHero = obj;
            createImage = true;
            press = true; 
        }

        GUI_Manager.instance.startDrag = true;
    }

    public void PointUp()
    {
        // Debug.Log("PointUp");
        lane[0].GetComponent<Lane>().PointExit();
        lane[1].GetComponent<Lane>().PointExit();
        lane[2].GetComponent<Lane>().PointExit();
        lane[3].GetComponent<Lane>().PointExit();
        GUI_Manager.instance.startDrag = false;
    }
}
