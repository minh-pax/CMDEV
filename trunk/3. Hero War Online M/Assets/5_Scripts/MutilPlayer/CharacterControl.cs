﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterControl : MonoBehaviour
{
    public List<GameObject> listTarget;
    public CharacterLayer target;

    public MenuManager menuManager;
    public CharacterAnimation charAnimation;
    public CharacterMove charMove;
    public ConfigHero.Row hero;

    public bool checkAttack = true;
    private bool checkDie = true;
    public bool checkSub = false;

    // Use this for initialization
    void OnEnable()
    {
        charAnimation = GetComponent<CharacterAnimation>();
        charMove = GetComponent<CharacterMove>();
        listTarget = new List<GameObject>();     
    }

    void Start()
    {
        menuManager = GameObject.Find("ControlManager").transform.Find("MenuManager").GetComponent<MenuManager>();

        TextAsset configHeroAsset = Resources.Load("Config/ConfigHero") as TextAsset;
        ConfigHero configHero = new ConfigHero();
        configHero.Load(configHeroAsset);
        hero = configHero.Find_Name(gameObject.name.Split('_')[0]);
       
        CharacterMove();
    }

    public void CharacterMove()
    {
        charAnimation.PlayOneAnimation(CharacterAnim.run.ToString());
        charMove.checkMove = true;
    }

    public void CharacterAttack()
    {
        charMove.checkMove = false;
        checkAttack = true;
    }

    public void CharacterStopMove()
    {
        charMove.checkMove = false;
    }

    public void CharacterVictory()
    {
        charAnimation.PlayOneAnimation(CharacterAnim.victory.ToString());
        charMove.checkMove = false;
        listTarget.Clear();
    }

    public void CharacterIdle()
    {
        charAnimation.PlayOneAnimation(CharacterAnim.idle.ToString());
        charMove.checkMove = false;
        listTarget.Clear();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        charAnimation.OnAnimation();
        charMove.OnMove();
        OnControl();
    }

    public void OnControl()
    {
        if (int.Parse(hero.HP) <= 0 && checkDie)
        {
            charAnimation.PlayDealthAnimation(CharacterAnim.die.ToString());
            checkDie = false;
        }
    }

    public void OnAttack()
    {
        if (listTarget.Count > 0)
        {
            if (listTarget[0] != null && gameObject.layer == (int)CharacterLayer.Player)
            {
                if (checkAttack)
                {
                    StartCoroutine(menuManager.controlMulti.SubHealthHero(gameObject.name, listTarget[0].name, hero.Attack));
                    checkSub = true;
                    checkAttack = false;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == target.ToString())
        {
            CharacterAttack();

            if (gameObject.layer == (int)CharacterLayer.Player)
            {
                if (other.gameObject.name == "Body")
                {
                    menuManager.controlMulti.AttackCharacter(gameObject.name, other.gameObject.GetComponent<Body>().target.name);
                }
                else
                {
                    menuManager.controlMulti.AttackCharacter(gameObject.name, other.gameObject.name);
                }   
            }
        }
    }
}
