﻿using UnityEngine;
using System.Collections;
using SocketIO;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class ControlMulti : MonoBehaviour {

    public SocketIOComponent socket;
    public MenuManager menuManager;
    public Room playerInRoom;
    public Dictionary<string, string> dataPlayer;
    public Dictionary<string, string> dataEnemy;

    GameObject objAttack;
    GameObject objTarget;


    // Use this for initialization

    void Start () {
        dataPlayer = new Dictionary<string, string>();
        dataEnemy = new Dictionary<string, string>();
        playerInRoom = new Room();
        socket.On("USER_CONNECTED", OnUserConntected);
        socket.On("PLAY", OnUserPlay);
        socket.On("CREATE_ROOM", OnCreateRoom);
        socket.On("USER_DISCONNECTED", OnUserDisconnected);
        socket.On("RESULT", OnUserResult);
        socket.On("ACCEPT", OnAccept);
        socket.On("ADD_HERO_PLAYER", OnAddHeroPlayer);
        socket.On("ADD_HERO_ENEMY", OnAddHeroEnemy);
        socket.On("ATTACK_HERO", OnAttackCharacter);
        socket.On("SUB_HERO", OnSubHealthHero);
        socket.On("DEALTH_HERO", OnDealthHero);
        socket.On("LEAVE_MATCH", OnLeaveMatch);
        socket.On("VERSION", OnVersion);
    }

    public void CheckVersion()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("34"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, null);
            return;
        }

        Dictionary<string, string> data = new Dictionary<string, string>();
        data["version"] = AvUIManager.version;
        socket.Emit("VERSION", new JSONObject(data));
    }
	
	public void ConnectToServer()
    {
        MatchGameServer();
        socket.Emit("USER_CONNECT");

        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void MatchGameServer()
    {
        if (menuManager.matchGame.inputField.text != "")
        {
            string device = SystemInfo.deviceUniqueIdentifier;
            dataPlayer["device"] = device;
            dataPlayer["name"] = menuManager.matchGame.inputField.text;
            dataPlayer["tower"] = menuManager.selectHero.inforGame.lvTower.ToString();
            Dictionary<string, string> data = new Dictionary<string, string>();
            data["name"] = dataPlayer["name"];
            data["device"] = dataPlayer["device"];
            data["tower"] = dataPlayer["tower"];
            data["random"] = Random.Range(1, 100).ToString();
            socket.Emit("PLAY", new JSONObject(data));
            menuManager.OpenWaitting();
        }
        else
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("33"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, null);
        }
    }

    private void OnVersion(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnVersion ");
        string version = JsonToString(evt.data.GetField("version").ToString(), "\"");
       
        if (AvUIManager.version == version)
        {
            menuManager.OpenMatch();
        }
        else
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("43"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, null);
        }
    }

    private void OnUserConntected(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserConntected ");
    }

    private void OnUserPlay(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserPlay ");
    }

    private void OnUserDisconnected(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserDisconnected ");
        menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("35"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, res => {
            if (res == MessageBox.DialogResult.Ok)
            {
                menuManager.OpenResetMenu();
                SceneManager.LoadScene("Menu");
            }

            return true;
        });
    }

    private void OnCreateRoom(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " CreateRoom ");

        bool checkOpenInGame = false;
        string playerName1 = JsonToString(evt.data.GetField("player1").ToString(), "\"");
        string playerName2 = JsonToString(evt.data.GetField("player2").ToString(), "\"");
        string playerName1Device = JsonToString(evt.data.GetField("player1Device").ToString(), "\"");
        string playerName2Device = JsonToString(evt.data.GetField("player2Device").ToString(), "\"");
        string towerName1 = JsonToString(evt.data.GetField("tower1").ToString(), "\"");
        string towerName2 = JsonToString(evt.data.GetField("tower2").ToString(), "\"");
        string random = JsonToString(evt.data.GetField("random").ToString(), "\"");
        string room = JsonToString(evt.data.GetField("room").ToString(), "\"");

        if (menuManager.matchGame.inputField.text == playerName1 && dataPlayer["device"] == playerName1Device)
        {

            dataPlayer["id"] = "1";
            dataEnemy["id"]  = "2";
            dataEnemy["device"] = playerName2Device;
            dataEnemy["tower"] = towerName2;
            checkOpenInGame = true;
        }
        else 
        {
            dataPlayer["id"] = "2";
            dataEnemy["id"] = "1";
            dataEnemy["device"] = playerName1Device;
            dataEnemy["tower"] = towerName1;
            checkOpenInGame = true;
        }

        if (checkOpenInGame)
        {
            if (dataPlayer["name"] == playerName1)
                dataEnemy["name"] = playerName2;
            else
                dataEnemy["name"] = playerName1;

            dataPlayer["random"] = random;

            playerInRoom.player1 = playerName1;
            playerInRoom.player2 = playerName2;
            playerInRoom.room = room;
            checkOpenInGame = false;
            menuManager.sceneLoader.GotoSceneGame();
        }

        Dictionary<string, string> data = new Dictionary<string, string>();
        data["player1"] = playerName1;
        data["player2"] = playerName2;
        data["player1Device"] = playerName1Device;
        data["player2Device"] = playerName2Device;
        socket.Emit("ADD_ENEMY", new JSONObject(data));
    }

    public void AcceptGoToGame()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["room"] = playerInRoom.room; 
        socket.Emit("ACCEPT", new JSONObject(data));
    }

    private void OnAccept(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnAccept ");
        menuManager.sceneLoader.checkFinish = true;
    }

    public void CreateHeroPlayer(string position, int index)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["playerID"] = dataPlayer["id"];
        data["position"] = position;
        data["room"] = playerInRoom.room;
        socket.Emit("CREATE_HERO", new JSONObject(data));
    }

    private void OnAddHeroPlayer(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnAddHeroPlayer ");
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["playerID"] = JsonToString(evt.data.GetField("playerID").ToString(), "\"");
        data["position"] = JsonToString(evt.data.GetField("position").ToString(), "\"");
        data["room"] = JsonToString(evt.data.GetField("room").ToString(), "\"");

        menuManager.multiManager.CreateHeroPlayer(data);
    }

    private void OnAddHeroEnemy(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnAddHeroEnemy ");
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["playerID"] = JsonToString(evt.data.GetField("playerID").ToString(), "\"");
        data["position"] = JsonToString(evt.data.GetField("position").ToString(), "\"");
        data["room"] = JsonToString(evt.data.GetField("room").ToString(), "\"");

        menuManager.multiManager.CreateHeroEnemy(data);
    }

    public void AttackCharacter(string attack, string target)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["attack"] = attack;
        data["target"] = target;
        data["room"] = playerInRoom.room;
        socket.Emit("ATTACK_HERO", new JSONObject(data));
    }

    private void OnAttackCharacter(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnAttackCharacter ");
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["attack"] = JsonToString(evt.data.GetField("attack").ToString(), "\"");
        data["target"] = JsonToString(evt.data.GetField("target").ToString(), "\"");
        data["room"] = JsonToString(evt.data.GetField("room").ToString(), "\"");

        objAttack = null;
        objTarget = null;

        bool checkTarget = true;

        foreach (GameObject item in menuManager.multiManager.listHero)
        {
            if (item.name == data["attack"])
            {
                objAttack = item;
            }

            if (item.name == data["target"])
            {
                objTarget = item;
                checkTarget = false;
            }
        }

        if (checkTarget)
        {
            return;
        }

        if (objAttack != null && objTarget != null)
        {
            objAttack.GetComponent<CharacterControl>().listTarget.Add(objTarget);
            objAttack.GetComponent<CharacterControl>().charAnimation.PlayAttackAnimation(CharacterAnim.attack_1.ToString());
            objAttack.GetComponent<CharacterControl>().OnAttack();
        }
    }

    public IEnumerator SubHealthHero(string attack, string target, string damge)
    {
        yield return new WaitForSeconds(AvUIManager.DeplayAttack(attack.Split('_')[0]));
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["attack"] = attack;
        data["target"] = target;
        data["damge"] = damge;
        data["device"] = dataPlayer["device"];
        data["room"] = playerInRoom.room;
        socket.Emit("SUB_HERO", new JSONObject(data));
    }

    private void OnSubHealthHero(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnSubHealthHero ");
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["attack"] = JsonToString(evt.data.GetField("attack").ToString(), "\"");
        data["target"] = JsonToString(evt.data.GetField("target").ToString(), "\"");
        data["damge"] = JsonToString(evt.data.GetField("damge").ToString(), "\"");
        data["device"] = JsonToString(evt.data.GetField("device").ToString(), "\"");

        objAttack = null;
        objTarget = null;

        foreach (GameObject item in menuManager.multiManager.listHero)
        {
            if (item.name == data["attack"])
            {
                objAttack = item;
                break;
            }
        }

        if (objAttack != null)
        {
            foreach (GameObject item in objAttack.GetComponent<CharacterControl>().listTarget)
            {
                if (item.name == data["target"])
                {
                    objTarget = item;
                    break;
                }
            }
        }

        if (objAttack != null && objTarget != null)
        {
            //Target 
            int hp = 0;
            Dictionary<string, string> dataResult = new Dictionary<string, string>();

            if (objTarget.name == "Tower_Player" || objTarget.name == "Tower_Enemy")
            {
                hp = (int) SubTower(data, dataResult);
            }
            else
            {
                hp = int.Parse(objTarget.GetComponent<CharacterControl>().hero.HP);
                int damge = int.Parse(data["damge"]);
                hp -= damge;

                objTarget.GetComponent<CharacterControl>().hero.HP = hp.ToString();
            }

            //Attack
            if (hp <= 0)
            {
                if (objTarget.name == "Tower_Player" || objTarget.name == "Tower_Enemy")
                {
                    WinLoseGame(dataResult);
                    objAttack.GetComponent<CharacterControl>().listTarget.Clear();
                    objAttack.GetComponent<CharacterControl>().checkSub = false;
                }
                else
                {
                    DealthHero(objAttack.name, objTarget.name);
                }
            }
            else
            {
                objAttack.GetComponent<CharacterControl>().checkAttack = true;
                objAttack.GetComponent<CharacterControl>().OnAttack();
            }
        }
    }

    public void DealthHero(string attack, string dealth)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["attack"] = attack;
        data["dealth"] = dealth;
        data["room"] = playerInRoom.room;
        socket.Emit("DEALTH_HERO", new JSONObject(data));
    }

    private void OnDealthHero(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnDealthHero ");

        Dictionary<string, string> data = new Dictionary<string, string>();
        data["attack"] = JsonToString(evt.data.GetField("attack").ToString(), "\"");
        data["dealth"] = JsonToString(evt.data.GetField("dealth").ToString(), "\"");
        data["room"] = JsonToString(evt.data.GetField("room").ToString(), "\"");

        objAttack = null;
        objTarget = null;

        foreach (GameObject item in menuManager.multiManager.listHero)
        {
            if (item.name == data["attack"])
            {
                objAttack = item;
                break;
            }
        }

        foreach (GameObject item in objAttack.GetComponent<CharacterControl>().listTarget)
        {
            if (item.name == data["dealth"])
            {
                objTarget = item;
                break;
            }
        }

        if (objAttack != null && objTarget != null)
        {
            objAttack.GetComponent<CharacterControl>().listTarget.Remove(objTarget);
            objAttack.GetComponent<CharacterControl>().checkAttack = true;
            objAttack.GetComponent<CharacterControl>().OnAttack();
        }
    }

    public float SubTower(Dictionary<string, string> data, Dictionary<string, string> dataResult)
    {
        float oldHP = 0;
        float currentHP = 0;
        int damge = int.Parse(data["damge"]);
        Tower userTower;

        if (dataPlayer["device"] == data["device"])
        {
            userTower = menuManager.multiManager.towerEnemy;
            currentHP = int.Parse(userTower.tower.HP);
            oldHP = currentHP;
            currentHP -= damge;
            userTower.tower.HP = currentHP.ToString();

            dataResult["name"] = dataPlayer["name"];
            dataResult["device"] = dataPlayer["device"];

            menuManager.multiManager.playerUI.checkFillAmount = false;
            menuManager.multiManager.enemyUI.SubHeath(currentHP, oldHP, float.Parse(userTower.maxHealth));
        }
        else
        {
            userTower = menuManager.multiManager.towerPlayer;
            currentHP = int.Parse(userTower.tower.HP);
            oldHP = currentHP;
            currentHP -= damge;
            userTower.tower.HP = currentHP.ToString();

            dataResult["name"] = dataEnemy["name"];
            dataResult["device"] = dataEnemy["device"];

            menuManager.multiManager.playerUI.checkFillAmount = false;
            menuManager.multiManager.playerUI.SubHeath(currentHP, oldHP, float.Parse(userTower.maxHealth));
        }

        return currentHP;
    }

    public void WinLoseGame(Dictionary<string, string> dataUser)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["name"] = dataUser["name"];
        data["device"] = dataUser["device"];
        data["room"] = playerInRoom.room;
        socket.Emit("RESULT", new JSONObject(data));
    }

    private void OnUserResult(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserResult ");

        menuManager.googleAdmob.RequestInterstitial();
        StartCoroutine(OnWaitResult(evt));
    }

    IEnumerator OnWaitResult(SocketIOEvent evt)
    {
        yield return new WaitForSeconds(2f);

        string usernameWin = JsonToString(evt.data.GetField("name").ToString(), "\"");
        string device = JsonToString(evt.data.GetField("device").ToString(), "\"");

        menuManager.selectHero.statusGame = "Multi";
        LeaveRoom();

        if (usernameWin == dataPlayer["name"] && device == dataPlayer["device"])
        {
            menuManager.guiManager.money += 100;
            menuManager.guiManager.SaveMoneyinGame(menuManager.guiManager.money);

            menuManager.OpenResutl("40", dataPlayer["name"], 100);
            StatusHero(true);
        }
        else
        {
            menuManager.OpenResutl("41", dataPlayer["name"], 0);
            StatusHero(false);
        }
    }
         

    public void StatusHero(bool win)
    {
        foreach (GameObject item in menuManager.singeManager.listHero)
        {
            if (win)
            {
                if (item.tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControl>().CharacterVictory();
                }
                else if (item.tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControl>().CharacterIdle();
                }

                if (item.tag == CharacterLayer.Ranger.ToString())
                {
                    if (item.transform.Find("Body").tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControl>().CharacterVictory();
                    }
                    else if (item.transform.Find("Body").tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControl>().CharacterIdle();
                    }
                }
            }
            else
            {
                if (item.tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControl>().CharacterIdle();
                }
                else if (item.tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControl>().CharacterVictory();
                }

                if (item.tag == CharacterLayer.Ranger.ToString())
                {
                    if (item.transform.Find("Body").tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControl>().CharacterIdle();
                    }
                    else if (item.transform.Find("Body").tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControl>().CharacterVictory();
                    }
                }
            }
        }
    }

    public void LeaveRoom()
    {
        socket.Emit("LEAVE_ROOM");  
    }

    public void LeaveMatchGame()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["name"] = dataPlayer["name"];
        data["device"] = dataPlayer["device"];
        data["status"] = "ongame";
        socket.Emit("LEAVE_MATCH", new JSONObject(data));
    }

    public void LeaveMatchInGame()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["name"] = dataPlayer["name"];
        data["device"] = dataEnemy["device"];
        data["status"] = "ingame";
        socket.Emit("LEAVE_MATCH", new JSONObject(data));
    }

    private void OnLeaveMatch(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnLeaveMatch ");
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["name"] = JsonToString(evt.data.GetField("name").ToString(), "\"");
        data["device"] = JsonToString(evt.data.GetField("device").ToString(), "\"");
        data["status"] = JsonToString(evt.data.GetField("status").ToString(), "\"");

        if (dataPlayer["device"] == data["device"] && data["status"] == "ingame")
        {
            menuManager.guiManager.money += 100;
            menuManager.guiManager.SaveMoneyinGame(menuManager.guiManager.money);
        }
    }

    string JsonToString(string target, string s)
    {
        string[] newString = Regex.Split(target, s);
        return newString[1];
    }
}
