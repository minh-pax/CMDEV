﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SingeManager : MonoBehaviour
{

    public GameObject img_Hero;
    public GameObject groupIcon;
    public MenuManager menuManager;
    public PlayerUI playerUI, enemyUI;
    public Tower towerPlayer;
    public Tower towerEnemy;
    public MoneyInGame moneyInGame;
    public Factions factions;
    public GameObject[] lane;
    public List<GameObject> listHero;
    public List<bool> listCheckSlot;

    private bool press = false;
    private bool createImage = false;
    private bool checkComputer = true;
    private bool checkStop = true;
    private int count;
    private GameObject createObj;
    private GameObject objHero;
    private SelectHero selectHero;
    private List<int> ListDragEnemy;

    public void InitSinglePlayerGame()
    {
        count = 0;
        checkComputer = true;
        checkStop = true;

        ListDragEnemy = new List<int>();

        GameObject obj = GameObject.Find("Model");
        GameObject objMapGame = GameObject.Find("MapGame");

        for (int i = 0; i < 4; i++)
        {
            lane[i] = obj.transform.GetChild(i).gameObject;
        }

        towerPlayer = obj.transform.Find("Tower_Player").GetComponent<Tower>();
        towerEnemy = obj.transform.Find("Tower_Enemy").GetComponent<Tower>();

        int random = Random.Range(1, 100);
        objMapGame.GetComponent<MapGame>().StartRandom(random);

        listCheckSlot = new List<bool>();
        listHero = new List<GameObject>();

        selectHero = menuManager.selectHero;

        for (int i = 0; i < 6; i++)
        {
            bool check = false;
            listCheckSlot.Add(check);
        }

        for (int i = 0; i < int.Parse(selectHero.lvNormalTower.Slot); i++)
        {
            ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();
            item.InitIcon(item.normalIcon, false, "");
        }

        for (int i = 0; i < selectHero.listIcon.Count; i++)
        {
            ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();
            item.InitIcon(selectHero.listIcon[i], false, AvUIManager.OnHero(selectHero.listIcon[i].name.Split('_')[3]).Price);
            listCheckSlot[i] = false;
        }

        towerPlayer.InitInfoTower(selectHero.inforGame.lvTower.ToString());
        towerEnemy.InitInfoTower(GUI_Manager.instance.level.ToString());

        playerUI.playerName.text = "PLAYER";
        playerUI.txtHealth.text = towerPlayer.tower.HP + "/" + towerPlayer.maxHealth;

        enemyUI.playerName.text = "COMPUTER";
        enemyUI.txtHealth.text = towerEnemy.tower.HP + "/" + towerEnemy.maxHealth;

        moneyInGame.Init(selectHero.inforGame.lv.ToString(), towerPlayer.tower.Level);

        listHero.Add(towerPlayer.gameObject);
        listHero.Add(towerEnemy.gameObject);
    }

    public void RemoveData()
    {
        menuManager.selectHero.ClearIconHero();
        listHero.Clear();
    }

    void Update()
    {
        DragIconChar();
        OnOffIcon();
        playerUI.Init();
        enemyUI.Init();

        if (checkComputer && checkStop)
        {
            StartCoroutine(AISetPosition());
            checkComputer = false;
        }
    }

    public void StopCreateHeroComputer(bool checkStatus)
    {
        if (checkStatus)
        {
            checkStop = true;
        }
        else
        {
            checkStop = false;
        }
    }

    public void OnOffIcon()
    {
        for (int i = 0; i < selectHero.listIcon.Count; i++)
        {
            string[] name = selectHero.listIcon[i].name.Split('_');
            ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();

            if (groupIcon.transform.GetChild(i).GetComponent<EventTrigger>().enabled)
            {
                if (int.Parse(moneyInGame.txtPoint.text) < int.Parse(AvUIManager.OnHero(name[3]).Price))
                {
                    item.boder.SetActive(true);
                    item.boder.GetComponent<Image>().fillAmount = 1;
                    listCheckSlot[i] = false;
                }
                else
                {
                    item.boder.SetActive(false);
                    item.boder.GetComponent<Image>().fillAmount = 1;
                    listCheckSlot[i] = true;
                }
            }
        }
    }

    public void DragIconChar()
    {
        if (press)
        {
            if (createImage)
            {
                img_Hero.GetComponent<Image>().sprite = selectHero.listIcon[int.Parse(objHero.name.Split('_')[1]) - 1];
                createObj = Instantiate(img_Hero);
                createObj.transform.localPosition = Vector3.zero;
                createObj.transform.SetParent(objHero.transform);
                createImage = false;
            }

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Lane")))
            {
                if (hit.transform.gameObject.tag == "Lane_1")
                {
                    lane[0].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[0].GetComponent<Lane>().PointExit();
                }

                if (hit.transform.gameObject.tag == "Lane_2")
                {
                    lane[1].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[1].GetComponent<Lane>().PointExit();
                }

                if (hit.transform.gameObject.tag == "Lane_3")
                {
                    lane[2].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[2].GetComponent<Lane>().PointExit();
                }

                if (hit.transform.gameObject.tag == "Lane_4")
                {
                    lane[3].GetComponent<Lane>().PointEnter();
                }
                else
                {
                    lane[3].GetComponent<Lane>().PointExit();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (hit.transform.gameObject.tag == "Lane_1")
                    {
                        AddHero(0);
                    }

                    if (hit.transform.gameObject.tag == "Lane_2")
                    {
                        AddHero(1);
                    }

                    if (hit.transform.gameObject.tag == "Lane_3")
                    {
                        AddHero(2);
                    }

                    if (hit.transform.gameObject.tag == "Lane_4")
                    {
                        AddHero(3);
                    }

                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                Destroy(createObj);
                press = false;
            }

            createObj.transform.position = Input.mousePosition;
        }
    }

    public void AddHero(int index)
    {
        string[] name = img_Hero.GetComponent<Image>().sprite.name.Split('_');
        string link = "Prefabs/" + menuManager.selectHero.faction.ToString() + "/" + name[3];
        string position = link + "_" + index + "_" + name[3];

        int price = int.Parse(AvUIManager.OnHero(name[3]).Price);
        int point = int.Parse(moneyInGame.txtPoint.text);

        if (point < price)
        {
            return;
        }

        moneyInGame.point -= price;

        CreateHeroPlayer(position);

        lane[0].GetComponent<Lane>().PointExit();
        lane[1].GetComponent<Lane>().PointExit();
        lane[2].GetComponent<Lane>().PointExit();
        lane[3].GetComponent<Lane>().PointExit();
    }

    public void CreateHeroPlayer(string position)
    {
        count++;
        string[] name = position.Split('_');
        string link = name[0];
        int index = int.Parse(name[1]);
        string nameHero = name[2] + "_" + count;
        ListDragEnemy.Add(index);

        GameObject obj = Instantiate(Resources.Load(link) as GameObject);

        if (obj != null)
        {
            if (obj.tag == CharacterLayer.Tank.ToString())
                obj.tag = CharacterLayer.Player.ToString();
            else
            {
                obj.transform.Find("Body").tag = CharacterLayer.Player.ToString();

                if (name[2] != "Sniper" && name[2] != "Disruptor"
                    && name[2] != "Morgana" && name[2] != "Twitch")
                {
                    obj.GetComponent<CharacterControlSingle>().charAnimation.effice.GetComponent<RangerEffice>().target = CharacterLayer.Enemy;
                }
            }

            obj.GetComponent<CharacterControlSingle>().target = CharacterLayer.Enemy;
            obj.GetComponent<CharacterControl>().enabled = false;

            //Pos Red
            CharacterMove charMove = obj.GetComponent<CharacterMove>();
            charMove.tranPosBlue.gameObject.SetActive(false);
            charMove.tranPosRed.gameObject.SetActive(true);
            charMove.tranPosRed.localPosition = new Vector3(-0.6f, 25, 0);

            obj.transform.SetParent(lane[index].transform.GetChild(0));
            obj.transform.localPosition = Vector3.zero;

            if (name[2] == "Sniper")
                obj.transform.localRotation = Quaternion.Euler(0, 180, 0);
            else
                obj.transform.localRotation = Quaternion.identity;

            obj.name = nameHero;

            listHero.Add(obj);
        }

        for (int i = 0; i < selectHero.listIcon.Count; i++)
        {
            if (name[2] == selectHero.listIcon[i].name.Split('_')[3])
            {
                ItemHandler item = groupIcon.transform.GetChild(i).GetComponent<ItemHandler>();
                groupIcon.transform.GetChild(i).GetComponent<EventTrigger>().enabled = false;
                string deplay = GUI_Manager.instance.configHero.Find_Name(name[2]).Deplay;
                listCheckSlot[i] = false;
                item.OpenBoder(float.Parse(deplay), i);
                break;
            }
        }
    }

    IEnumerator AISetPosition()
    {
        yield return new WaitForSeconds(2f);

        int max = GUI_Manager.instance.level + 1;
        int random = Random.Range(1, max);

        string hero = "Darius";
        string faction = "LOL";

        if (menuManager.selectHero.faction == "LOL")
        {
            faction = "DOTA";
            hero = AvUIManager.OnHeroDOTA(random);
        }
        else
        {
            faction = "LOL";
            hero = AvUIManager.OnHeroLOL(random);
        }

        ConfigHero.Row row = AvUIManager.OnHero(hero);

        int price = int.Parse(row.Price);
        int point = moneyInGame.pointEnemy;

        if (point >= price)
        {
            if (ListDragEnemy.Count > 0)
            {
                moneyInGame.pointEnemy -= price;

                string link = "Prefabs/" + faction + "/" + hero;
                string position = link + "_" + ListDragEnemy[0] + "_" + hero;
                ListDragEnemy.Remove(ListDragEnemy[0]);

                CreateHeroEnemy(position);
            }
            else
            {

                moneyInGame.pointEnemy -= price;
                int index = Random.Range(0, 3);

                string link = "Prefabs/" + faction + "/" + hero;
                string position = link + "_" + index + "_" + hero;

                ListDragEnemy.Remove(index);
                CreateHeroEnemy(position);
            }
        }
        else
            checkComputer = true;
    }

    public void CreateHeroEnemy(string position)
    {
        count++;
        string[] name = position.Split('_');
        string link = name[0];
        int index = int.Parse(name[1]);
        string nameHero = name[2] + "_" + count;

        GameObject obj = Instantiate(Resources.Load(link) as GameObject);

        if (obj != null)
        {
            if (obj.tag == CharacterLayer.Tank.ToString())
                obj.tag = CharacterLayer.Enemy.ToString();
            else
            {
                obj.transform.Find("Body").tag = CharacterLayer.Enemy.ToString();

                if (name[2] != "Sniper" && name[2] != "Disruptor" 
                    && name[2] != "Morgana" && name[2] != "Twitch")
                {
                    obj.GetComponent<CharacterControlSingle>().charAnimation.effice.GetComponent<RangerEffice>().target = CharacterLayer.Player;
                    obj.GetComponent<CharacterControl>().charAnimation.effice.GetComponent<RangerEffice>().target = CharacterLayer.Player;
                }
            }

            obj.GetComponent<CharacterControlSingle>().target = CharacterLayer.Player;
            obj.GetComponent<CharacterControl>().enabled = false;

            //Pos Blue
            CharacterMove charMove = obj.GetComponent<CharacterMove>();
            charMove.tranPosRed.gameObject.SetActive(false);
            charMove.tranPosBlue.gameObject.SetActive(true);
            charMove.tranPosBlue.localPosition = new Vector3(0.6f, 25, 0);

            obj.transform.SetParent(lane[index].transform.GetChild(1));
            obj.transform.localPosition = Vector3.zero;

            if (name[2] == "Sniper")
                obj.transform.localRotation = Quaternion.Euler(0, 180, 0); 
            else
                obj.transform.localRotation = Quaternion.identity;

            obj.name = nameHero;

            listHero.Add(obj);
        }

        checkComputer = true;
    }

    public void PointDown(GameObject obj)
    {
        int index = int.Parse(obj.name.Split('_')[1]);
        if (obj.name == "Slot_" + index && listCheckSlot[index - 1])
        {
            objHero = obj;
            createImage = true;
            press = true;
        }

        GUI_Manager.instance.startDrag = true;
    }

    public void PointUp()
    {
        // Debug.Log("PointUp");
        lane[0].GetComponent<Lane>().PointExit();
        lane[1].GetComponent<Lane>().PointExit();
        lane[2].GetComponent<Lane>().PointExit();
        lane[3].GetComponent<Lane>().PointExit();
        GUI_Manager.instance.startDrag = false;
    }
}
