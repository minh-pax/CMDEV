﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectMap : MonoBehaviour
{
    public MenuManager menuManager;
    public List<GameObject> btn;
    public List<GameObject> star;
    public List<GameObject> arrow;

    private InforData data;

    public void Init(InforData inforData)
    {
        data = inforData;

        for (int i = 0; i < 8; i++)
        {
            SetStar(i, data.star[i]);

            if (i + 1 <= data.lv)
            {
                btn[i].GetComponent<Button>().enabled = true;
            }
            else
            {
                btn[i].GetComponent<Button>().enabled = false;
            }

            if (i < arrow.Count - 1)
            {
                if (i + 2 == data.lv)
                {
                    arrow[i].GetComponent<Animator>().enabled = true;
                }
                else
                {
                    arrow[i].GetComponent<Animator>().enabled = false;
                }
            }  
        }

    }

    public void SetStar(int count, int index)
    {
        for (int i = 0; i < 3; i++)
        {
            if (i < index)
            {
                star[count].transform.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                star[count].transform.GetChild(i).gameObject.SetActive(false);
            }
          
        }
    }

    public void LevelMap(GameObject obj)
    {
        string[] name = obj.name.Split('_');

        GUI_Manager.instance.level = int.Parse(name[1]);
        menuManager.OpenSelectHeroSingle();
    }
}
