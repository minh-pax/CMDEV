﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlSingleManager : MonoBehaviour
{
    public MenuManager menuManager;
    private GameObject objAttack;
    private GameObject objTarget;

    public void AttackCharacter(string attack, string target)
    {
        objAttack = null;
        objTarget = null;

        bool checkTarget = true;

        foreach (GameObject item in menuManager.singeManager.listHero)
        {
            if (item.name == attack)
            {
                objAttack = item;
            }

            if (item.name == target)
            {
                objTarget = item;
                checkTarget = false;
            }
        }

        if (checkTarget)
        {
            return;
        }

        if (objAttack != null && objTarget != null)
        {
            objAttack.GetComponent<CharacterControlSingle>().listTarget.Add(objTarget);
            objAttack.GetComponent<CharacterControlSingle>().OnAttack();
            objAttack.GetComponent<CharacterControlSingle>().charAnimation.PlayAttackAnimation(CharacterAnim.attack_1.ToString());
        }
    }

    public IEnumerator SubHealthHero(string attack, string target, string damge)
    {
        yield return new WaitForSeconds(AvUIManager.DeplayAttack(attack.Split('_')[0]));
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["attack"] = attack;
        data["target"] = target;
        data["damge"] = damge;

        objAttack = null;
        objTarget = null;

        foreach (GameObject item in menuManager.singeManager.listHero)
        {
            if (item.name == data["attack"])
            {
                objAttack = item;
                break;
            }
        }

        if (objAttack != null)
        {
            List<GameObject> obj = objAttack.GetComponent<CharacterControlSingle>().listTarget;

            for (int i = 0; i < obj.Count; i++)
            {
                if (obj[i] == null)
                    obj.Remove(obj[i]);
            }

            foreach (GameObject item in objAttack.GetComponent<CharacterControlSingle>().listTarget)
            {
                if (item.name == data["target"])
                {
                    objTarget = item;
                    break;
                }
            }
        }

        if (objAttack != null && objTarget != null)
        {
            //Target 
            int hp = 0;
            Dictionary<string, string> dataResult = new Dictionary<string, string>();

            if (objTarget.name == "Tower_Player" || objTarget.name == "Tower_Enemy")
            {
                hp = (int)SubTower(objTarget.name, int.Parse(damge), dataResult);
            }
            else
            {
                hp = int.Parse(objTarget.GetComponent<CharacterControlSingle>().hero.HP);
                int damgeHero = int.Parse(data["damge"]);
                hp -= damgeHero;

                objTarget.GetComponent<CharacterControlSingle>().hero.HP = hp.ToString();
            }

            //Attack
            if (hp <= 0)
            {
                if (objTarget.name == "Tower_Player" || objTarget.name == "Tower_Enemy")
                {
                    WinLoseGame(dataResult);
                    objAttack.GetComponent<CharacterControlSingle>().listTarget.Clear();
                    objAttack.GetComponent<CharacterControlSingle>().checkSub = false;
                }
                else
                {
                    DealthHero(objAttack.name, objTarget.name);
                }
            }
            else
            {
                objAttack.GetComponent<CharacterControlSingle>().checkAttack = true;
                objAttack.GetComponent<CharacterControlSingle>().OnAttack();
            }
        }
    }

    public void DealthHero(string attack, string dealth)
    {
        objAttack = null;
        objTarget = null;

        foreach (GameObject item in menuManager.singeManager.listHero)
        {
            if (item.name == attack)
            {
                objAttack = item;
                break;
            }
        }

        foreach (GameObject item in objAttack.GetComponent<CharacterControlSingle>().listTarget)
        {
            if (item.name == dealth)
            {
                objTarget = item;
                break;
            }
        }

        if (objAttack != null && objTarget != null)
        {
            objAttack.GetComponent<CharacterControlSingle>().listTarget.Remove(objTarget);
            objAttack.GetComponent<CharacterControlSingle>().checkAttack = true;
            objAttack.GetComponent<CharacterControlSingle>().OnAttack();
        }
    }

    public float SubTower(string target, int damge, Dictionary<string, string> dataResult)
    {
        float oldHP = 0;
        float currentHP = 0;
        Tower userTower;

        if (target == "Tower_Enemy")
        {
            userTower = menuManager.singeManager.towerEnemy;
            currentHP = int.Parse(userTower.tower.HP);
            oldHP = currentHP;
            currentHP -= damge;
            userTower.tower.HP = currentHP.ToString();

            dataResult["name"] = "PLAYER";

            menuManager.singeManager.playerUI.checkFillAmount = false;
            menuManager.singeManager.enemyUI.SubHeath(currentHP, oldHP, float.Parse(userTower.maxHealth));
        }
        else
        {
            userTower = menuManager.singeManager.towerPlayer;
            currentHP = int.Parse(userTower.tower.HP);
            oldHP = currentHP;
            currentHP -= damge;
            userTower.tower.HP = currentHP.ToString();

            dataResult["name"] = "COMPUTER";

            menuManager.singeManager.playerUI.checkFillAmount = false;
            menuManager.singeManager.playerUI.SubHeath(currentHP, oldHP, float.Parse(userTower.maxHealth));
        }

        return currentHP;
    }

    public void WinLoseGame(Dictionary<string, string> dataUser)
    {
        menuManager.googleAdmob.RequestInterstitial();
        StartCoroutine(OnWaitResult(dataUser));
    }

    IEnumerator OnWaitResult(Dictionary<string, string> dataUser)
    {
        yield return new WaitForSeconds(2f);

        menuManager.selectHero.statusGame = "Multi";
        menuManager.singeManager.StopCreateHeroComputer(false);

        if (dataUser["name"] == "PLAYER")
        {
            menuManager.OpenResutl("40", "PLAYER", OnResult());
            StatusHero(true);
        }
        else
        {
            menuManager.OpenResutl("41", "PLAYER", 0);
            StatusHero(false);
        }
    }

    public void StatusHero(bool win)
    {
        foreach (GameObject item in menuManager.singeManager.listHero)
        {
            if (win)
            {
                if (item.tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControlSingle>().CharacterVictory();
                }
                else if (item.tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControlSingle>().CharacterIdle();
                }

                if (item.tag == CharacterLayer.Ranger.ToString())
                {
                    if (item.transform.Find("Body").tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControlSingle>().CharacterVictory();
                    }
                    else if (item.transform.Find("Body").tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControlSingle>().CharacterIdle();
                    }
                }
            }
            else
            {
                if (item.tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControlSingle>().CharacterIdle();
                }
                else if (item.tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                {
                    item.GetComponent<CharacterControlSingle>().CharacterVictory();
                }

                if (item.tag == CharacterLayer.Ranger.ToString())
                {
                    if (item.transform.Find("Body").tag == CharacterLayer.Player.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControlSingle>().CharacterIdle();
                    }
                    else if (item.transform.Find("Body").tag == CharacterLayer.Enemy.ToString() && item.name != "Tower_Player" && item.name != "Tower_Enemy")
                    {
                        item.GetComponent<CharacterControlSingle>().CharacterVictory();
                    }
                }
            }
        }
    }

    public int OnResult()
    {
        int receive = 10;

        if (menuManager.selectHero.inforGame.lv <= GUI_Manager.instance.level)
        {
            menuManager.selectHero.inforGame.lv++;
        }

        if (menuManager.selectHero.inforGame.star[GUI_Manager.instance.level - 1] <= CalculatorStar(int.Parse(menuManager.singeManager.towerPlayer.tower.HP)))
        {
            menuManager.selectHero.inforGame.star[GUI_Manager.instance.level - 1] = CalculatorStar(int.Parse(menuManager.singeManager.towerPlayer.tower.HP));
        }

        InforGame infor = new InforGame();
        infor.lv = menuManager.selectHero.inforGame.lv;
        infor.lvTower = menuManager.selectHero.inforGame.lvTower;
        infor.star = menuManager.selectHero.inforGame.star;

        if (menuManager.selectHero.faction == "LOL")
        {
            SaveLoadManager.SaveInfor(infor, "LOL");
        }
        else
        {
            SaveLoadManager.SaveInfor(infor, "DOTA");
        }

        receive = GUI_Manager.instance.level * 20;
        menuManager.guiManager.money += receive;

        menuManager.guiManager.SaveMoneyinGame(menuManager.guiManager.money);
        return receive;
    }

    public int CalculatorStar(int health)
    {
        int star = 1;

        if (health >= 90 && health <= 100)
        {
            star = 3;
        }
        else if (health >= 70 && health <= 89)
        {
            star = 2;
        }

        return star;
    }
}
