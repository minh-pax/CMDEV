using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MessageBox;


public class UIMessageBox : MonoBehaviour
{

    #region Dialog Button Control
    public class DialogBtnControl
    {
        public Transform tranformBtn;
        public Text text;
        public DialogResult result;
        public DialogBtnControl(Transform _tran, Text _labelText)
        {
            tranformBtn = _tran;
            text = _labelText;
        }
        public void Reset()
        {
            result = DialogResult.None;
            text.text = "";
            tranformBtn.gameObject.SetActive(false);
        }
        public void SetInfomation(DialogResult _result, int _textId, Vector3 _location)
        {
            tranformBtn.gameObject.SetActive(true);
            result = _result;
//            text.text = AvLocalizationManager.GetString(_textId);
            tranformBtn.localPosition = _location;
        }
		public void SetInfomation(DialogResult _result, string _text, Vector3 _location)
		{
			tranformBtn.gameObject.SetActive(true);
			result = _result;
			text.text = _text;
			tranformBtn.localPosition = _location;
		}
    }
    #endregion

	public GameObject	guiControlLocation;
	public GameObject btnClose;
    // content
	public Text contentMessage;
	public Text captionText;

    // location btn first save
    public Vector3 location1Btn;
    public Vector3 location2Btn;
    public Vector3 location3Btn;

    // current Result for three button
    private DialogBtnControl[] btnDialog = new DialogBtnControl[3];

    // control static
    private static List<MessageItem> items = new List<MessageItem>();
	

    //********************* Overide *********************//
    void Awake()
    {
		btnClose.SetActive(true);
		transform.localPosition = new Vector3(0, 0, 0);
		guiControlLocation.SetActive(false);
        for (int i = 0; i < 3; i++)
        {
            Transform _tran = guiControlLocation.transform.Find("Btn0" + (i + 1).ToString()); 
			Text _label = _tran.Find("staticText").GetComponent<Text>();
            btnDialog[i] = new DialogBtnControl(_tran, _label);
            btnDialog[i].Reset();
        }
            
    }
    
	public void OnClickedByName(string name)
	{
		switch (name)
		{
			case "Btn01":
                {
                    SoundManager.instance.PlaySingle(SoundEffect.CLICK);
                    OnBtnClick(0);
                    OnCloseClicked();
                }
				break;
				
			case "Btn02":
                {
                    SoundManager.instance.PlaySingle(SoundEffect.CLICK);
                    OnBtnClick(1);

                }
				break;
				
			case "Btn03":
                {
                    SoundManager.instance.PlaySingle(SoundEffect.CLICK);
                    OnBtnClick(2);
                }
				break;
		}
	}

    public void OnBtnClick(int i)
    {

        bool close = true;

        if (items[items.Count - 1].callback != null){
            close = items[items.Count - 1].callback(btnDialog[i].result);
    	}

        if (!close)
		{
			Debug.LogError("Not Close " + i);
            return;
		}

        items.RemoveAt(items.Count - 1);

		//Debug.LogError (close +" && "+ !CheckShowMessageDialog());
        if (close && !CheckShowMessageDialog())
			OnCloseClicked();
    }

    public bool CheckShowMessageDialog()
    {
		//Debug.LogError("item count " + items.Count);
        if (items.Count > 0)
        {
            MessageItem item = items[items.Count - 1];
			Show(item);
			//Debug.LogError("Need show " + item.message);
            return true;
        }

        return false;
    }


    #region simulator message same with .Net
    private void ResetMessageState()
    {
        for (int i = 0; i < 3; i++)
        {
            btnDialog[i].Reset();
        }
		btnClose.SetActive(true);
    }

    private void SetupDisplayButtons(MessageItem item)
    {
        ResetMessageState();
        switch (item.buttons)
        {
			
            case Buttons.OK:
				btnDialog[0].SetInfomation(DialogResult.Ok, "OK", location1Btn);
				btnClose.SetActive(false);
                break;

            case Buttons.OKCancel:
				btnDialog[0].SetInfomation(DialogResult.Ok, "OK",  new Vector3(location2Btn.x,location2Btn.y,location2Btn.z));
				btnDialog[1].SetInfomation(DialogResult.Cancel, "Cancel",location3Btn);
                break;

            case Buttons.AbortRetryIgnore:
				btnDialog[0].SetInfomation(DialogResult.Abort, "Abort", location3Btn);
				btnDialog[1].SetInfomation(DialogResult.Retry, "Retry", new Vector3(location3Btn.x, location2Btn.y, location2Btn.z));
				btnDialog[2].SetInfomation(DialogResult.Ignore, "Ignore", new Vector3(location3Btn.x, location2Btn.y, location2Btn.z));
                break;

            case Buttons.YesNoCancel:
                btnDialog[0].SetInfomation(DialogResult.Yes, "Yes", location3Btn);
                btnDialog[1].SetInfomation(DialogResult.No, "No", new Vector3(location3Btn.x, location2Btn.y, location2Btn.z));
                btnDialog[2].SetInfomation(DialogResult.Cancel, "Cancel", new Vector3(location3Btn.x, location2Btn.y, location2Btn.z));
                break;

            case Buttons.YesNo:
				btnDialog[2].SetInfomation(DialogResult.Yes, "Yes", new Vector3(location2Btn.x, location2Btn.y, location2Btn.z));
				btnDialog[1].SetInfomation(DialogResult.No, "No", location3Btn);
                break;

            case Buttons.RetryCancel:
				btnDialog[0].SetInfomation(DialogResult.Retry, "Retry", location2Btn);
				btnDialog[1].SetInfomation(DialogResult.Cancel, "Cancel", new Vector3(location2Btn.x, location2Btn.y, location2Btn.z));
                break;

            default:
                btnDialog[0].SetInfomation(DialogResult.None, 37, location1Btn);
                break;

        }
    }

    public void Show(Callback callback, string message, string caption, Buttons buttons, DefaultButton defaultButton)
    {
        //Debug.Log("Message box:" + caption + "-" + message);

        MessageItem item = new MessageItem
        {
            caption = caption,
            buttons = buttons,
            defaultButton = defaultButton,
            callback = callback
        };
		item.message = message;
		if (items.Count > 2)
		{
			items.RemoveAt(0);
		}
		items.Add(item);

		Show(item);
    }
	
    #endregion

	void Show(MessageItem item)
	{
		if (item == null) {
			Debug.LogError("WTF??Message item null");
			return;
		}

		gameObject.SetActive (true);
		guiControlLocation.SetActive(true);

		
		SetupDisplayButtons(item);
		contentMessage.text = item.message;
		captionText.text = item.caption;

		guiControlLocation.transform.localPosition = new Vector3(0, 0, 0f);
		//Debug.Log("Show that ne:" + item.message);
	}
		
	public void OnCloseClicked(bool isClick=false)
	{
        if (isClick)
            SoundManager.instance.PlaySingle(SoundEffect.CLICK);

        guiControlLocation.SetActive(false);
	}

}
