﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour
{
    public ConfigTower.Row tower;
    public string maxHealth;

    // Use this for initialization
    public void InitInfoTower(string lvTower)
    {
        TextAsset configAsset = Resources.Load("Config/ConfigTower") as TextAsset;
        ConfigTower config = new ConfigTower();
        config.Load(configAsset);
        tower = config.Find_Level(lvTower);

        maxHealth = tower.HP;
    }
}
