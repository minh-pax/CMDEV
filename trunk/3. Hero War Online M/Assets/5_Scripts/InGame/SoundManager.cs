﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public MenuManager menuManager;
    public AudioSource efxSource;                   
    public AudioSource musicSource;
    public static SoundManager instance = null;                
    public float lowPitchRange = .95f;             
    public float highPitchRange = 1.05f;

    public float volumeEffice = 1;
    public float volumeMusic = 0.3f;

    public AudioClip audioClick;
    public AudioClip audioEnter;
    public AudioClip audioMenu;
    public AudioClip audioInGame;
    public AudioClip audioRain;
    public AudioClip audioSnow;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        volumeEffice = menuManager.guiManager.option.sfx;
        volumeMusic = menuManager.guiManager.option.music;
        OnSoundManager();
        PlayMulti(SoundEffect.MENU);
    }
    
   public void OnSoundManager()
    {
        efxSource.volume = volumeEffice;
        musicSource.volume = volumeMusic;
    }

    public void PlaySingle(SoundEffect sound)
    {
        efxSource.clip = PlaySound(sound);
        efxSource.Play();
    }

    public void PlayMulti(SoundEffect sound)
    {
        musicSource.clip = PlaySound(sound);
        musicSource.Play();
    }

    public void RandomizeSfx(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);

        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        efxSource.pitch = randomPitch;

        efxSource.clip = clips[randomIndex];

        efxSource.Play();
    }

    public AudioClip PlaySound(SoundEffect sound)
    {
        AudioClip audioClip = new AudioClip();

        switch (sound)
        {
            case SoundEffect.CLICK:
                audioClip = audioClick;
                break;
            case SoundEffect.ENTER:
                audioClip = audioEnter;
                break;
            case SoundEffect.MENU:
                audioClip = audioMenu;
                break;
            case SoundEffect.INGAME:
                audioClip = audioInGame;
                break;
            case SoundEffect.RAIN:
                audioClip = audioRain;
                break;
            case SoundEffect.SNOW:
                audioClip = audioSnow;
                break;
        }

        return audioClip;
    }
}
