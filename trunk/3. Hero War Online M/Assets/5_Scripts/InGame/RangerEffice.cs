﻿using UnityEngine;
using System.Collections;

public class RangerEffice : MonoBehaviour {

    public CharacterLayer target;
    public Vector3 pos;

    void Start()
    {
        transform.localPosition = pos;
    }

    void FixedUpdate()
    {
        gameObject.transform.Translate(new Vector3(0, 0, 0.1f));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == target.ToString())
        {
            Destroy(gameObject);
        }
    }
}
