﻿using UnityEngine;
using System.Collections;

public class Lane : MonoBehaviour {

    public bool check = true;

	public void PointEnter()
    {
         if (check)
         {
             GetComponent<MeshRenderer>().material.color = new Color(204, 204, 204, 0.5f);
         }
         else
         {
             GetComponent<MeshRenderer>().material.color = new Color(6, 111, 255, 0.5f);
         }
    }

    public void PointExit()
    {
        GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 0);
    }
}
