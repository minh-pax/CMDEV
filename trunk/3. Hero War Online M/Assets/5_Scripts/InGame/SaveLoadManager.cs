﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

class SaveLoadManager
{
    public static void SaveOption(OptionGame option)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/option.dat", FileMode.Create);

        OptionData data = new OptionData(option);

        bf.Serialize(stream, data);
        stream.Close();
    }

    public static OptionData LoadOption()
    {
        OptionData data = new OptionData();

        if (File.Exists(Application.persistentDataPath + "/option.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/option.dat", FileMode.Open);

            data = bf.Deserialize(stream) as OptionData;

            stream.Close();
            return data;
        }

        return data;
    }

    public static void SaveInfor(InforGame inforGame, string name)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/" + name + ".dat", FileMode.Create);

        InforData data = new InforData(inforGame);

        bf.Serialize(stream, data);
        stream.Close();
    }

    public static InforData LoadInfor(string name)
    {
        InforData data = new InforData();

        if (File.Exists(Application.persistentDataPath + "/" + name + ".dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/" + name + ".dat", FileMode.Open);

            data = bf.Deserialize(stream) as InforData;

            stream.Close();
            return data;
        }

        return data;
    }

    public static void SaveMoney(MoneyGame money)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/money.dat", FileMode.Create);

        MoneyData data = new MoneyData(money);

        bf.Serialize(stream, data);
        stream.Close();
    }

    public static MoneyData LoadMoney()
    {
        MoneyData data = new MoneyData();

        if (File.Exists(Application.persistentDataPath + "/money.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/money.dat", FileMode.Open);

            data = bf.Deserialize(stream) as MoneyData;

            stream.Close();
            return data;
        }

        return data;
    }
}

[Serializable]
public class OptionData
{
    public float sfx;
    public float music;
    public string language;

    public OptionData(OptionGame option)
    {
        sfx = option.sfx;
        music = option.music;
        language = option.language;
    }

    public OptionData()
    {

    }
}

[Serializable]
public class InforData
{
    public int lv;
    public List<int> star = new List<int>();
    public int lvTower;

    public InforData(InforGame inforGame)
    {
        lv = inforGame.lv;
        star = inforGame.star;
        lvTower = inforGame.lvTower;
    }

    public InforData()
    {
         
    }
}

[Serializable]
public class MoneyData
{
    public int money;

    public MoneyData(MoneyGame moneyGame)
    {
        money = moneyGame.money;
    }

    public MoneyData()
    {

    }
}
