﻿  using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class MenuManager : MonoBehaviour
{
    public GameObject Menu_TopCenter;
    public GameObject Menu_Center;
    public GameObject Option_Script;
    public GameObject Option_Center;
    public GameObject Option_TopLeft;
    public GameObject Credits_Center;
    public GameObject Credits_TopLeft;
    public GameObject SelectFaction_Center;
    public GameObject SelectFaction_TopLeft;
    public GameObject SelectHero_Script;
    public GameObject SelectHero_TopLeft;
    public GameObject SelectHero_TopRight;
    public GameObject SelectHero_HeroCenter;
    public GameObject SelectHero_TowerCenter;
    public GameObject SelectHero_Bottom;
    public GameObject Match_Center;
    public GameObject Waitting_Center;
    public GameObject Waitting_TopLeft;
    public GameObject SelectMap_Center;
    public GameObject SelectMap_Script;
    public GameObject SelectMap_TopLeft;
    public GameObject MapMultiPlayer_Script;
    public GameObject MapMultiPlayer_PlayerTopLeft;
    public GameObject MapMultiPlayer_EnemyTopRight;
    public GameObject MapMultiPlayer_Bottom;
    public GameObject MapSinglePlayer_Script;
    public GameObject MapSinglePlayer_PlayerTopLeft;
    public GameObject MapSinglePlayer_EnemyTopRight;
    public GameObject MapSinglePlayer_Bottom;
    public GameObject UIMessageBox_Center;
    public GameObject EndWaitting_Center;

    public GUI_Manager guiManager;
    public SelectHero selectHero;
    public Option option;
    public ControlMulti controlMulti;
    public MatchGame matchGame;
    public SceneLoader sceneLoader;
    public SingeManager singeManager;
    public MultiManager multiManager;
    public SelectMap selectMap;
    public ControlSingleManager controlSingleManager;
    public GoogleAdmob googleAdmob;

    void OnEnable()
    {   
        ObjMenu(true);
    }

    public void OpenMenu()
    {
        ObjMenu(true);
        ObjSelectFaction(false);
        ObjSelectHero(false);
        ObjHero(false);
        ObjTower(false);
        ObjOption(false);
        ObjCredits(false);
        ObjMapMultiPlayer(false);
        ObjMapSinglePlayer(false);
        ObjSelectMap(false);
        guiManager.SetInforGame();
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);

        googleAdmob.InitAdmob();
    }

    public void OpenResetMenu()
    {
        ObjMenu(true);
        ObjSelectFaction(false);
        ObjSelectHero(false);
        ObjHero(false);
        ObjTower(false);
        ObjOption(false);
        ObjCredits(false);
        ObjMapMultiPlayer(false);
        ObjMapSinglePlayer(false);
        ObjEndWaitting(false);
        ObjWaitting(false);
        sceneLoader.RemoveData();
        multiManager.RemoveData();
        singeManager.RemoveData();
        asyncMenu = null;
        guiManager.SetInforGame();
        SoundManager.instance.PlayMulti(SoundEffect.MENU);
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenSelectFaction(GameObject obj)
    {
        ObjSelectFaction(true);
        ObjMenu(false);
        selectHero.statusGame = obj.name;
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenOption()
    {
        ObjOption(true);
        ObjMenu(false);
        option.SetOption();
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenCredits()
    {
        ObjCredits(true);
        ObjMenu(false);
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenHero(GameObject obj)
    {
        InforData data;

        if (obj.name == "Red")
        {
            selectHero.faction = "LOL";
            data = guiManager.lol;
        }
        else
        {
            selectHero.faction = "DOTA";
            data = guiManager.dota;
        }

        if (selectHero.statusGame == "Single")
        {
            ObjSelectMap(true);
            selectMap.Init(data);
        }
        else
        {
            ObjSelectHero(true);
            ObjHero(true);
            ObjTower(false);
            selectHero.SetSelectHero();
        }

        ObjSelectFaction(false);
       
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenSelectHero()
    {
        ObjHero(true);
        ObjTower(false);
        ObjMatch(false);
        ObjWaitting(false);
        selectHero.SetSelectHero();
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenSelectHeroSingle()
    {
        ObjSelectHero(true);
        ObjHero(true);
        ObjTower(false);
        ObjMatch(false);
        ObjWaitting(false);
        ObjSelectMap(false);
        selectHero.SetSelectHero();
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenSelectTower()
    {
        ObjTower(true);
        ObjHero(false);
        selectHero.ClearPositionHero();
        selectHero.ClearIconHero();
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenMatch()
    {
        ObjMatch(true);
        ObjWaitting(false);
        selectHero.ClearPositionHero();
    }

    public void OpenWaitting()
    {
        ObjWaitting(true);
        ObjMatch(false);
    }

    public void CancelWaitting()
    {
        ObjSelectHero(true);
        ObjHero(true);
        ObjTower(false);
        ObjWaitting(false);
        controlMulti.LeaveMatchGame();
        selectHero.SetSelectHero();
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenMapMultiPlayer()
    {
        ObjMapMultiPlayer(true);
        ObjWaitting(false);
        ObjSelectHero(false);
        ObjHero(false);
        ObjTower(false);
        multiManager.InitMultiPlayerGame();
    }

    public void OpenEndWaitting()
    {
        ObjEndWaitting(true);
        ObjSelectMap(false);
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void CancelEndWaitting()
    {
        ObjEndWaitting(false);
        ObjMapSinglePlayer(true);
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OpenSceneGame()
    {
        StartCoroutine(LoadNewMenu(1));
        ObjEndWaitting(true);
        checkMenu = true;
        ObjSelectHero(false);
        ObjHero(false);
        ObjTower(false);
        selectHero.ClearPositionHero();
    }

    public void OpenMapSinglePlayer()
    {
        ObjMapSinglePlayer(true);
        ObjWaitting(false);
        ObjSelectHero(false);
        ObjHero(false);
        ObjTower(false);
        ObjEndWaitting(false);
        asyncMenu = null;
        singeManager.InitSinglePlayerGame();

        googleAdmob.LoadAdmob();
    }

    public void OpenResutl(string result, string user, int receive)
    {
        guiManager.ShowMessage(string.Format(AvUIManager.ChangeLanguage(result), user, receive), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, res =>
        {
            if (res == MessageBox.DialogResult.Ok)
            {
                StartCoroutine(LoadNewMenu(0));
                ObjEndWaitting(true);
                checkMenu = true;
            }

            return true;
        });
    }

    public void OpenQuitSingleGame()
    {
        selectHero.statusGame = "Multi";
        guiManager.ShowMessage(AvUIManager.ChangeLanguage("42"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.YesNo, res =>
        {
            if (res == MessageBox.DialogResult.Yes)
            {
                StartCoroutine(LoadNewMenu(0));
                ObjEndWaitting(true);
                controlSingleManager.StatusHero(false);
                checkMenu = true;
            }
            else
            {
                singeManager.StopCreateHeroComputer(true);
            }

            googleAdmob.LoadAdmob();

            return true;
        });
    }

    public void OpenQuitMultiGame()
    {
        guiManager.ShowMessage(AvUIManager.ChangeLanguage("42"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.YesNo, res =>
        {
            if (res == MessageBox.DialogResult.Yes)
            {
                Dictionary<string, string> dataResult = new Dictionary<string, string>();
                dataResult["name"] = controlMulti.dataEnemy["name"];
                dataResult["device"] = controlMulti.dataEnemy["device"];
                controlMulti.WinLoseGame(dataResult);
            }

            googleAdmob.LoadAdmob();

            return true;
        });
    }

    public void OnApplication()
    {
        Dictionary<string, string> dataResult = new Dictionary<string, string>();
        dataResult["name"] = controlMulti.dataPlayer["name"];
        dataResult["device"] = controlMulti.dataPlayer["device"];
        controlMulti.WinLoseGame(dataResult);
    }

    private AsyncOperation asyncMenu;
    private bool checkMenu = false;

    public IEnumerator LoadNewMenu(int scene)
    {
        yield return new WaitForSeconds(1);
        asyncMenu = SceneManager.LoadSceneAsync(scene);
    }

    void Update()
    {
        sceneLoader.onLoad();
        SoundManager.instance.OnSoundManager();
        OnMenu();
    }

    public void OnMenu()
    {
        if (asyncMenu != null)
        {
            if (asyncMenu.isDone)
            {
                if (checkMenu)
                {
                    if (selectHero.statusGame == "Single")
                    {
                        OpenMapSinglePlayer();
                    }
                    else
                    {
                        OpenResetMenu();
                    }

                    checkMenu = false;
                }
            }
        }
    }

    private void ObjMenu(bool check)
    {
        Menu_TopCenter.SetActive(check);
        Menu_Center.SetActive(check);
    }

    private void ObjOption(bool check)
    {
        Option_Script.SetActive(check);
        Option_Center.SetActive(check);
        Option_TopLeft.SetActive(check);
    }

    private void ObjCredits(bool check)
    {
        Credits_Center.SetActive(check);
        Credits_TopLeft.SetActive(check);
    }

    private void ObjSelectFaction(bool check)
    {
        SelectFaction_Center.SetActive(check);
        SelectFaction_TopLeft.SetActive(check);
    }

    private void ObjSelectMap(bool check)
    {
        SelectMap_Script.SetActive(check);
        SelectMap_Center.SetActive(check);
        SelectMap_TopLeft.SetActive(check);
    }

    private void ObjSelectHero(bool check)
    {
        SelectHero_Script.SetActive(check);
        SelectHero_TopLeft.SetActive(check);
        SelectHero_TopRight.SetActive(check);
    }

    private void ObjMatch(bool check)
    {
        Match_Center.SetActive(check);
    }

    private void ObjWaitting(bool check)
    {
        Waitting_Center.SetActive(check);
        Waitting_TopLeft.SetActive(check);
    }

    private void ObjMapMultiPlayer(bool check)
    {
        MapMultiPlayer_Script.SetActive(check);
        MapMultiPlayer_PlayerTopLeft.SetActive(check);
        MapMultiPlayer_EnemyTopRight.SetActive(check);
        MapMultiPlayer_Bottom.SetActive(check);
    }

    private void ObjMapSinglePlayer(bool check)
    {
        MapSinglePlayer_Script.SetActive(check);
        MapSinglePlayer_PlayerTopLeft.SetActive(check);
        MapSinglePlayer_EnemyTopRight.SetActive(check);
        MapSinglePlayer_Bottom.SetActive(check);
    }

    private void ObjHero(bool check)
    {
        SelectHero_HeroCenter.SetActive(check);
        SelectHero_Bottom.SetActive(check);
    }

    private void ObjTower(bool check)
    {
        SelectHero_TowerCenter.SetActive(check);
    }

    private void ObjUIMessageBox(bool check)
    {
        UIMessageBox_Center.SetActive(check);
    }

    private void ObjEndWaitting(bool check)
    {
        EndWaitting_Center.SetActive(check);
    }
}
