﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CharacterAnim
{
    idle,
    run,
    attack_1,
    attack_2,
    die,
    victory,
}

public enum Factions
{
    LOL,
    DOTA,
}

public enum CharacterLayer
{
    Player = 10,
    Enemy = 11,
    Tank = 19,
    Ranger = 20,
}

public enum LOL
{
    Darius = 1,
    Ashe = 2,
    MasterYi = 3,
    Morgana = 4,
    Pantheon = 5,
    Twitch = 6,
    Hecarim = 7,
    Sejiuani = 8,
}

public enum DOTA
{
    Axe = 1,
    DrowRanger = 2,
    Juggernaut = 3,
    QueenOfPain = 4,
    DragonKnight = 5,
    Sniper = 6,
    CentaurWarrunner = 7,
    Disruptor = 8, 
}

public class Room
{
    public string player1;
    public string player2;
    public string room;
}

public enum SoundEffect
{
    CLICK,
    ENTER,
    MENU,
    INGAME,
    RAIN,
    SNOW,
}

public class InforGame
{
    public int lv;
    public List<int> star = new List<int>();
    public int lvTower;
}

public class OptionGame
{
    public float sfx;
    public float music;
    public string language;
}

public class MoneyGame
{
    public int money;
}

public static class AvUIManager
{
    public static string version = "1.2";

    public static ConfigString_Normal LoadString_Normal()
    {
        TextAsset config = Resources.Load("Config/ConfigString_Normal") as TextAsset;
        ConfigString_Normal csv = new ConfigString_Normal();
        csv.Load(config);

        return csv;
    }

    public static string ChangeLanguage(string stringID)
    {
        string text = "";
        string language = PlayerPrefs.GetString("Language");

        switch (language)
        {
            case "English":
                text = AvUIManager.LoadString_Normal().Find_ID(stringID).EN;
                break;
            case "VietNam":
                text = AvUIManager.LoadString_Normal().Find_ID(stringID).VN;
                break;
            default:
                text = AvUIManager.LoadString_Normal().Find_ID(stringID).EN;
                break;
        }

        return text;
    }

    public static ConfigHero.Row OnHero(string name)
    {
        ConfigHero.Row hero;

        TextAsset configHeroAsset = Resources.Load("Config/ConfigHero") as TextAsset;
        ConfigHero configHero = new ConfigHero();
        configHero.Load(configHeroAsset);
        hero = configHero.Find_Name(name);

        return hero;
    }

    public static string OnHeroLOL(int index)
    {
        string hero = LOL.Darius.ToString();

        switch (index)
        {
            case (int)LOL.Darius:
                hero = LOL.Darius.ToString();
                break;
            case (int)LOL.Ashe:
                hero = LOL.Ashe.ToString();
                break;
            case (int)LOL.MasterYi:
                hero = LOL.MasterYi.ToString();
                break;
            case (int)LOL.Morgana:
                hero = LOL.Morgana.ToString();
                break;
            case (int)LOL.Pantheon:
                hero = LOL.Pantheon.ToString();
                break;
            case (int)LOL.Twitch:
                hero = LOL.Twitch.ToString();
                break;
            case (int)LOL.Hecarim:
                hero = LOL.Hecarim.ToString();
                break;
            case (int)LOL.Sejiuani:
                hero = LOL.Sejiuani.ToString();
                break;
        }

        return hero;
    }

    public static string OnHeroDOTA(int index)
    {
        string hero = DOTA.Axe.ToString();

        switch (index)
        {
            case (int)DOTA.Axe:
                hero = DOTA.Axe.ToString();
                break;
            case (int)DOTA.DrowRanger:
                hero = DOTA.DrowRanger.ToString();
                break;
            case (int)DOTA.Juggernaut:
                hero = DOTA.Juggernaut.ToString();
                break;
            case (int)DOTA.QueenOfPain:
                hero = DOTA.QueenOfPain.ToString();
                break;
            case (int)DOTA.DragonKnight:
                hero = DOTA.DragonKnight.ToString();
                break;
            case (int)DOTA.Sniper:
                hero = DOTA.Sniper.ToString();
                break;
            case (int)DOTA.CentaurWarrunner:
                hero = DOTA.CentaurWarrunner.ToString();
                break;
            case (int)DOTA.Disruptor:
                hero = DOTA.Disruptor.ToString();
                break;
        }

        return hero;
    }

    public static float DeplayAttack(string name)
    {
        float time = 0;

        switch (name)
        {
            //LOL
            case "Darius":
                time = 1.14f;
                break;
            case "Ashe":
                time = 2.13f;
                break;            
            case "MasterYi":
                time = 2f;
                break;
            case "Morgana":
                time = 1.01f;
                break;
            case "Pantheon":
                time = 1.05f;
                break;
            case "Twitch":
                time = 1.05f;
                break;
            case "Hecarim":
                time = 2.1f;
                break;
            case "Sejiuani":
                time = 1.19f;
                break;
            //DOTA
            case "Axe":
                time = 1.11f;
                break;
            case "DrowRanger":
                time = 1.22f;
                break;
            case "Juggernaut":
                time = 1.05f;
                break;
            case "QueenOfPain":
                time = 1.05f;
                break;
            case "DragonKnight":
                time = 1.08f;
                break;
            case "Sniper":
                time = 1.06f;
                break;
            case "CentaurWarrunner":
                time = 1f;
                break;
            case "Disruptor":
                time = 1.06f;
                break;
        }

        return time;
    }

    public static float TimeEffice(string name)
    {
        float time = 0;

        switch (name)
        {
            //LOL
            case "Darius":
                time = 0.7f;
                break;
            case "Ashe":
                time = 0.5f;
                break;
            case "MasterYi":
                time = 0.8f;
                break;
            case "Morgana":
                time = 0.6f;
                break;
            case "Pantheon":
                time = 0.6f;
                break;
            case "Twitch":
                time = 0.7f;
                break;
            case "Hecarim":
                time = 0.7f;
                break;
            case "Sejiuani":
                time = 0.5f;
                break;
           //DOTA
            case "Axe":
                time = 0.7f;
                break;
            case "DrowRanger":
                time = 0.7f;
                break;
            case "Juggernaut":
                time = 0.6f;
                break;
            case "QueenOfPain":
                time = 0.6f;
                break;
            case "DragonKnight":
                time = 0.7f;
                break;
            case "Sniper":
                time = 0.7f;
                break;
            case "CentaurWarrunner":
                time = 0.19f;
                break;
            case "Disruptor":
                time = 1.39f;
                break;
        }

        return time;
    }
}
