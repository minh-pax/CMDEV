﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SelectHero : MonoBehaviour
{
    public GameObject groupButtonImage;
    public GameObject groupButtonIcon;
    public GameObject posHero;
    
    public Sprite[] centerDOTAImages;
    public Sprite[] centerLOLImages;
    public Sprite[] bottomDOTAIcon;
    public Sprite[] bottomLOLIcon;
    public List<Sprite> listIcon;

    public ConfigTower.Row lvNormalTower;
    public ConfigTower.Row lvUpdateTower;
    public MenuManager menuManager;

    public Text txtLvTower;
    public Text txtPoint;
    public Text txtLvMap;
    public Text txtMoney;
    public Text txtPrice;

    public Level txtLvNormalTower;
    public Level txtlvUpdateTower;

    public Sprite[] selectHeroImage;
    public Sprite[] selectHeroIcon;

    public string faction;
    public string statusGame;

    public InforData inforGame;
    public int randomMap;

    private Sprite selectIcon;
    private int maxHero = 4;
    private int maxSlot = 3;
    private bool checkIcon = true;
   
    void OnEnable()
    {
        GameObject obj = GameObject.Find("PosHero");
        posHero = obj;

        listIcon = new List<Sprite>();
    }

    public void SetSelectHero()
    {
        if (faction == "LOL")
            inforGame = menuManager.guiManager.lol;
        else
            inforGame = menuManager.guiManager.dota;

        ShowUI();

        if (faction == "LOL")
        {
            selectHeroImage = centerLOLImages;
            selectHeroIcon = bottomLOLIcon;
            LoadCenterImage(maxHero, selectHeroImage, maxSlot);
        }
        else
        {
            selectHeroImage = centerDOTAImages;
            selectHeroIcon = bottomDOTAIcon;
            LoadCenterImage(maxHero, selectHeroImage, maxSlot);
        }
    }

    public void ShowUI()
    {
        lvNormalTower = GUI_Manager.instance.configTower.Find_Level(inforGame.lvTower.ToString());
        lvUpdateTower = GUI_Manager.instance.configTower.Find_Level((inforGame.lvTower + 1).ToString());

        txtLvNormalTower.Init(lvNormalTower);
        txtlvUpdateTower.Init(lvUpdateTower);

        maxHero = int.Parse(lvNormalTower.Hero);
        maxSlot = int.Parse(lvNormalTower.Slot);

        txtLvMap.text = inforGame.lv.ToString();
        txtLvTower.text = AvUIManager.ChangeLanguage("13") + " " + lvNormalTower.Level;
        txtPoint.text = lvNormalTower.Point;
        txtMoney.text = menuManager.guiManager.money.ToString();
        txtPrice.text = lvUpdateTower.Price.ToString();
    }

    public void LoadCenterImage(int countImage, Sprite[] listImage, int countIcon)
    {
        for (int i = 0; i < countImage; i++)
        {
            ItemHandler item = groupButtonImage.transform.GetChild(i).GetComponent<ItemHandler>();
            item.InitHero(listImage[i], false);
        }

        for (int i = 0; i < countIcon; i++)
        {
            ItemHandler item = groupButtonIcon.transform.GetChild(i).GetComponent<ItemHandler>();
            item.InitIcon(false, "");
        }
    }

    public void CreateHero(GameObject obj)
    {
        string[] name = obj.name.Split('_');

        if (listIcon.Count < maxSlot && int.Parse(name[1]) <= maxHero)
        {
            ClearPositionHero();

            string[] number = obj.name.Split('_');
            string[] nameHero = selectHeroImage[int.Parse(number[1]) - 1].name.Split('_');

            string link = "Prefabs/" + faction.ToString() + "/" + nameHero[3];
            GameObject objHero = Instantiate(Resources.Load(link) as GameObject);

            if (objHero != null)
            {
                objHero.transform.SetParent(posHero.transform);
                objHero.transform.localPosition = Vector3.zero;
                objHero.transform.localRotation = Quaternion.Euler(0, 105, 0);
                objHero.transform.localScale = new Vector3(12, 12, 12);
                objHero.GetComponent<CharacterControl>().enabled = false;
                objHero.GetComponent<CharacterControlSingle>().enabled = false;
                objHero.GetComponent<CharacterMove>().enabled = false;
                objHero.GetComponent<CharacterAnimation>().PlayOneAnimation(CharacterAnim.idle.ToString());
                objHero.name = nameHero[3];

                selectIcon = selectHeroIcon[int.Parse(number[1]) - 1];

                if (listIcon.Count > 0)
                {
                    for (int i = 0; i < listIcon.Count; i++)
                    {
                        if (listIcon[i].name == selectIcon.name)
                        {
                            checkIcon = false;
                            break;
                        }
                    }
                }

                if (checkIcon)
                {
                    listIcon.Add(selectIcon);
                    ItemHandler item = groupButtonIcon.transform.GetChild(listIcon.Count - 1).GetComponent<ItemHandler>();
                    item.InitIcon(selectIcon, false, AvUIManager.OnHero(objHero.name).Price);
                }
                else
                {
                    checkIcon = true;
                }
            }
        }

        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void ClearPositionHero()
    {
        if (posHero.transform.childCount > 0)
        {
            Destroy(posHero.transform.GetChild(0).gameObject);
        }
    }

    public void ClearIconHero()
    {
        if (listIcon.Count > 0)
        {
            for (int i = 0; i < listIcon.Count; i++)
            {
                ItemHandler item = groupButtonIcon.transform.GetChild(i).GetComponent<ItemHandler>();
                item.InitIcon(item.normalIcon, false, "");
            }

            listIcon.Clear();
        }
    }

    public void RemoveIcon(GameObject obj)
    {
        string[] number = obj.name.Split('_');

        if (listIcon.Count > 0)
        {
            if (int.Parse(number[1]) <= maxSlot)
            {
                for (int i = 0; i < listIcon.Count; i++)
                {
                    ItemHandler item = groupButtonIcon.transform.GetChild(i).GetComponent<ItemHandler>();
                    item.InitIcon(item.normalIcon, false, "");
                }

                listIcon.RemoveAt(int.Parse(number[1]) - 1);

                for (int i = 0; i < listIcon.Count; i++)
                {
                    ItemHandler item = groupButtonIcon.transform.GetChild(i).GetComponent<ItemHandler>();
                    item.InitIcon(listIcon[i], false, AvUIManager.OnHero(listIcon[i].name.Split('_')[3]).Price);
                }
            }
        }

        ClearPositionHero();
        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void BuyTower()
    {
        if (menuManager.guiManager.money >= int.Parse(lvUpdateTower.Price))
        {
            menuManager.guiManager.ShowMessage(string.Format(AvUIManager.ChangeLanguage("21"), lvUpdateTower.Price), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.YesNo, res => {
                if (res == MessageBox.DialogResult.Yes)
                {
                    int old = menuManager.guiManager.money;
                    int current = menuManager.guiManager.money - int.Parse(lvUpdateTower.Price);

                    inforGame.lvTower++;
                    menuManager.guiManager.money -= int.Parse(lvUpdateTower.Price);

                    OnSaveTower();

                    Hashtable ht = new Hashtable();
                    ht.Add("from", old);
                    ht.Add("to", current);
                    ht.Add("time", 2f);
                    ht.Add("onupdate", "OnChange");
                    iTween.ValueTo(gameObject, ht);
                    iTween.PunchScale(txtMoney.gameObject, new Vector3(0.8f, 0.8f, 0.8f), 1);

                    ShowUI();
                }
                if (res == MessageBox.DialogResult.No)
                    Debug.Log("Click No ne");

                return true;
            });
        }
        else
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("22") + ", " + AvUIManager.ChangeLanguage("23"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, null);
        }
    }

    public void OnChange(float value)
    {
        txtMoney.text = value.ToString();
    }

    public void MatchGame()
    {
        if (listIcon.Count > 0)
        {
            if (statusGame == "Single")
            {
                menuManager.OpenSceneGame();
            }
            else
            {
                if(GameObject.FindObjectOfType<SetIPGame>().checkMulti)
                menuManager.controlMulti.CheckVersion();
            }
        }
        else
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("32"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, null);
        }

        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OnSaveTower()
    {
        InforGame data = new InforGame();
        data.lv = inforGame.lv;
        data.star = inforGame.star;
        data.lvTower = inforGame.lvTower;

        if (faction == "LOL")
            SaveLoadManager.SaveInfor(data, "LOL");
        else
            SaveLoadManager.SaveInfor(data, "DOTA");

        menuManager.guiManager.SaveMoneyinGame(menuManager.guiManager.money);
    }
}
