﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterAnimation : MonoBehaviour {

    public GameObject effice;
    public bool checkAttackEnemy = false;

    private Animation playAnim;
    private string playName;
    private bool checkAnimation = false;
    private CharacterControlSingle characterControlSingle;
    private CharacterControl characterControl;
    private bool checkAttack = true;
    private AudioSource playSound;

    // Use this for initialization
    void Awake ()
    {
        playAnim = GetComponent<Animation>();
        characterControl = GetComponent<CharacterControl>();
        characterControlSingle = GetComponent<CharacterControlSingle>();
        playSound = GetComponent<AudioSource>();
    }

    public void PlayOneAnimation(string name)
    {
        if (playAnim != null)
        {
            playAnim.Play(name);
        }
    }

    public void PlayDealthAnimation(string name)
    {
        //dang choi single
        if (characterControl.isActiveAndEnabled)
        {
            characterControl.listTarget.Clear();
            characterControl.menuManager.multiManager.listHero.Remove(gameObject);
        }
        // dang choi multi
        if (characterControlSingle.isActiveAndEnabled)
        {
            characterControlSingle.listTarget.Clear();
            characterControlSingle.menuManager.singeManager.listHero.Remove(gameObject);
        }
        //neu la ranger 
        if (gameObject.tag == "Ranger")
            transform.Find("Body").GetComponent<BoxCollider>().enabled = false;

        GetComponent<BoxCollider>().enabled = false;
      
        playName = name;
        checkAnimation = true;
        playAnim.Play(playName);
    }


    public void PlayAttackAnimation(string name)
    {
        playName = name;
        playAnim.Play(playName);
    }

    public void OnAnimation()
    {
        if (checkAnimation)
        {
            if (!playAnim.IsPlaying(playName))
            {
                Destroy(gameObject);
                checkAnimation = false;
            }
        }

        //dang choi single
        if (characterControlSingle.isActiveAndEnabled && !checkAnimation)
        {
            if (characterControlSingle.listTarget.Count > 0)
            {
                characterControlSingle.CharacterStopMove();
                if (playAnim[playName].time >= AvUIManager.TimeEffice(gameObject.name.Split('_')[0]) && checkAttack)
                {
                    CreateEffice(characterControlSingle.listTarget[0]);
                    checkAttack = false;
                }

                if (!playAnim.IsPlaying(playName))
                {
                    PlayAttackAnimation(CharacterAnim.attack_1.ToString());
                    checkAttack = true;
                }
            }
            else
            {
                if (characterControlSingle.checkSub)
                {
                    characterControlSingle.CharacterMove();
                }
            }
        }

        // dang choi multi
        if (characterControl.isActiveAndEnabled && !checkAnimation)
        {
            if (characterControl.listTarget.Count > 0)
            {
                characterControl.CharacterStopMove();
                if (playAnim[playName].time >= AvUIManager.TimeEffice(gameObject.name.Split('_')[0]) && checkAttack)
                {
                    CreateEffice(characterControl.listTarget[0]);
                    checkAttack = false;
                }

                if (!playAnim.IsPlaying(playName))
                {
                    PlayAttackAnimation(CharacterAnim.attack_1.ToString());
                    checkAttack = true;
                }
            }
            else
            {
                if (characterControl.checkSub)
                {
                    characterControl.CharacterMove();
                }
            }
        }
    }

    public void CreateEffice(GameObject target)
    {
        if (target != null)
        {
            PlaySoundHero();
            GameObject obj = Instantiate(effice as GameObject);
            if (gameObject.tag == CharacterLayer.Ranger.ToString())
            {
                if (gameObject.name.Split('_')[0] != "Sniper" && gameObject.name.Split('_')[0] != "Disruptor"
                    && gameObject.name.Split('_')[0] != "Morgana" && gameObject.name.Split('_')[0] != "Twitch")
                    obj.transform.SetParent(gameObject.transform);
                else
                    obj.transform.SetParent(target.transform);
            }
            else
            {
                obj.transform.SetParent(target.transform);
                obj.transform.localPosition = new Vector3(0, 0, 0);
            }

            obj.transform.localRotation = Quaternion.Euler(0, 0, 0);
            obj.name = effice.name;
        }
        else
        {
            if (characterControlSingle.isActiveAndEnabled)
                characterControlSingle.listTarget.Remove(target);
            if (characterControl.isActiveAndEnabled)
                characterControl.listTarget.Remove(target);
            Debug.Log("Target bi banh xac roi sao tạo được nữa");
        }
    }

    public void PlaySoundHero()
    {
        playSound.volume = SoundManager.instance.volumeEffice / 3;
        playSound.Play();
    }
}
