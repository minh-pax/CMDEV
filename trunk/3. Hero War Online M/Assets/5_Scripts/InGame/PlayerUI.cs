﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

    public Image heath;
    public Text playerName;
    public Text txtHealth;
    private float maxHealth;
    public bool checkFillAmount;

    void OnEnable()
    {
        checkFillAmount = true;
    }

    public void Init()
    {
        if(checkFillAmount)
             heath.fillAmount = 1;
    }

	public void SubHeath(float current, float old, float max)
    {
        maxHealth = max;
        if (current >= 0)
        {
            Hashtable ht = new Hashtable();
            ht.Add("from", old);
            ht.Add("to", current);
            ht.Add("time", 2f);
            ht.Add("onupdate", "OnChange");
            iTween.ValueTo(gameObject, ht);
        }
        else
        {

            Hashtable ht = new Hashtable();
            ht.Add("from", 1);
            ht.Add("to", 0);
            ht.Add("time", 2f);
            ht.Add("onupdate", "OnChange");
            iTween.ValueTo(gameObject, ht);
        }
    }

    public void OnChange(float value)
    {
        heath.fillAmount = (value / maxHealth);
        txtHealth.text = (int)value + "/" + maxHealth;
    }
}
