﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public Text level;
    public Text hp;
    public Text point;
    public Text hero;
    public Text slot;

    public void Init(ConfigTower.Row normalLevel)
    {
        level.text = AvUIManager.ChangeLanguage("15") +" " + normalLevel.Level;
        hp.text = AvUIManager.ChangeLanguage("16") + ": " + normalLevel.HP;
        point.text = AvUIManager.ChangeLanguage("17") + ": " + normalLevel.Point;
        hero.text = AvUIManager.ChangeLanguage("18") + ": " + normalLevel.Hero;
        slot.text = AvUIManager.ChangeLanguage("19") + ": " + normalLevel.Slot;
    }
}
