﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyInGame : MonoBehaviour
{

    public Text txtLvMap;
    public Text txtLvTower;
    public Text txtPoint;
    public int point = 0;
    public int pointEnemy = 0;
    public MenuManager menuManager;

    private int old = 0;
    private int current = 0;
    private bool checkPoint = true;
    private bool checkPointComputer = true;

    public void Init(string lvMap, string lvTower)
    {
        point = 0;
        pointEnemy = 0;
        checkPointComputer = true;
        txtLvMap.text = GUI_Manager.instance.level.ToString();
        txtLvTower.text = lvTower.ToString();
        txtPoint.text = point.ToString();
    }

    void FixedUpdate()
    {
        if (checkPoint)
        {
            AddPoint();
            checkPoint = false;
        }

        if (checkPointComputer && menuManager.singeManager.isActiveAndEnabled)
        {
            StartCoroutine(AddPointComputer());
            checkPointComputer = false;
        }
    }

    IEnumerator AddPointComputer()
    {
        yield return new WaitForSeconds(1f);
        pointEnemy += int.Parse(menuManager.singeManager.towerEnemy.tower.Point);
        checkPointComputer = true;
    }

    public void AddPoint()
    {
        old = point;
        if (menuManager.singeManager.isActiveAndEnabled)
            current = old + int.Parse(menuManager.singeManager.towerPlayer.tower.Point);
        if (menuManager.multiManager.isActiveAndEnabled)
            current = old + int.Parse(menuManager.multiManager.towerPlayer.tower.Point);
        point = current;
       

        Hashtable ht = new Hashtable();
        ht.Add("from", old);
        ht.Add("to", current);
        ht.Add("time", 1f);
        ht.Add("onupdate", "OnChange");
        ht.Add("oncomplete", "OnComplete");
        iTween.ValueTo(gameObject, ht);
    }

    public void OnChange(float value)
    {
        int point = (int)value;
        txtPoint.text = point.ToString();
    }

    public void OnComplete()
    {
        checkPoint = true;
    }
}
