using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UILocalizeLabel : MonoBehaviour {

	public string stringID = "";
    // Use this for initialization
    void OnEnable()
    {
        Text txtLanguage = GetComponent<Text>();
        if (txtLanguage != null)
        {
            txtLanguage.text = AvUIManager.ChangeLanguage(stringID);
        }
	}
}
