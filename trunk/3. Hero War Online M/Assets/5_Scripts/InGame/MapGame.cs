﻿using UnityEngine;
using System.Collections;

public class MapGame : MonoBehaviour {

    public GameObject objTerranRain;
    public GameObject objTerranSnow;
    public GameObject objRain;
    public GameObject objSnow;

    public void StartRandom(int random)
    {
        objTerranRain.SetActive(false);
        objTerranSnow.SetActive(false);
        objRain.SetActive(false);
        objSnow.SetActive(false);

        if (random < 1 || random > 100 )
        {
            random = Random.Range(1, 100);
        }

        if (random >= 1 && random <= 30)
        {
            objTerranRain.SetActive(true);
            SoundManager.instance.PlayMulti(SoundEffect.INGAME);
        }
        else if (random > 30 && random <= 75)
        {
            objTerranRain.SetActive(true);
            objRain.SetActive(true);
            SoundManager.instance.PlayMulti(SoundEffect.RAIN);
        }
        else if (random > 75 && random <= 100)
        {
            objTerranSnow.SetActive(true);
            objSnow.SetActive(true);
            SoundManager.instance.PlayMulti(SoundEffect.SNOW);
        }
    }
}
