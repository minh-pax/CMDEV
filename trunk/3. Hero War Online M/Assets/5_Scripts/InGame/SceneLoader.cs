﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public MenuManager menuManager;
    public int scene;
    public Text loadingText;
    public bool checkFinish;
    public bool checkDis;
    public GameObject objBack;
    private bool checkInput;
    private bool loadScene;
    private bool checkSceneLoad;
    private AsyncOperation async;
/*
    void OnApplicationfocus(bool pauseStatus)
    {
        if (pauseStatus)
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("35"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, res =>
            {
                if (res == MessageBox.DialogResult.Ok)
                {
                    menuManager.controlMulti.LeaveMatchGame();
                    menuManager.OpenResetMenu();
                    SceneManager.LoadScene("Menu");
                }

                return true;
            });
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            menuManager.guiManager.ShowMessage(AvUIManager.ChangeLanguage("35"), AvUIManager.ChangeLanguage("24"), MessageBox.Buttons.OK, res =>
            {
                if (res == MessageBox.DialogResult.Ok)
                {
                    menuManager.controlMulti.LeaveMatchGame();
                    menuManager.OpenResetMenu();
                    SceneManager.LoadScene("Menu");
                }

                return true;
            });
        }
    }
 */   
    void OnEnable()
    {
        loadingText.text = AvUIManager.ChangeLanguage("36");
        loadScene = false;
        checkInput = true;
        checkSceneLoad = true;
        checkFinish = false;
        checkDis = true;
    }

    public void RemoveData()
    {
        loadingText.text = AvUIManager.ChangeLanguage("36");
        loadScene = false;
        checkInput = true;
        checkSceneLoad = true;
        checkFinish = false;
        checkDis = true;
        async = null;
    }

    public void onLoad()
    {
        if (checkSceneLoad)
        {
            if (loadScene == true)
            {
                loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
            }

            if (async != null)
            {
                if (async.isDone)
                {
                    menuManager.OpenMapMultiPlayer();
                    checkSceneLoad = false;
                }
            }
        }
    }

    public void GotoSceneGame()
    {
        loadScene = true;

        loadingText.text = AvUIManager.ChangeLanguage("37");

        objBack.SetActive(true);

        StartCoroutine(LoadNewScene());
    }

    public IEnumerator LoadNewScene()
    {
        yield return new WaitForSeconds(1);
        async = SceneManager.LoadSceneAsync(scene);
        async.allowSceneActivation = false;

        while (!async.isDone)
        {
            if (async.progress == 0.9f)
            {

                if (checkInput)
                {
                    menuManager.sceneLoader.loadingText.text = AvUIManager.ChangeLanguage("38");

                    if (Input.anyKeyDown)
                    {
                        
                        menuManager.controlMulti.AcceptGoToGame();
                        objBack.SetActive(false);
                        checkInput = false;
                    } 
                }
                else
                {
                    loadingText.text = AvUIManager.ChangeLanguage("39");
                    if (checkFinish)
                    {
                        async.allowSceneActivation = true;
                    }
                }        
            }
            yield return null;
        }
    }
}
