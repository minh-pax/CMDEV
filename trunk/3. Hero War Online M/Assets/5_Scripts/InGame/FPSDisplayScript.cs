﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSDisplayScript : MonoBehaviour
{
    float timeA;
    public int fps;
    public int lastFPS;
    public GUIStyle textStyle;
    private Text txt;

    public int frameRate = 50;
    // Use this for initialization
    void Start()
    {
        txt = GetComponent<Text>();
        timeA = Time.timeSinceLevelLoad;
        DontDestroyOnLoad(this);
    }

    void Awake()
    {
        Application.targetFrameRate = frameRate;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Time.timeSinceLevelLoad+" "+timeA);
        if (Time.timeSinceLevelLoad - timeA <= 1)
        {
            fps++;
        }
        else
        {
            lastFPS = fps + 1;
            timeA = Time.timeSinceLevelLoad;
            fps = 0;
        }

        if (txt != null)
            txt.text = frameRate.ToString();
    }
}
