﻿using UnityEngine;
using System.Collections;

public class CharacterMove : MonoBehaviour {

    public Transform tranPosRed;
    public Transform tranPosBlue;
    public bool checkMove = false;
    public bool swip = true;
    private float speed;

    // Update is called once per frame
    public void OnMove ()
    {
        if (checkMove)
        {
            if (GetComponent<CharacterControl>().isActiveAndEnabled)
            {
                speed = float.Parse(GetComponent<CharacterControl>().hero.Speed);
            }
            if (GetComponent<CharacterControlSingle>().isActiveAndEnabled)
            {
                speed = float.Parse(GetComponent<CharacterControlSingle>().hero.Speed);
            }

            if (swip)
            {
                transform.Translate(Vector3.forward * Time.deltaTime * speed);
            }
            else
            {
                transform.Translate(-(Vector3.forward * Time.deltaTime * speed));
            }
        }
    }
}
