﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Option : MonoBehaviour
{
    public MenuManager menuManager;
    public Slider sfx, music;
    public Toggle onLanguage, offLanguage;

    public void SetOption()
    {
        SoundManager.instance.volumeEffice = menuManager.guiManager.option.sfx;
        SoundManager.instance.volumeMusic = menuManager.guiManager.option.music;

        sfx.value = SoundManager.instance.volumeEffice;
        music.value = SoundManager.instance.volumeMusic * 3;

        if (PlayerPrefs.GetString("Language") == "English")
        {
            onLanguage.isOn = true;
            offLanguage.isOn = false;
        }
        else
        {
            onLanguage.isOn = false;
            offLanguage.isOn = true;
        }
    }

    public void OnSound(GameObject obj)
    {
        if (obj.name == "SliderSFX")
        {
            SoundManager.instance.volumeEffice = obj.GetComponent<Slider>().value;
        }
        else if (obj.name == "SliderMusic")
        {
            SoundManager.instance.volumeMusic = obj.GetComponent<Slider>().value / 3;
        }
    }

    public void OnLanguage(GameObject obj)
    {
        if (obj.name == "VietNam")
        {
            menuManager.guiManager.option.language = "VietNam";
        }
        else if (obj.name == "English")
        {
            menuManager.guiManager.option.language = "English";
        }
        PlayerPrefs.SetString("Language", menuManager.guiManager.option.language);

        SoundManager.instance.PlaySingle(SoundEffect.CLICK);
    }

    public void OnSaveOption()
    {
        OptionGame data = new OptionGame();
        data.sfx = SoundManager.instance.volumeEffice;
        data.music = SoundManager.instance.volumeMusic;
        data.language = menuManager.guiManager.option.language;
        SaveLoadManager.SaveOption(data);

        GUI_Manager.instance.SetOption();
    }
}
