﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	public float moveSpeed = 15.0f;
	public float leftBound = -9.5f;
	public float rightBound = 12.42f;

	private bool isMouseDown;
	private Vector3 startPos, endPos;

	// Update is called once per frame
	void LateUpdate ()
    {
        if (GUI_Manager.instance.startDrag)
        {
            return;
        }

		if(Input.GetMouseButtonDown(0))
		{
			isMouseDown = true;
			startPos = Input.mousePosition;
		}

		if(Input.GetMouseButtonUp(0))
		{
			isMouseDown = false;
		}

		if(isMouseDown)
		{
			endPos = Input.mousePosition;
			Vector3 dir = (startPos - endPos).normalized;
			if(dir.magnitude > 0.1f)
			{
				Vector3 newDir = new Vector3(0, 0, dir.x);
				transform.position += newDir * Time.deltaTime * moveSpeed;
				if(transform.position.z >= rightBound)
					transform.position = new Vector3(transform.position.x, transform.position.y,rightBound);
				else if(transform.position.z <= leftBound)
					transform.position = new Vector3(transform.position.x, transform.position.y,leftBound);
			}

			startPos = endPos;
		}
	}
}
