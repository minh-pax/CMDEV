﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemHandler : MonoBehaviour
{
    public Image icon;
    public GameObject key;
    public GameObject boder;
    public Sprite normalIcon;
    public Text txtPricePoint;

    public void InitHero(Sprite img, bool check)
    {
        icon.sprite = img;
        key.SetActive(check);
    }

    public void InitIcon(bool check, string pricePoint)
    {
        key.SetActive(check);
        boder.SetActive(check);
        txtPricePoint.text = pricePoint;
    }

    public void InitIcon(Sprite img, bool check, string pricePoint)
    {
        icon.sprite = img;
        key.SetActive(check);
        boder.SetActive(check);
        txtPricePoint.text = pricePoint;
    }

    public void OpenBoder(float time, int value)
    {
        boder.SetActive(true);

        Hashtable ht = new Hashtable();
        ht.Add("from", 1);
        ht.Add("to", 0);
        ht.Add("time", time);
        ht.Add("onupdate", "OnChange");
        ht.Add("oncomplete", "OnComplete");
        iTween.ValueTo(gameObject, ht);
    }

    public void OnChange(float value)
    {
        boder.GetComponent<Image>().fillAmount = value;
    }

    public void OnComplete()
    {
        gameObject.GetComponent<EventTrigger>().enabled = true;
    }
}
