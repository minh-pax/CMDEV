﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUI_Manager : MonoBehaviour
{
    
    public static GUI_Manager instance = null;
    public ConfigHero configHero;
    public ConfigTower configTower;
    public SoundManager soundManager;
    public UIMessageBox messageBox;
    public bool startDrag = false;
    public OptionData option;
    public InforData lol;
    public InforData dota;
    public int money;
    public int level;
    public bool checkAdmob = true;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
           Destroy(gameObject);

        /* PlayerPrefs.SetInt("Option", 0);
         PlayerPrefs.SetInt("LOL", 0);
         PlayerPrefs.SetInt("DOTA", 0);
         PlayerPrefs.SetInt("Money", -1);*/

        LoadCSV();

        SetPlayerPrefs();

        SetInforGame();

        DontDestroyOnLoad(gameObject);
    }

    public void SetInforGame()
    {
        option = SaveLoadManager.LoadOption();
        lol = SaveLoadManager.LoadInfor("LOL");
        dota = SaveLoadManager.LoadInfor("DOTA");
        money = SaveLoadManager.LoadMoney().money;

		soundManager.volumeEffice = option.sfx;
		soundManager.volumeMusic = option.music;
    }

    public void SetOption()
    {
        option = SaveLoadManager.LoadOption();
    }

    public void SaveMoneyinGame(int money)
    {
        MoneyGame dataMoney = new MoneyGame();
        dataMoney.money = money;
        SaveLoadManager.SaveMoney(dataMoney);
    }

    public void LoadCSV()
    {
        TextAsset configHeroAsset = Resources.Load("Config/ConfigHero") as TextAsset;
        configHero = new ConfigHero();
        configHero.Load(configHeroAsset);

        TextAsset configTowerAsset = Resources.Load("Config/ConfigTower") as TextAsset;
        configTower = new ConfigTower();
        configTower.Load(configTowerAsset);
    }

    public void SetPlayerPrefs()
    {
        if (PlayerPrefs.GetInt("Option") == 0)
        {
            OptionGame data = new OptionGame();
            data.sfx = 1;
            data.music = 0.3f;
            data.language = "English";
            SaveLoadManager.SaveOption(data);
            soundManager.volumeEffice = data.sfx;
            soundManager.volumeMusic = data.music;
            PlayerPrefs.SetInt("Option", 1);
        }
        if (PlayerPrefs.GetInt("LOL") == 0)
        {
            InforGame data = new InforGame();
            data.lv = 1;
            data.star = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                data.star.Add(0);
            }
            data.lvTower = 1;
            SaveLoadManager.SaveInfor(data, "LOL");
            PlayerPrefs.SetInt("LOL", 1);
        }
        if (PlayerPrefs.GetInt("DOTA") == 0)
        {
            InforGame data = new InforGame();
            data.lv = 1;
            data.star = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                data.star.Add(0);
            }
            data.lvTower = 1;
            SaveLoadManager.SaveInfor(data, "DOTA");
            PlayerPrefs.SetInt("DOTA", 1);    
        }
        if (PlayerPrefs.GetInt("Money") <= 0)
        {
            MoneyGame data = new MoneyGame();
            data.money = 0;
            SaveLoadManager.SaveMoney(data);
            PlayerPrefs.SetInt("Money", 1);
        }
        if (PlayerPrefs.GetString("Language") == "")
        {
            PlayerPrefs.SetString("Language", "English");
        }
    }

    public void ShowMessage(string message)
    {
        messageBox.Show(null, message, string.Empty, MessageBox.Buttons.OK, MessageBox.DefaultButton.Button1);
    }

    public void ShowMessage(string caption, string message)
    {
        messageBox.Show(null, message, caption, MessageBox.Buttons.OK, MessageBox.DefaultButton.Button1);
    }

    public void ShowMessage(string message, MessageBox.Callback callback)
    {
        messageBox.Show(callback, message, string.Empty, MessageBox.Buttons.OK, MessageBox.DefaultButton.Button1);
    }

    public void ShowMessage(string message, string caption, MessageBox.Callback callback)
    {

        messageBox.Show(callback, message, caption, MessageBox.Buttons.OK, MessageBox.DefaultButton.Button1);
    }

    public void ShowMessage(string message, string caption, MessageBox.Buttons buttons, MessageBox.Callback callback)
    {

        messageBox.Show(callback, message, caption, buttons, MessageBox.DefaultButton.Button1);
    }
}
