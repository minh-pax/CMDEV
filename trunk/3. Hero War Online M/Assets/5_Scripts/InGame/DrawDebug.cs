using UnityEngine;
using System.Collections;

public class DrawDebug : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void OnDrawGizmos()
	{
		Camera cam = GetComponent<Camera>();
		
		if(cam != null && cam.orthographic)
		{
			Gizmos.color = Color.yellow;
			Vector3 pos = cam.transform.position;
			float halfHeight = cam.orthographicSize;
			float halfWidth = halfHeight * cam.aspect;
			Vector3 topLeft = pos - cam.transform.right * halfWidth + cam.transform.up * halfHeight;
			Vector3 topRight = pos + cam.transform.right * halfWidth + cam.transform.up * halfHeight;
			Vector3 bottomLeft = pos - cam.transform.right * halfWidth - cam.transform.up * halfHeight;
			Vector3 bottomRight = pos + cam.transform.right * halfWidth - cam.transform.up * halfHeight;
			Gizmos.DrawLine(topLeft,topRight);
			Gizmos.DrawLine(topRight,bottomRight);
			Gizmos.DrawLine(bottomRight,bottomLeft);
			Gizmos.DrawLine(bottomLeft,topLeft);
		}
		
		Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1));

	}
}
