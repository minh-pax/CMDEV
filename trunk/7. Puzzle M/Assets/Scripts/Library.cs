﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class Library : MonoBehaviour {

    public GameObject obj_Img;
    public GameObject target;

    private Object[] textures;

    public void AddImage(int count)
    {
        Texture2D texture2D = LoadPNG(Application.persistentDataPath + "/img_" + count + ".png");
        CreateObject(texture2D, count);
    }

    public void StartAddImage()
    {
        textures = Resources.LoadAll("Images", typeof(Texture2D));

        for (int i = 0; i < textures.Length; i++)
        {
            Texture2D texture2D = textures[i] as Texture2D;
            CreateObject(texture2D, i);
        }

        if (PlayerPrefs.GetInt("Number") > 0)
        {
            for (int i = 0; i < PlayerPrefs.GetInt("Number"); i++)
            {
                Texture2D texture2D = LoadPNG(Application.persistentDataPath + "/img_" + i + ".png");
                CreateObject(texture2D, textures.Length + i);
            }
        }
    }

    public void DeleteAllParent()
    {
        foreach (Transform child in target.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void CreateObject(Texture2D texture2D, int count)
    {
        Rect rec = new Rect(0, 0, texture2D.width, texture2D.height);
        Sprite sprite = Sprite.Create(texture2D, rec, new Vector2(0, 0), 100);
        GameObject obj = Instantiate(obj_Img);
        obj.GetComponent<Image>().sprite = sprite;
        obj.transform.SetParent(target.transform);
        obj.transform.localScale = new Vector3(1, 1, 1);
        obj.name = "img_" + count;
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }
}
