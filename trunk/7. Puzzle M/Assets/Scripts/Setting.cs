﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Setting : MonoBehaviour
{
    public Text txtMusic;
    public Text txtSound;
    public Text txtLanguage;
    public AudioSource music;
    public AudioSource effSound;
    public AudioSource effShot;

    public void InitSetting()
    {
        if (PlayerPrefs.GetString("music") != "")
        {
            music.volume = int.Parse(PlayerPrefs.GetString("music"));

            if (music.volume == 0)
            {
                txtMusic.text = "Off";
            }
            else
            {
                txtMusic.text = "On";
            }
        }

        if (PlayerPrefs.GetString("effect") != "")
        {
            effSound.volume = int.Parse(PlayerPrefs.GetString("effect"));
            effShot.volume = int.Parse(PlayerPrefs.GetString("effect"));

            if (effSound.volume == 0)
            {
                txtSound.text = "Off";
            }
            else
            {
                txtSound.text = "On";
            }
        }

        if (PlayerPrefs.GetString("Language") != "")
        {
            if (PlayerPrefs.GetString("Language") == "VN")
            {
                txtLanguage.text = PlayerPrefs.GetString("Language");
            }
            else
            {
                txtLanguage.text = PlayerPrefs.GetString("Language");
            }
           
        }
    }

    public void OnMusic(Text txt)
    {
        if (txt.text == "On")
        {
            txt.text = "Off";
            music.volume = 0;
            PlayerPrefs.SetString("music", "0");
        }
        else if (txt.text == "Off")
        {
            txt.text = "On";
            music.volume = 1;
            PlayerPrefs.SetString("music", "1");
        } 
    }

    public void OnSound(Text txt)
    {
        if (txt.text == "On")
        {
            txt.text = "Off";
            effSound.volume = 0;
            effShot.volume = 0;
            PlayerPrefs.SetString("effect", "0");
        }
        else if (txt.text == "Off")
        {
            txt.text = "On";
            effSound.volume = 1;
            effShot.volume = 1;
            PlayerPrefs.SetString("effect", "1");
        }
    }

    public void OnLanguage(Text txt)
    {
        if (txt.text == "VN")
        {
            txt.text = "EN";
            PlayerPrefs.SetString("Language", txt.text);
        }
        else if (txt.text == "EN")
        {
            txt.text = "VN";
            PlayerPrefs.SetString("Language", txt.text);
        }
    }
}
