﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.IO;
using System;
using System.Collections.Generic;

public class DevFacebook : MonoBehaviour
{
    public FacebookMessage mess;
    private Texture2D lastResponseTexture;
    private string lastResponse = "";
    private string ApiQuery = "";
    private bool ShareImage;
    private int count = 0;
    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void OnLogin()
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends"  };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }

            Share();
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    public void Share()
    {
        if (!ShareImage)
        {
            StartCoroutine(ShareImageShot());
        }
    }

    IEnumerator ShareImageShot()
    {
        yield return new WaitForEndOfFrame();

        ControlManager controlManager = FindObjectOfType<ControlManager>();
        controlManager.OnWaitting(true);

        ShareImage = true;
        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);

        screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);

        screenTexture.Apply();

        byte[] dataToSave = screenTexture.EncodeToPNG();

        string Screenshot_Name = "image";
        string destination = Path.Combine(Application.persistentDataPath, Screenshot_Name);

        File.WriteAllBytes(destination, dataToSave);

        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", dataToSave, "ImageShare.png");
        wwwForm.AddField("message", mess.txtFacebook.text);

        FB.API("me/photos", Facebook.Unity.HttpMethod.POST, Callback, wwwForm);
    }

    private void Callback(IGraphResult result)
    {
        //throw new NotImplementedException();

        ControlManager controlManager = FindObjectOfType<ControlManager>();
        controlManager.OnMenu();
        controlManager.OnWaitting(false);
    }
}
