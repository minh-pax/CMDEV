using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CropSprite : MonoBehaviour 
{
//	Reference for sprite which will be cropped and it has BoxCollider or BoxCollider2D
	public GameObject spriteToCrop;
    public float x1, x2, y1, y2;
    public float width = 3;
    public float height = 5;
    private Vector3 startPoint, endPoint;
    void Start () 
	{
        Image spriteRenderer = spriteToCrop.GetComponent<Image>();
        Sprite spriteToCropSprite = spriteRenderer.sprite;
        Texture2D spriteTexture = spriteToCropSprite.texture;
        Rect spriteRect = spriteToCrop.GetComponent<Image>().sprite.textureRect;

        Rect croppedSpriteRect = spriteRect;
        Debug.Log(croppedSpriteRect.width + ", " + croppedSpriteRect.height + ", " + croppedSpriteRect.x + ", " + croppedSpriteRect.y + ", " + croppedSpriteRect.size);

        cropSprite(width, height, x1, y1, 1);
        cropSprite(width, height, x2, y2, 2);
    }


	void Update () 
	{
	}

	//	Following method crops as per displayed cropping area
	private void cropSprite(float width, float height, float x, float y, int count)
	{
        Image spriteRenderer = spriteToCrop.GetComponent<Image>();
		Sprite spriteToCropSprite = spriteRenderer.sprite;
		Texture2D spriteTexture = spriteToCropSprite.texture;
		Rect spriteRect = spriteToCrop.GetComponent<Image>().sprite.textureRect;
		GameObject croppedSpriteObj = new GameObject("CroppedSprite-" + count);
		Rect croppedSpriteRect = spriteRect;

        croppedSpriteRect.width = width;
        croppedSpriteRect.x  = x;
        croppedSpriteRect.height = height;
        croppedSpriteRect.y = y;
        
        Debug.Log(croppedSpriteRect.width + ", " + croppedSpriteRect.height + ", " + croppedSpriteRect.x + ", " + croppedSpriteRect.y + ", " + croppedSpriteRect.size);
        
        Sprite croppedSprite = Sprite.Create(spriteTexture, croppedSpriteRect, new Vector2(0,1), 100);
        Image cropSpriteRenderer = croppedSpriteObj.AddComponent<Image>();	
		cropSpriteRenderer.sprite = croppedSprite;
        cropSpriteRenderer.rectTransform.sizeDelta = new Vector2(croppedSpriteRect.width, croppedSpriteRect.height);
        croppedSpriteObj.transform.parent = spriteToCrop.transform.parent;
		croppedSpriteObj.transform.localScale = spriteToCrop.transform.localScale;
	}

}
