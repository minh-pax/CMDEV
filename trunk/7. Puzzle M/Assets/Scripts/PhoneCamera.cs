﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using UnityEngine;

public class PhoneCamera : MonoBehaviour
{
    public bool camAvailable;
    public WebCamTexture backCam;
    private Texture defaultBackground;
    public Text txt;
    private RawImage background;
    private AspectRatioFitter fit;
    private int orient;
    private int number;
    private bool checkSnapShot = true;

    public void InitCamarePhone()
    {
        if (PlayerPrefs.GetString("First") == null)
        {
            PlayerPrefs.SetInt("Number", 0);
            PlayerPrefs.SetString("First", "finish");
        }
        else
        {
            number = PlayerPrefs.GetInt("Number");
        }
       
        background = GetComponent<RawImage>();
        fit = GetComponent<AspectRatioFitter>();

        //txt.text = Application.dataPath + "===" + Application.persistentDataPath + "===" + Application.streamingAssetsPath + "===" + Application.temporaryCachePath;
        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No camera detected");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }

        if (backCam == null)
        {
            Debug.Log("Unable to find vack camera");
            return;
        }

        background.texture = backCam;
        camAvailable = false;
        backCam.Stop();
    }

    private void Update()
    {
        if (!camAvailable)
            return;

        float ratio = (float)backCam.width / (float)backCam.height;
        fit.aspectRatio = ratio;

        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

        orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0,0,orient);
    }

    public void OnRotation(GameObject obj)
    {
        obj.transform.Rotate(new Vector3(0,0,90));
    }

    public void SaveImage()
    {
        if (checkSnapShot)
        {
            //Create a Texture2D with the size of the rendered image on the screen.
            Texture2D texture = new Texture2D(backCam.width, backCam.height, TextureFormat.ARGB32, false);
            texture.SetPixels(backCam.GetPixels());
            texture.Apply();

            byte[] bytes = rotateTexture(texture).EncodeToPNG();

            File.WriteAllBytes(Application.persistentDataPath + "/img_" + number + ".png", bytes);
          
            txt.text = "Completed";
            backCam.Pause();
            checkSnapShot = false;
            StartCoroutine(WaitSnapShot());
        }
       
    }

    public void ClearWebCam()
    {
        if (backCam != null)
        {
            backCam.Stop();
        }
    }

    public IEnumerator WaitSnapShot()
    {
        yield return new WaitForSeconds(1f);
        backCam.Play();
        txt.text = "";
        ControlManager manager = GameObject.FindObjectOfType<ControlManager>();
        manager.pbLibrary.GetComponent<Library>().AddImage(number);
        number++;
        PlayerPrefs.SetInt("Number", number);
        checkSnapShot = true;
    }

    public Texture2D rotateTexture(Texture2D image)
    {
        Texture2D target = new Texture2D(image.height, image.width, image.format, false);    //flip image width<>height, as we rotated the image, it might be a rect. not a square image

        Color32[] pixels = image.GetPixels32(0);
        pixels = rotateTextureGrid(pixels, image.width, image.height);
        target.SetPixels32(pixels);
        target.Apply();

        return target;
    }


    public Color32[] rotateTextureGrid(Color32[] tex, int wid, int hi)
    {
        Color32[] ret = new Color32[wid * hi];      //reminder we are flipping these in the target

        for (int y = 0; y < hi; y++)
        {
            for (int x = 0; x < wid; x++)
            {
                //ret[(hi - 1) - y + x * hi] = tex[x + y * wid];         //juggle the pixels around
                ret[y + (wid - 1 - x) * hi] = tex[x + y * wid];
            }
        }

        return ret;
    }
}

