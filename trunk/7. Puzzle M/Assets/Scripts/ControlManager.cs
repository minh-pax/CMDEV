﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlManager : MonoBehaviour {

    public GameObject pbMenu;
    public GameObject pbSetting;
    public GameObject pbSelect;
    public GameObject pbCamera;
    public GameObject pbLibrary;
    public GameObject pbInGame;
    public GameObject snapShot;
    public GameObject pbTutorial;
    public GameObject pbFacebookMessage;
    public GameObject pbFacebookShare;
    public GameObject bg_main;
    public GameObject bgWatting;
    // Use this for initialization
    void Awake ()
    {
        pbLibrary.GetComponent<Library>().StartAddImage();
        snapShot.GetComponent<PhoneCamera>().InitCamarePhone();
        pbSetting.GetComponent<Setting>().InitSetting();
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void OnMenu()
    {
        pbMenu.SetActive(true);
        pbSetting.SetActive(false);
        pbInGame.SetActive(false);
        pbFacebookShare.SetActive(false);
        pbFacebookMessage.SetActive(false);
       

        GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
        admob.bannerView.Show();
    }

    public void OnSetting()
    {
        pbSetting.SetActive(true);
        pbMenu.SetActive(false);
    }

    public void OnSelect()
    {
        snapShot.SetActive(false);
        pbSelect.SetActive(true);
        pbMenu.SetActive(false);
        pbLibrary.SetActive(false);


    }

    public void OnCamera()
    {
        snapShot.SetActive(true);
        pbCamera.SetActive(true);
        pbSelect.SetActive(false);
        bg_main.SetActive(false);
            
        snapShot.GetComponent<PhoneCamera>().backCam.Play();
        snapShot.GetComponent<PhoneCamera>().camAvailable = true;
    }

    public void OnShowTutorial()
    {
        pbTutorial.SetActive(true);
    }

    public void OnHideTutorial()
    {
        pbTutorial.SetActive(false);
    }


    public void OnLibrary()
    {
        pbLibrary.SetActive(true);
        pbSelect.SetActive(false);
        pbCamera.SetActive(false);
        pbInGame.SetActive(false);
        snapShot.SetActive(false);
        bg_main.SetActive(true);
    }

    public void OnInGame(Image img)
    {
        pbInGame.SetActive(true);
        pbLibrary.SetActive(false);

        pbInGame.GetComponent<InGame>().InitInGame(img.sprite);

        GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
        admob.RequestInterstitial();
    }

    public void OnFacebookMessage()
    {
        pbFacebookMessage.SetActive(true);
        pbInGame.SetActive(false);

        GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
        admob.RequestInterstitial();
        admob.bannerView.Hide();
    }

    public void OnFacebookShare()
    {
        pbFacebookShare.SetActive(true);
        pbFacebookMessage.SetActive(false);
    }

    public void OnWaitting(bool check)
    {
        bgWatting.SetActive(check);
    }
}
