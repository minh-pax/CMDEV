﻿﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.IO;

public class InGame : MonoBehaviour
{
    public GridLayoutGroup grid;
    public Image mainImgae;
    public List<GameObject> listObj;
    public int[] listX;
    public int[] listY;
    public Text txtMatch;
    public Text txtShow;

    private bool press = false;
    private Vector2 fingerStart;
    private Vector2 fingerEnd;
    private GameObject objTarget;
    private Dictionary<string, bool> dictionary;
    private List<Sprite> listSprite;
    private List<Sprite> listSpriteRandom;
    public List<GameObject> listObjRandom;

    public void Awake()
    {
        dictionary = new Dictionary<string, bool>();
        listSprite = new List<Sprite>();
        listObjRandom = new List<GameObject>(listObj);
    }

    public void InitInGame(Sprite sprite)
    {
        OnResetGame();

        mainImgae.sprite = sprite;
        dictionary.Add("0-0", true);

        foreach (GameObject item in listObj)
        {
            dictionary.Add(item.name, false);
        }

        for (int i = 0; i < listObj.Count; i++)
        {
            Sprite sprt = cropSprite(listX[i], listY[i], sprite);
            sprt.name = listObj[i].name;
            listSprite.Add(sprt);
        }

        listObjRandom = listObjRandom.OrderBy(x => Random.value).ToList();

        for (int i = 0; i < listObjRandom.Count; i++)
        {
            listObjRandom[i].GetComponent<Image>().sprite = listSprite[i];
            
        }
       
        grid.enabled = false;
    }

    private Sprite cropSprite(float x, float y, Sprite sprite)
    {
        Sprite spriteToCropSprite = sprite;
        Texture2D spriteTexture = spriteToCropSprite.texture;
        Rect spriteRect = sprite.textureRect;
        Rect croppedSpriteRect = spriteRect;
        int pixelsToUnits = 100;

        croppedSpriteRect.width = 150;
        croppedSpriteRect.x = x;
        croppedSpriteRect.height = 150;
        croppedSpriteRect.y = y;

        Sprite croppedSprite = Sprite.Create(spriteTexture, croppedSpriteRect, new Vector2(0, 1), pixelsToUnits);
        return croppedSprite;
    }

    public void OnResetGame()
    {
        listSprite.Clear();
        dictionary.Clear();

        txtMatch.text = "No Match";
        txtShow.text = "Show";
    }

    void Update()
    {
        if (press)
        {
            OnMouseEvent();
        }
    }

    public void OnMouseEvent()
    {
        int tolerance = 0;
        fingerEnd = Input.mousePosition;

        if (Mathf.Abs(fingerEnd.x - fingerStart.x) > tolerance ||
           Mathf.Abs(fingerEnd.y - fingerStart.y) > tolerance)
        {

            if (Mathf.Abs(fingerStart.x - fingerEnd.x) > Mathf.Abs(fingerStart.y - fingerEnd.y))
            {
                //Right Swipe
                if ((fingerEnd.x - fingerStart.x) > 0)
                    OnRight();
                //Left Swipe
                else
                    OnLeft();
            }
            else
            {
                //Upward Swipe
                if ((fingerEnd.y - fingerStart.y) > 0)
                    OnUp();
                //Downward Swipe
                else
                    OnDown();
            }

            fingerStart = fingerEnd;
        }
    }

    public void OnRight()
    {
       // Debug.Log("Right");

        if (objTarget != null)
        {
            string[] str = objTarget.name.Split('-');
            string newTarget = (int.Parse(str[0]) + 1) + "-" + str[1];

            if (dictionary.ContainsKey(newTarget))
            {
                bool checkDic = dictionary[newTarget];

                if (checkDic)
                {
                    objTarget.transform.localPosition += new Vector3(150, 0, 0);
                    dictionary[newTarget] = false;
                    dictionary[objTarget.name] = true;
                    objTarget.name = newTarget;
                }
            }

            CheckTrueFalse();

            objTarget = null;
        }
    }

    public void OnLeft()
    {
       // Debug.Log("Left");

        if (objTarget != null)
        {
            string[] str = objTarget.name.Split('-');
            string newTarget = (int.Parse(str[0]) - 1) +"-"+ str[1];

            if (dictionary.ContainsKey(newTarget))
            {
                bool checkDic = dictionary[newTarget];

                if (checkDic)
                {
                    objTarget.transform.localPosition -= new Vector3(150, 0, 0);
                    dictionary[newTarget] = false;
                    dictionary[objTarget.name] = true;
                    objTarget.name = newTarget;
                }
            }

            CheckTrueFalse();

            objTarget = null;
        }
    }

    public void OnUp()
    {
       // Debug.Log("Up");

        if (objTarget != null)
        {
            string[] str = objTarget.name.Split('-');
            string newTarget = str[0] + "-" + (int.Parse(str[1]) - 1);
            if (dictionary.ContainsKey(newTarget))
            {
                bool checkDic = dictionary[newTarget];
                if (checkDic)
                {
                    objTarget.transform.localPosition += new Vector3(0, 150, 0);
                    dictionary[newTarget] = false;
                    dictionary[objTarget.name] = true;
                    objTarget.name = newTarget; 
                }
            }

            CheckTrueFalse();

            objTarget = null;
        }
    }

    public void OnDown()
    {
       // Debug.Log("Down");

        if (objTarget != null)
        {
            string[] str = objTarget.name.Split('-');
            string newTarget = str[0] + "-" + (int.Parse(str[1]) + 1);
            if (dictionary.ContainsKey(newTarget))
            {
                bool checkDic = dictionary[newTarget];
                if (checkDic)
                {
                    objTarget.transform.localPosition -= new Vector3(0, 150, 0);
                    dictionary[newTarget] = false; 
                    dictionary[objTarget.name] = true;
                    objTarget.name = newTarget;
                }
            }

            CheckTrueFalse();

            objTarget = null;
        }
    }

    public void OnDrag(GameObject obj)
    {
        objTarget = obj;
        fingerStart = Input.mousePosition;
        fingerEnd = Input.mousePosition;
        press = true;
    }

    public void OnDrop()
    {
        fingerStart = Vector2.zero;
        fingerEnd = Vector2.zero;
        press = false;
    }

    public void CheckTrueFalse()
    {
        bool check = true;

        for (int i = 0; i < listObj.Count; i++)
        {
            //Debug.Log(listObj[i].name +" != "+ listObj[i].GetComponent<Image>().sprite.name);
            if (listObj[i].name != listObj[i].GetComponent<Image>().sprite.name)
            {
                check = false;
                break;
            }
        }

        if (check)
        {
            ControlManager control = GameObject.FindObjectOfType<ControlManager>();
            control.OnFacebookMessage();

            FacebookMessage face = GameObject.FindObjectOfType<FacebookMessage>();
            face.img.sprite = mainImgae.sprite;
        }
    }

    public void OnShowHideSmaple(Text txt)
    {
        if (txt.text == "Show")
        {
            mainImgae.enabled = false;
            txt.text = "Hide";
        }
        else
        {
            mainImgae.enabled = true;
            txt.text = "Show";
        }
    }

    public void OnMath(Text txt)
    {
        grid.enabled = true;

        if (txt.text == "No Match")
        {
            for (int i = 0; i < listObj.Count; i++)
            {
                listObj[i].GetComponent<Image>().sprite = listSprite[i];
                listObj[i].name = listSprite[i].name;
            }

            txt.text = "Match";
        }
        else
        {
            listObjRandom = listObjRandom.OrderBy(x => Random.value).ToList();

            for (int i = 0; i < listObjRandom.Count; i++)
            {
                listObjRandom[i].GetComponent<Image>().sprite = listSprite[i];

            }

            txt.text = "No Match";
        }

        List<string> keys = new List<string>(dictionary.Keys);

        for (int i = 0; i < keys.Count; i++)
        {

            if (keys[i] == "0-0")
            {
                dictionary[keys[i]] = true;
            }
            else
            {
                dictionary[keys[i]] = false;
            }
        }

        StartCoroutine(OnArrange());
    }

    public IEnumerator OnArrange()
    {
        yield return new WaitForSeconds(1f);
        grid.enabled = false;
    }
}
