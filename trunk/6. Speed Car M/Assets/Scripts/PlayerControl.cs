﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public ControlManager controlManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            controlManager.inGame.checkRun = false;
            controlManager.pbInGame.SetActive(false);
            controlManager.OpenWinGame();
        }
    }
}
