﻿using UnityEngine;
using System.Collections;

public class CarControl : MonoBehaviour
{
    public float posIndex = 20;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Camera.main.transform.position.z >= (this.transform.position.z + 10))
        {
            Vector3 np = this.transform.position;
            np.x = 0;
            np += new Vector3(RangeX(Random.Range(1, 3)), 0, RangeZ(Random.Range(1, 4)));
            this.transform.position = np;
        }
    }

    public int RangeZ(int index)
    {
        int range = 0;

        switch (index)
        {
            case 1:
                range = 80;
                break;
            case 2:
                range = 90;
                break;
            case 3:
                range = 100;
                break;
            case 4:
                range = 110;
                break;
            case 5:
                range = 120;
                break;
            case 6:
                range = 130;
                break;
        }

        return range;
    }

    public int RangeX(int index)
    {
        int range = 0;

        switch (index)
        {
            case 1:
                range = -5;
                break;
            case 2:
                range = 0;
                break;
            case 3:
                range = 5;
                break;
        }

        return range;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "car")
        {
            if (posIndex > other.GetComponent<CarControl>().posIndex)
            {
                Vector3 np = this.transform.position;
                np.x = 0;
                np += new Vector3(RangeX(Random.Range(1, 3)), 0, RangeZ(Random.Range(1, 4)));
                this.transform.position = np;
            }
        }
    }
}
