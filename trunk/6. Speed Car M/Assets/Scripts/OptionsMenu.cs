﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour {

	public GameObject sound;
	public GameObject music;
    public Toggle onMusic, offMusic, onSound, offSound, onLanguage, offLanguage;
	// Use this for initialization
	void Start () {
	
	}

    public void SetOption()
    {
        if (PlayerPrefs.GetInt("Music") == 0)
        {
            music.GetComponent<AudioSource>().mute = false;
            onMusic.isOn = true;
            offMusic.isOn = false;
        }
        else
        {
            music.GetComponent<AudioSource>().mute = true;
            onMusic.isOn = false;
            offMusic.isOn = true;
        }

        if (PlayerPrefs.GetInt("Sound") == 0)
        {
            sound.GetComponent<AudioSource>().mute = false;
            onSound.isOn = true;
            offSound.isOn = false;
        }
        else
        {
            sound.GetComponent<AudioSource>().mute = true;
            onSound.isOn = false;
            offSound.isOn = true;
        }

        if (PlayerPrefs.GetString("Language") == "VietNam")
        {
            onLanguage.isOn = false;
            offLanguage.isOn = true;
        }
        else
        {
            onLanguage.isOn = true;
            offLanguage.isOn = false;
        }
    }

	public void OnSound(GameObject obj)
	{
		if (obj.name == "OnSound" && obj.GetComponent<Toggle> ().isOn == true)
        {
			sound.GetComponent<AudioSource>().mute = false;
            PlayerPrefs.SetInt("Sound", 0);
		} 
		else if (obj.name == "OffSound" && obj.GetComponent<Toggle> ().isOn == true)
		{
			sound.GetComponent<AudioSource>().mute = true;
            PlayerPrefs.SetInt("Sound", 1);
        }
	}

    public void OnMusic(GameObject obj)
    {
        if (obj.name == "OnMusic" && obj.GetComponent<Toggle>().isOn == true)
        {
            music.GetComponent<AudioSource>().mute = false;
            PlayerPrefs.SetInt("Music", 0);
        }
        else if (obj.name == "OffMusic" && obj.GetComponent<Toggle>().isOn == true)
        {
            music.GetComponent<AudioSource>().mute = true;
            PlayerPrefs.SetInt("Music", 1);
        }
    }

    public void OnLanguage(GameObject obj)
	{
		if (obj.name == "English" && obj.GetComponent<Toggle> ().isOn == true) {
            PlayerPrefs.SetString("Language", "English");
		} 
		else if (obj.name == "VietNam" && obj.GetComponent<Toggle> ().isOn == true)
		{
            PlayerPrefs.SetString("Language", "VietNam");
        }
	}
}
