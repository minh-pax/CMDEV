﻿using UnityEngine;
using System.Collections;

public class ControlManager : MonoBehaviour {

    public GameObject pbPlay;
    public GameObject pbOption;
    public GameObject pbCredits;
    public GameObject pbInGame;
    public GameObject pbWinGame;

    public InGame inGame;
    public WinGame winGame;
    public OptionsMenu optionsMenu;

    void Start()
    {
        OpenMenu();
        optionsMenu.SetOption();
    }

    public void PlayMenu()
    {
        pbPlay.SetActive(false);
        pbInGame.SetActive(true);
        pbWinGame.SetActive(false);
        inGame.ResetGame();
    }

    public void OpenMenu()
    {
        pbPlay.SetActive(true);
        pbOption.SetActive(false);
        pbCredits.SetActive(false);
        pbInGame.SetActive(false);
        pbWinGame.SetActive(false);
    }

    public void OpenOption()
    {
        pbOption.SetActive(true);
        pbPlay.SetActive(false);
        pbInGame.SetActive(false);
        optionsMenu.SetOption();
    }

    public void OpenCredits()
    {
        pbCredits.SetActive(true);
        pbPlay.SetActive(false);
        pbInGame.SetActive(false);
    }

    public void OpenWinGame()
    {
        pbWinGame.SetActive(true);
        winGame.Init((int)inGame.score);

		GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
		admob.Requestinterstitial();
    }

    public void OpenReStartGame()
    {      
        pbWinGame.SetActive(false);
        inGame.ResetGame();
    }

    public void OpenInterstitialAd()
    {
		OpenMenu(); 
    }
}
