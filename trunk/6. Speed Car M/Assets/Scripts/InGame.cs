﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGame : MonoBehaviour
{
    public Transform[] objCar;
    public Transform[] objStreet;
    public Transform player;
    public Transform cam;

    public Text txtScore;

    public bool checkRun = false;

    public float moveSpeed = 0.5f;
    public float score;

    private float time = 0;
	// Use this for initialization
	void Start ()
    {
        txtScore.text = 0 + "km";
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Application.CaptureScreenshot("Screenshot.png");
        }

        if (checkRun)
        {
            player.Translate(0, 0, -moveSpeed);
            cam.Translate(0, 0, moveSpeed);

            time += Time.deltaTime;

            if (time >= 0.5f)
            {
                float newScore = 0;
                newScore = score + (1 * (moveSpeed * 5));
                TimeUpSpeed((int)score);

                Hashtable ht = iTween.Hash("from", score, "to", newScore, "time", .5f, "onupdate", "OnChangeScore");
                //make iTween call:
                iTween.ValueTo(gameObject, ht);
                time = 0;
            }
        }
    }

    void OnChangeScore(float newValue)
    {
        txtScore.text = (int)newValue + " Km";
        score = newValue;
    }

    public void OnLeft()
    {
        if (player.position.x > -5)
            player.position += new Vector3(-5, 0, 0);
    }

    public void OnRight()
    {
        if (player.position.x < 5)
            player.position += new Vector3(5, 0, 0);
    }

    public void TimeUpSpeed(int point)
    {
        switch (point)
        {
            case 5:
                moveSpeed += 0.1f;
                break;
            case 10:
                moveSpeed += 0.1f;
                break;
            case 15:
                moveSpeed += 0.1f;
                break;
            case 20:
                moveSpeed += 0.1f;
                break;
            case 25:
                moveSpeed += 0.1f;
                break;
            case 30:
                moveSpeed += 0.1f;
                break;
            case 35:
                moveSpeed += 0.1f;
                break;
            case 70:
                moveSpeed += 0.1f;
                break;
            case 140:
                moveSpeed += 0.1f;
                break;
            case 280:
                moveSpeed += 0.1f;
                break;
            case 420:
                moveSpeed += 0.1f;
                break;
            case 630:
                moveSpeed += 0.1f;
                break;
            case 946:
                moveSpeed += 0.1f;
                break;
            case 1418:
                moveSpeed += 0.1f;
                break;
            case 2128:
                moveSpeed += 0.1f;
                break;
            case 3192:
                moveSpeed += 0.1f;
                break;
        }
    }

    public void ResetGame()
    {
        moveSpeed = 0.1f;
        score = 0;
        time = 0;

        objCar[0].localPosition = Vector3.zero;
        objCar[1].localPosition = Vector3.zero;
        objCar[2].localPosition = Vector3.zero;

        objStreet[0].localPosition = new Vector3(0, 0, 0);
        objStreet[1].localPosition = new Vector3(0, 0, 30);
        objStreet[2].localPosition = new Vector3(0, 0, 60);

        player.transform.localPosition = Vector3.zero;
        cam.transform.localPosition = new Vector3(0, 10, -28);

        ControlManager controlManager = FindObjectOfType<ControlManager>();
        controlManager.pbInGame.SetActive(true);
        checkRun = true;
    }
}
