﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinGame : MonoBehaviour
{
    public Text txtScore;
    public int _score;
    public void Init(int score)
    {
        txtScore.text = score + "";
        _score = score;
    }
}
