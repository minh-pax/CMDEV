﻿using UnityEngine;
using System.Collections;

public class ControlManager : MonoBehaviour {

    public GameObject pbIntro;
    public GameObject pbMenu;
    public GameObject pbMatchGame;
    public GameObject pbWaitingPlayer;
    public GameObject pbInGameMulti;
    public GameObject pbOption;
    public GameObject pbCredits;
    public GameObject pbWin;
    public GameObject pbLose;
    public GameObject pbQuit;
    public GameObject pbMessage;
    public GameObject pbBoder;

    public ControlMulti controlMulti;
    public MatchGame matchGame;
    public InGameMulti ingameMulti;
    public WinGame win;
    public LoseGame lose;
    public Message mess;
    public OptionsMenu option;

    void Start()
    {
        OpenIntro();
        option.SetOption();
    }

    public void OpenIntro()
    {
        pbIntro.SetActive(true);
		if (pbMenu.activeSelf == true) {
			pbMenu.SetActive (false);
		}
    }

    public void OpenMenu()
    {
        pbMenu.SetActive(true);
        pbIntro.SetActive(false);
        pbOption.SetActive(false);
        pbMessage.SetActive(false);
        pbCredits.SetActive(false);
        pbMatchGame.SetActive(false);
        pbInGameMulti.SetActive(false);     
        pbWaitingPlayer.SetActive(false);
        pbBoder.SetActive(false);
    }

    public void OpenOption()
    {
        pbOption.SetActive(true);
        pbMenu.SetActive(false);
    }

    public void OpenCredits()
    {
        pbCredits.SetActive(true);
        pbMenu.SetActive(false);
    }

    public void OpenMulti()
    {
        pbMatchGame.SetActive(true);
        pbMenu.SetActive(false);
    }

    public void OpenWaitingPlayer()
    {
        pbWaitingPlayer.SetActive(true);
        pbMatchGame.SetActive(false);
    }

    public void OpenInGameMulti()
    {
        pbInGameMulti.SetActive(true);
        pbWaitingPlayer.SetActive(false);
    }

    public void OpenQuit()
    {
        pbQuit.SetActive(true);
        pbBoder.SetActive(true);
    }

    public void OpenWin()
    {
        pbWin.SetActive(true);
        pbBoder.SetActive(true);

		GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
		admob.RequestInterstitial();
    }

    public void CanncelWinLose()
    {
        pbMenu.SetActive(true);
        pbWin.SetActive(false);
        pbLose.SetActive(false);
        pbInGameMulti.SetActive(false);
        controlMulti.LeaveRoom();
        pbBoder.SetActive(false);
    }

    public void OpenLose()
    {
        pbLose.SetActive(true);
        pbBoder.SetActive(true);

		GoogleAdmob admob = FindObjectOfType<GoogleAdmob>();
		admob.RequestInterstitial();
    }

    public void CanncelMatchGame()
    {
        pbMatchGame.SetActive(true);
        pbWaitingPlayer.SetActive(false);
        controlMulti.LeaveMatchGame();
        pbBoder.SetActive(false);
    }

    public void CanncelInGameMulti()
    {
        controlMulti.WinLoseGame(controlMulti.dataEnemy);
        pbBoder.SetActive(true);
    }

    public void MessageGame(string name)
    {
        mess.OnMessage(name);
        pbMessage.SetActive(true);
        pbBoder.SetActive(true);
    }

    public void CanncelMessage()
    {
        pbMessage.SetActive(false);
        pbBoder.SetActive(false);
    }
}
