﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PosButton
{
    public int x;
    public int y;

}

public class CurrentChess
{
    public List<string> list = new List<string>();
    public GameObject chess = new GameObject();
    public bool checkMove = false;
}

public class InGameMulti : MonoBehaviour
{
    public GameObject buttonGroup;
    public GameObject objRedChess;
    public GameObject objBlackChess;
    public GameObject objChessDestroy;
    public GameObject boder;
    public GameObject checkX;
    public GameObject checkO;
    public Text playerName_1, playerName_2;
    public GameObject vs;
    public GameObject txtTime;
    public CurrentChess currentChess;
    public ControlManager controlManager;
    public bool playerClick;
    public int countTimeGame = 20;

    private int maxX = 9;
    private int maxY = 10;
    private int countGame = 10;
    private float time = 0;
    private bool checkAddList = true;
    private List<GameObject> listRedpoint;
    private Dictionary<string, string> redChess;
    private Dictionary<string, string> blackChess;
    private GameObject objNewMove;

    void OnApplicationfocus(bool pauseStatus)
    {
        if (pauseStatus)
        {
            string mess = avUIManager.ChangeLanguage("31");
            controlManager.MessageGame(mess);
            controlManager.mess.btnMess.onClick.AddListener(controlManager.CanncelMatchGame);
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            string mess = avUIManager.ChangeLanguage("31");
            controlManager.MessageGame(mess);
            controlManager.pbBoder.SetActive(true);
            controlManager.controlMulti.LeaveMatchGame();
            controlManager.mess.btnMess.onClick.AddListener(controlManager.OpenMenu);
        }
    }

    void Start()
    {
        listRedpoint = new List<GameObject>();
        currentChess = new CurrentChess();

        objNewMove = new GameObject();
        objNewMove.name = "99-99";

        currentChess = new CurrentChess();

       // StartGame();
    }

    public void StartGame()
    {
        if (controlManager.controlMulti.dataPlayer["id"] == "1")
        {
            controlManager.controlMulti.dataPlayer["check"] = "X";
            controlManager.controlMulti.dataEnemy["check"] = "O";
            checkX.GetComponent<Animator>().enabled = true;
            playerClick = true;
            boder.SetActive(false);
        }
        else if (controlManager.controlMulti.dataPlayer["id"] == "2")
        {
            controlManager.controlMulti.dataPlayer["check"] = "O";
            controlManager.controlMulti.dataEnemy["check"] = "X";
            //checkX.GetComponent<Animator>().enabled = true;
            playerClick = false;
            boder.SetActive(true);
        }

        playerName_1.text = controlManager.controlMulti.dataPlayer["name"];
        playerName_2.text = controlManager.controlMulti.dataEnemy["name"];

        countGame = countTimeGame;

        for (int i = 0; i < buttonGroup.transform.childCount; i++)
        {
            if (buttonGroup.transform.GetChild(i).GetChild(0).childCount > 0)
            {
                Destroy(buttonGroup.transform.GetChild(i).GetChild(0).GetChild(0).gameObject);
            }
        }

        PositionChess();

        ClearMove();
    }

    public void PositionChess()
    {
        redChess = new Dictionary<string, string>();
        redChess[Chess.LeftChariot.ToString()] = "0-9";
        redChess[Chess.LeftHorse.ToString()] = "1-9";
        redChess[Chess.LeftElephant.ToString()] = "2-9";
        redChess[Chess.LeftAdvisor.ToString()] = "3-9";
        redChess[Chess.King.ToString()] = "4-9";
        redChess[Chess.RightChariot.ToString()] = "8-9";
        redChess[Chess.RightHorse.ToString()] = "7-9";
        redChess[Chess.RightElephant.ToString()] = "6-9";
        redChess[Chess.RightAdvisor.ToString()] = "5-9";
        redChess[Chess.LeftCannon.ToString()] = "1-7";
        redChess[Chess.RightCannon.ToString()] = "7-7";
        redChess[Chess.Pawn_1.ToString()] = "0-6";
        redChess[Chess.Pawn_2.ToString()] = "2-6";
        redChess[Chess.Pawn_3.ToString()] = "4-6";
        redChess[Chess.Pawn_4.ToString()] = "6-6";
        redChess[Chess.Pawn_5.ToString()] = "8-6";

        blackChess = new Dictionary<string, string>();
        blackChess[Chess.RightChariot.ToString()] = "0-0";
        blackChess[Chess.RightHorse.ToString()] = "1-0";
        blackChess[Chess.RightElephant.ToString()] = "2-0";
        blackChess[Chess.RightAdvisor.ToString()] = "3-0";
        blackChess[Chess.King.ToString()] = "4-0";
        blackChess[Chess.LeftChariot.ToString()] = "8-0";
        blackChess[Chess.LeftHorse.ToString()] = "7-0";
        blackChess[Chess.LeftElephant.ToString()] = "6-0";
        blackChess[Chess.LeftAdvisor.ToString()] = "5-0";
        blackChess[Chess.RightCannon.ToString()] = "1-2";
        blackChess[Chess.LeftCannon.ToString()] = "7-2";
        blackChess[Chess.Pawn_5.ToString()] = "0-3";
        blackChess[Chess.Pawn_4.ToString()] = "2-3";
        blackChess[Chess.Pawn_3.ToString()] = "4-3";
        blackChess[Chess.Pawn_2.ToString()] = "6-3";
        blackChess[Chess.Pawn_1.ToString()] = "8-3";

        SetupPositionChess(objRedChess, redChess);
        SetupPositionChess(objBlackChess, blackChess);
    }

    public void SetupPositionChess(GameObject objPosChess, Dictionary<string, string> d)
    {
        foreach (KeyValuePair<string, string> pair in d)
        {
            GameObject objChess = objPosChess.transform.Find(pair.Key).gameObject;
            GameObject objPosition = buttonGroup.transform.Find(pair.Value).gameObject.transform.Find("Pos").gameObject;
            GameObject objClone = Instantiate(objChess);
            objClone.transform.SetParent(objPosition.transform);
            objClone.transform.localPosition = Vector3.zero;
            objClone.transform.localRotation = Quaternion.identity;
            objClone.transform.localScale = Vector3.one;

            if (objPosChess.name == "RedChess")
            {
                objClone.name = pair.Key + "-" + pair.Value;   
            }
            else
            {
                objClone.name = pair.Key;
                objClone.tag = pair.Key;
            }

        }
    }

    private void Update()
    {
        if (playerClick)
        {
            time += 0.02f;

            checkX.GetComponent<Animator>().enabled = true;

            vs.SetActive(false);
            txtTime.SetActive(true);

            if (time >= 1)
            {
                countGame--;
                txtTime.GetComponent<Text>().text = countGame.ToString();
                time = 0;
            }

            if (countGame <= 0)
            {
                ClearMove();
                controlManager.controlMulti.OnSendMove(objNewMove.name);
                boder.SetActive(true);
                playerClick = false;
            }
        }
        else
        {
            checkX.GetComponent<Animator>().enabled = false;
            checkO.GetComponent<Animator>().enabled = false;
            checkX.GetComponent<Image>().color = Color.white;
            checkO.GetComponent<Image>().color = Color.white;
            vs.SetActive(true);
            txtTime.SetActive(false);
            countGame = countTimeGame;
            txtTime.GetComponent<Text>().text = countGame.ToString();
        }
    }

    public void OnClickChess(GameObject obj)
    {
        if (currentChess.checkMove == false)
        {
            currentChess.list.Clear();
            List<string> listMove = onMove(obj.name);
            currentChess.chess = obj;
            currentChess.checkMove = true;
            for (int i = 0; i < listMove.Count; i++)
            {
                currentChess.list.Add(listMove[i]);
            }
        }
        else
        {
            currentChess.checkMove = false;
            ClearMove();
        }
    }

    public void OnClickPos(GameObject obj)
    {
        IEnumerator coroutine;
        coroutine = WaitClickPos(obj);
        StartCoroutine(coroutine);

        currentChess.chess.transform.SetParent(objChessDestroy.transform);
        iTween.MoveTo(currentChess.chess, obj.transform.position, 1);

        string[] name = currentChess.chess.name.Split('-');
        currentChess.chess.name = name[0] + "-" + obj.name;
        currentChess.checkMove = false;
        controlManager.controlMulti.OnSendMove(currentChess.chess.name);

        ClearMove();
        boder.SetActive(true);
        playerClick = false;
    }

    IEnumerator WaitClickPos(GameObject obj)
    {
        yield return new WaitForSeconds(2f);

        currentChess.chess.transform.SetParent(obj.transform.Find("Pos"));
        //currentChess.chess.transform.localPosition = Vector3.zero;
    }

    public void BlackMove(Dictionary<string, string> data)
    {
        string name = data["position"];

        if (name != "99-99")
        {
            Transform blackPosButton = buttonGroup.transform.Find(SetBlackPosMove(name));
            GameObject blackPosChess = GameObject.FindGameObjectWithTag(name.Split('-')[0]);

            IEnumerator coroutine;
            coroutine = WaitBlackMove(blackPosButton.gameObject, blackPosChess);
            StartCoroutine(coroutine);

            blackPosChess.transform.SetParent(objChessDestroy.transform);
            iTween.MoveTo(blackPosChess, blackPosButton.position, 1);
        }

        StartCoroutine(WaitBlackMoveGame());
    }

    IEnumerator WaitBlackMove(GameObject blackPosButton, GameObject blackPosChess)
    {
        yield return new WaitForSeconds(2f);

        if (blackPosButton.transform.Find("Pos").childCount > 0)
        {
            Destroy(blackPosButton.transform.Find("Pos").GetChild(0).gameObject);
        }

        blackPosChess.transform.SetParent(blackPosButton.transform.Find("Pos"));
        //blackPosChess.transform.localPosition = Vector3.zero;
        blackChess[blackPosChess.name] = blackPosButton.name;
    }

    IEnumerator WaitBlackMoveGame()
    {
        yield return new WaitForSeconds(2f);

        boder.SetActive(false);
        playerClick = true;
    }

    public void RemoveChess(GameObject obj)
    {
        IEnumerator coroutine;
        coroutine = WaitRemoveChess(obj);
        StartCoroutine(coroutine);

        string name = blackChess[obj.name];
        GameObject objRed = buttonGroup.transform.Find(name).gameObject;
        OnClickPos(objRed);
    }

    IEnumerator WaitRemoveChess(GameObject obj)
    {
        yield return new WaitForSeconds(2f);

        if (obj.name == "King")
        {
            controlManager.controlMulti.WinLoseGame(controlManager.controlMulti.dataPlayer);
        }
        listRedpoint.Remove(obj.transform.GetChild(0).gameObject);
        Destroy(obj);
    }

    public void ClearMove()
    {
        if (currentChess.list.Count > 0)
        {
            for (int i = 0; i < currentChess.list.Count; i++)
            {
                buttonGroup.transform.Find(currentChess.list[i]).gameObject.GetComponent<Image>().enabled = false;
                buttonGroup.transform.Find(currentChess.list[i]).gameObject.GetComponent<Button>().enabled = false;
            }

            currentChess.list.Clear();
        }

        if (listRedpoint.Count > 0)
        {
            for (int i = 0; i < listRedpoint.Count; i++)
            {
                listRedpoint[i].SetActive(false);
            }

            listRedpoint.Clear();
        }
    }

    public List<string> onMove(string obj)
    {
        List<string> listMove = new List<string>();

        string[] posObj = obj.Split('-');

        switch (posObj[0])
        {
            case "LeftChariot":
            case "RightChariot":
                Chariot(listMove, posObj);
                break;
            case "LeftHorse":
            case "RightHorse":
                Horse(listMove, posObj);
                break;
            case "LeftElephant":
            case "RightElephant":
                Elephant(listMove, posObj);
                break;
            case "LeftAdvisor":
            case "RightAdvisor":
                Advisor(listMove, posObj);
                break;
            case "LeftCannon":
            case "RightCannon":
                Cannon(listMove, posObj);
                break;
            case "Pawn_1":
            case "Pawn_2":
            case "Pawn_3":
            case "Pawn_4":
            case "Pawn_5":
                Pawn(listMove, posObj);
                break;
            case "King":
                King(listMove, posObj);
                break;
        }

        return listMove;
    }

    public string SetBlackPosMove(string name)
    {
        string str = "";
        string[] arrPos = name.Split('-');

        int a, b;
        a = b = -1;

        for (int i = maxX - 1; i >= 0; i--)
        {
            a++;
            if(int.Parse(arrPos[1]) == i)
            {
                break;
            }
        }

        for (int i = maxY - 1; i >= 0; i--)
        {
            b++;
            if (int.Parse(arrPos[2]) == i)
            {
                break;
            }
        }

        str = a + "-" + b;
        return str;
    }

    public void Chariot(List<string> listMove, string[] posObj)
    {
        int x = int.Parse(posObj[1]);
        int y = int.Parse(posObj[2]);

        //ROW
        for (int i = 0; i < x; i++)
        {
            // Debug.Log("Row 1: " + x + ", " + (y - (i + 1)) + " - " + listButton[x, y - (i + 1)] + " == " + playerID);
            if (x - (i + 1) >= 0)
            {
                string value = (x - (i + 1)) + "-" + y;
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (tranPosition.childCount  <= 0)
                {
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                    listMove.Add(value);
                }
                else
                {
                    if (tranPosition.GetChild(0).childCount  > 0)
                    {
                        tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                        listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }

        for (int i = x; i < maxX; i++)
        {
            //  Debug.Log("Row 2: " + x + ", " + (i + 1 < 7) + " - " + listButton[x, i + 1] + " == " + playerID);
            if (i + 1 < maxX)
            {
                string value = (i + 1) + "-" + y;
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (tranPosition.childCount  <= 0)
                {
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                    listMove.Add(value);
                }
                else
                {
                    if (tranPosition.GetChild(0).childCount  > 0)
                    {
                        tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                        listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }

        //COLUM
        for (int i = 0; i < y; i++)
        {
            //Debug.Log("Colum 1: " + (x - (i + 1)) + ", " + y + " - " + listButton[x - (i + 1), y] + " == " + playerID);
            if (y - (i + 1) >= 0)
            {
                string value = x + "-" + (y - (i + 1));
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (tranPosition.childCount  <= 0)
                {
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                    listMove.Add(value);
                }
                else
                {
                    if (tranPosition.GetChild(0).childCount  > 0)
                    {
                        tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                        listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }

        for (int i = y; i < maxY; i++)
        {
            // Debug.Log("Colum 2: " + (i + 1) + ", " + y + " - " + listButton[i + 1, y] + " == " + playerID);
            if (i + 1 < maxY)
            {
                string value = x + "-" + (i + 1);
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (tranPosition.childCount  <= 0)
                {
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                    listMove.Add(value);
                }
                else
                {
                    if (tranPosition.GetChild(0).childCount  > 0)
                    {
                        tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                        listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                    }
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }

    public void Horse(List<string> listMove, string[] posObj)
    {
        int x = int.Parse(posObj[1]);
        int y = int.Parse(posObj[2]);

        List<int> listPosX = new List<int>();
        List<int> listPosY = new List<int>();

        listPosX.Add(x - 1); listPosY.Add(y - 2);
        listPosX.Add(x + 1); listPosY.Add(y - 2);
        listPosX.Add(x - 1); listPosY.Add(y + 2);
        listPosX.Add(x + 1); listPosY.Add(y + 2);
        listPosX.Add(x - 2); listPosY.Add(y - 1);
        listPosX.Add(x - 2); listPosY.Add(y + 1);
        listPosX.Add(x + 2); listPosY.Add(y - 1);
        listPosX.Add(x + 2); listPosY.Add(y + 1);
        
        for (int i = 0; i < listPosX.Count; i++)
        {
            if (listPosX[i] >= 0 && listPosX[i] < 9 && listPosY[i] >= 0 && listPosY[i] < 10)
            {
                string value = listPosX[i] + "-" + listPosY[i];
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;
               
                if (tranPosition.childCount  <= 0)
                {
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                    buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                    listMove.Add(value);
                }
                else
                {
                    if (tranPosition.GetChild(0).childCount  > 0)
                    {
                        tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                        listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                    }
                }
            }
        }
    }

    public void Elephant(List<string> listMove, string[] posObj)
    {
        bool check_1 = true;

        int i = 0;
        int x = int.Parse(posObj[1]);
        int y = int.Parse(posObj[2]);

        //CROSSLEFT
        while (check_1 == true)
        {
             
            if ((x - (i + 1)) >= 0 && (y - (i + 1)) >= 0)
            {
                string value = (x - (i + 1)) + "-" + (y - (i + 1));
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (i == 0)
                {
                    if (tranPosition.childCount  > 0)
                    {
                        check_1 = false;
                    }
                } 
                else if (i == 1)
                {

                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                        }
                    }
                    check_1 = false;
                }
            }
            else
            {
                check_1 = false;
            }

            i++;
        }

        check_1 = true;
        i = 1;

        while (check_1 == true)
        {
            
            if (x + i < maxX && y + i < maxY)
            {
                string value = (x + i) + "-" + (y + i);
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (i == 1)
                {
                    if (tranPosition.childCount  > 0)
                    {
                        check_1 = false;
                    }
                }
                else if (i == 2)
                {

                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                        }
                    }
                    check_1 = false;
                }
            }
            else
            {
                check_1 = false;
            }

            i++;
        }

        check_1 = true;
        i = 0;

        //CROSSRIGHT
        while (check_1 == true)
        {
            //Debug.Log("CrossRight 1: " + (x - (i + 1)) + ", " + (y + (i + 1)));
            if (x - (i + 1) >= 0 && y + (i + 1) < maxY)
            {
                string value = (x - (i + 1)) + "-" + (y + (i + 1));
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (i == 0)
                {
                    if (tranPosition.childCount  > 0)
                    {
                        check_1 = false;
                    }
                }
                else if (i == 1)
                {

                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                        }
                    }
                    check_1 = false;
                }
            }
            else
            {
                check_1 = false;
            }

            i++;

        }

        check_1 = true;
        i = 0;

        while (check_1 == true)
        {
            //Debug.Log("CrossRight 2 " + (x + (i + 1)) + ", " + (y - (i + 1)));
            if (x + (i + 1) < maxX && y - (i + 1) >= 0)
            {
                string value = (x + (i + 1)) + "-" + (y - (i + 1));
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (i == 0)
                {
                    if (tranPosition.childCount  > 0)
                    {
                        check_1 = false;
                    }
                }
                else if (i == 1)
                {

                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                        }
                    }
                    check_1 = false;
                }
            }
            else
            {
                check_1 = false;
            }

            i++;
        }
    }

    public void Advisor(List<string> listMove, string[] posObj)
    {
        int x = int.Parse(posObj[1]);
        int y = int.Parse(posObj[2]);
        string pos = x + "-" + y;

        if (pos == "3-7" || pos == "3-9" || pos == "5-9" || pos == "5-7" || pos == "4-8")
        {
            //Column 1
            if (CheckAdvisor(x - 1, y - 1))
            {
                listMove.Add((x - 1) + "-" + (y - 1));
            }

            if (CheckAdvisor(x - 1, y + 0))
            {
                listMove.Add((x - 1) + "-" + (y + 0));
            }

            if (CheckAdvisor(x - 1, y + 1))
            {
                listMove.Add((x - 1) + "-" + (y + 1));
            }

            //Column 2
            if (CheckAdvisor(x + 0, y - 1))
            {
                listMove.Add((x + 0) + "-" + (y - 1));
            }

            if (CheckAdvisor(x + 0, y + 1))
            {
                listMove.Add((x + 0) + "-" + (y + 1));
            }

            //Column 3
            if (CheckAdvisor(x + 1, y - 1))
            {
                listMove.Add((x + 1) + "-" + (y - 1));
            }

            if (CheckAdvisor(x + 1, y + 0))
            {
                listMove.Add((x + 1) + "-" + (y + 0));
            }

            if (CheckAdvisor(x + 1, y + 1))
            {
                listMove.Add((x + 1) + "-" + (y + 1));
            }
        }

        if (pos == "3-8" || pos == "4-7" || pos == "5-8" || pos == "4-9")
        {
            if (CheckAdvisor(x + 0, y - 1))
            {
                listMove.Add((x + 0) + "-" + (y - 1));
            }

            if (CheckAdvisor(x + 0, y + 1))
            {
                listMove.Add((x + 0) + "-" + (y + 1));
            }

            if (CheckAdvisor(x + 1, y + 0))
            {
                listMove.Add((x + 1) + "-" + (y + 0));
            }

            if (CheckAdvisor(x - 1, y + 0))
            {
                listMove.Add((x - 1) + "-" + (y + 0));
            }
        }

        for (int i = 0; i < listMove.Count; i++)
        {
            Transform tranPosition = buttonGroup.transform.Find(listMove[i]).gameObject.transform.Find("Pos").transform;

            if (tranPosition.childCount  <= 0)
            {
                buttonGroup.transform.Find(listMove[i]).gameObject.GetComponent<Image>().enabled = true;
                buttonGroup.transform.Find(listMove[i]).gameObject.GetComponent<Button>().enabled = true;
            }
            else
            {
                if (tranPosition.GetChild(0).childCount  > 0)
                {
                    tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                    listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                }
            }
        }
    }

    public void Cannon(List<string> listMove, string[] posObj)
    {
        int x = int.Parse(posObj[1]);
        int y = int.Parse(posObj[2]);
        int count = 0;
        //ROW
        for (int i = 0; i < x; i++)
        {
            // Debug.Log("Row 1: " + x + ", " + (y - (i + 1)) + " - " + listButton[x, y - (i + 1)] + " == " + playerID);
            if (x - (i + 1) >= 0)
            {
                string value = (x - (i + 1)) + "-" + y;
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (count == 0)
                {
                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        count++;
                    }
                }
                else
                {
                    if (tranPosition.childCount  > 0)
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                            break;
                        }
                    }
                }
            }
            else
            {
                break;
            }
        }

        count = 0;

        for (int i = x; i < maxX; i++)
        {
            //  Debug.Log("Row 2: " + x + ", " + (i + 1 < 7) + " - " + listButton[x, i + 1] + " == " + playerID);
            if (i + 1 < maxX)
            {
                string value = (i + 1) + "-" + y;
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (count == 0)
                {
                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        count++;
                    }
                }
                else
                {
                    if (tranPosition.childCount  > 0)
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                            break;
                        }
                    }
                }
            }
            else
            {
                break;
            }
        }

        count = 0;

        //COLUM
        for (int i = 0; i < y; i++)
        {
            //Debug.Log("Colum 1: " + (x - (i + 1)) + ", " + y + " - " + listButton[x - (i + 1), y] + " == " + playerID);
            if (y - (i + 1) >= 0)
            {
                string value = x + "-" + (y - (i + 1));
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (count == 0)
                {
                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        count++;
                    }
                }
                else
                {
                    if (tranPosition.childCount  > 0)
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                            break;
                        }
                    }
                }
            }
            else
            {
                break;
            }
        }

        count = 0;

        for (int i = y; i < maxY; i++)
        {
            // Debug.Log("Colum 2: " + (i + 1) + ", " + y + " - " + listButton[i + 1, y] + " == " + playerID);
            if (i + 1 < maxY)
            {
                string value = x + "-" + (i + 1);
                Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

                if (count == 0)
                {
                    if (tranPosition.childCount  <= 0)
                    {
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                        buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                        listMove.Add(value);
                    }
                    else
                    {
                        count++;
                    }
                }
                else
                {
                    if (tranPosition.childCount  > 0)
                    {
                        if (tranPosition.GetChild(0).childCount  > 0)
                        {
                            tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                            listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                            break;
                        }
                    }
                }
            }
            else
            {
                break;
            }
        }
    }

    public void Pawn(List<string> listMove, string[] posObj)
    {
        int x = int.Parse(posObj[1]);
        int y = int.Parse(posObj[2]);

        if (y - 1 >= 0)
        {
            string value = x + "-" + (y - 1);
            Transform tranPosition = buttonGroup.transform.Find(value).gameObject.transform.Find("Pos").transform;

            if (tranPosition.childCount  <= 0)
            {
                buttonGroup.transform.Find(value).gameObject.GetComponent<Image>().enabled = true;
                buttonGroup.transform.Find(value).gameObject.GetComponent<Button>().enabled = true;
                listMove.Add(value);
            }
            else
            {
                if (tranPosition.GetChild(0).childCount  > 0)
                {
                    tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                    listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                }
            }
        }
    }

    public void King(List<string> listMove, string[] posObj)
    {
        int x = int.Parse(posObj[1]);
        int y = int.Parse(posObj[2]);

        if (CheckAdvisor(x - 1, y + 0))
        {
            listMove.Add((x - 1) + "-" + (y + 0));
        }

        if (CheckAdvisor(x + 1, y + 0))
        {
            listMove.Add((x + 1) + "-" + (y + 0));
        }

        if (CheckAdvisor(x + 0, y - 1))
        {
            listMove.Add((x + 0) + "-" + (y - 1));
        }

        if (CheckAdvisor(x + 0, y + 1))
        {
            listMove.Add((x + 0) + "-" + (y + 1));
        }

        for (int i = 0; i < listMove.Count; i++)
        {
            Transform tranPosition = buttonGroup.transform.Find(listMove[i]).gameObject.transform.Find("Pos").transform;

            if (tranPosition.childCount <= 0)
            {
                buttonGroup.transform.Find(listMove[i]).gameObject.GetComponent<Image>().enabled = true;
                buttonGroup.transform.Find(listMove[i]).gameObject.GetComponent<Button>().enabled = true;
            }
            else
            {
                if (tranPosition.GetChild(0).childCount > 0)
                {
                    tranPosition.GetChild(0).GetChild(0).gameObject.SetActive(true);
                    listRedpoint.Add(tranPosition.GetChild(0).GetChild(0).gameObject);
                }
            }
        }
    }

    public bool CheckAdvisor(int x, int y)
    {
        bool check = true;

        if (x < 3 || x > 5 || y < 7 || y > 9)
        {
            check = false;
        }

        return check;
    }
}
