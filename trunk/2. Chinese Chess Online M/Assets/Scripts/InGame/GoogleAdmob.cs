﻿using UnityEngine;
using System.Collections;
//using admob;
using GoogleMobileAds.Api;
using System;

//Admob chinh hang
public class GoogleAdmob : MonoBehaviour
{
    InterstitialAd interstitial;

    // Use this for initialization
    void Awake()
    {
        RequestBanner();
    }

    void Update()
    {
        OnGoogleAdmob();

    }

    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-3373095007217675/7683747949";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        BannerView bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    public void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-3373095007217675/8821536340";
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    public void OnGoogleAdmob()
    {
        if (interstitial != null)
        {
            if (interstitial.IsLoaded())
            {
                interstitial.Show();
            }
        }
    }
}


//Admob tu nhan viet
/*public class GoogleAdmob : MonoBehaviour
{
	public Admob ad;
	// Use this for initialization
	void Awake()
	{
		RequestBanner();
	}

	void Update ()
	{
		OnGoogleAdmob ();
       
    }

	public void RequestBanner()
	{
		ad = Admob.Instance();
#if UNITY_ANDROID
        ad.initAdmob("ca-app-pub-3373095007217675/7683747949", "ca-app-pub-3373095007217675/8821536340");
#elif UNITY_IOS
        ad.initAdmob("ca-app-pub-3373095007217675/2280728742", "ca-app-pub-3373095007217675/9978792341");
#endif

		Admob.Instance().showBannerRelative(AdSize.SmartBanner, AdPosition.TOP_CENTER, 0);
	}
		
	public void RequestInterstitial()
	{
		ad.loadInterstitial();
        
    }
		
	public void OnGoogleAdmob()
	{
		if (ad != null)
		{
			if (ad.isInterstitialReady())
			{
				ad.showInterstitial();
			}
        }
	}

	void onInterstitialEvent(string eventName, string msg)
	{
		Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
	}
	void onBannerEvent(string eventName, string msg)
	{
		Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
	}
}*/
