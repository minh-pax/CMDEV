﻿using UnityEngine;
using System.Collections;

public enum Chess
{
    LeftChariot,
    LeftHorse,
    LeftElephant,
    LeftAdvisor,
    LeftCannon,
    King,
    RightChariot,
    RightHorse,
    RightElephant,
    RightAdvisor,
    RightCannon,
    Pawn_1,
    Pawn_2,
    Pawn_3,
    Pawn_4,
    Pawn_5,
}

public static class avUIManager
{
    public static ConfigString_Normal LoadString_Normal()
    {
        TextAsset config = Resources.Load("Config/ConfigString_Normal") as TextAsset;
        ConfigString_Normal csv = new ConfigString_Normal();
        csv.Load(config);

        return csv;
    }

    public static string ChangeLanguage(string stringID)
    {

		if (PlayerPrefs.GetString("FirstLanguage") == "")
		{
			PlayerPrefs.SetString("Language", "English");
			PlayerPrefs.SetString("FirstLanguage", "first");
		}
		
        string text = "";
        string language = PlayerPrefs.GetString("Language");

        switch (language)
        {
            case "English":
                text = avUIManager.LoadString_Normal().Find_ID(stringID).EN;
                break;
            case "VietNam":
                text = avUIManager.LoadString_Normal().Find_ID(stringID).VN;
                break;
            default:
                text = avUIManager.LoadString_Normal().Find_ID(stringID).EN;
                break;
        }

        return text;
    }


}
