﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinGame : MonoBehaviour
{
    public Text txtStatus;

    public void Win(string playerWin)
    {
        string mess = string.Format("{0} {1}", playerWin, avUIManager.ChangeLanguage("32"));
        txtStatus.text = mess;
    }
}
