﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : MonoBehaviour {

    public int roundTime = 99;
    public BannerController banner;
    public Fighter player1;
    public Fighter player2;
    public AudioSource musicPlayer;
    public AudioClip backgroundMusic;


    private float lastTimeUpdate = 0;

    private bool battlestarted;
    private bool battleEnable;
	// Use this for initialization
	void Start () {
		banner.showRoundFight();
	}

    private void expireTime()
    {
        if (player1.healtPercent > player2.healtPercent)
        {
            player2.healt = 0;
        }
        else
        {
            player1.healt = 0;
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
        if(!battlestarted && !banner.isAnimating)
        {
            battlestarted = true;

            player1.enable = true;
            player2.enable = true;

            GameUtils.PlaySound(backgroundMusic, musicPlayer);
        }
        if(battlestarted && !battleEnable)
        {
            if (roundTime > 0 && Time.time - lastTimeUpdate > 1)
            {
                roundTime--;
                lastTimeUpdate = Time.time;

                if (roundTime == 0)
                {
                    expireTime();
                }
            }

            if(player1.healtPercent <= 0)
            {
                banner.showYouLose();
                battleEnable = true;
                player1.enable = false;
                player2.enable = false;
            }
            else if(player2.healtPercent <= 0)
            {
                banner.showYouWin();
                battleEnable = true;
                player1.enable = false;
                player2.enable = false;
            }
        }
	}
}
