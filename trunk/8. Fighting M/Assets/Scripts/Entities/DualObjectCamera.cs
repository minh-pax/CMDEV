﻿using UnityEngine;
using System.Collections;

public class DualObjectCamera : MonoBehaviour
{
    public Transform leftTarget;
    public Transform rightTarget;

    public float minDistance;
    private float min;

    private void Start()
    {
        min = Mathf.Abs(leftTarget.position.x - rightTarget.position.x);
    }

    // Update is called once per frame
    void LateUpdate()
     {
         float distanceBetweenTargets = Mathf.Abs(leftTarget.position.x - rightTarget.position.x) * 2;
         float pos_z = leftTarget.position.z + distanceBetweenTargets;

         float centerPosition = (leftTarget.position.x + rightTarget.position.x) / 2;

        //if (pos_z > (leftTarget.position.z + min))
         transform.position = new Vector3 (
             centerPosition, transform.position.y,
              pos_z > minDistance ? pos_z : minDistance
             );
     }
}
