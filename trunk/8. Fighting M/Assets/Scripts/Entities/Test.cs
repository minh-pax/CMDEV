﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

    private void Awake()
    {
        Debug.Log("Awake");
    }

    private void OnEnable()
    {
        Debug.Log("OnEnable");
    }

    // Use this for initialization
    void Start () {
        Debug.Log("Start");
        StartCoroutine(Test_1());
	}

    IEnumerator Test_1()
    {
        yield return new WaitForSeconds(0.1f);

        Debug.Log("Test_1");
    }

    private void FixedUpdate()
    {
        Debug.Log("FixedUpdate");
    }

    private void LateUpdate()
    {
        Debug.Log("LateUpdate");
    }

    // Update is called once per frame
    void Update () {
        Debug.Log("Update");
    }
}
