﻿using UnityEngine;
using System.Collections;

public enum FighterState
{
    IDLE, WALK, WALK_BACK, JUMP, DUCK,
    ATTACK, TAKE_HIT, TAKE_HIT_DEFEND,
    DEFEND, DEAD, NONE,
}
