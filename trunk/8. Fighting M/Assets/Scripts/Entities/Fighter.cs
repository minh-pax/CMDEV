using UnityEngine;
using System.Collections;

public class Fighter : MonoBehaviour
{
    public enum PlayerType
    {
        HUMAN, AI
    }

    public static float MAX_HEALTH = 100f;
    public float healt = MAX_HEALTH;
    public string fighterName;
    public Fighter oponent;
    public PlayerType player;
    public FighterState currentState = FighterState.IDLE;

    public bool enable;

    private Animator animator;
    private Rigidbody rig;
    private AudioSource audioPlayer;

    //for  aI only
    private float random;
    private float randomSetTime;

    
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        rig = GetComponent<Rigidbody>();
        audioPlayer = GetComponent<AudioSource>();
       
    }

    public void UpdateHumanInput()
    {
        if (Input.GetAxis("Horizontal") > 0.1)
        {
            animator.SetBool("WALK", true);
        }
        else
        {
            animator.SetBool("WALK", false);
        }

        if (Input.GetAxis("Horizontal") < -0.1)
        {
            animator.SetBool("WALK_BACK", true);
        }
        else
        {
            animator.SetBool("WALK_BACK", false);
        }

        if (Input.GetAxis("Vertical") < -0.1)
        {
            animator.SetBool("DUCK", true);
        }
        else
        {
            animator.SetBool("DUCK", false);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            animator.SetTrigger("JUMP");
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("PUNCH");
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            animator.SetTrigger("KICK");
        }

        if (Input.GetKey(KeyCode.F))
        {
            animator.SetBool("DEFEND", true);
        }
        else
        {
            animator.SetBool("DEFEND", false);
        }
        
    }

    public float getDistanceToOponent()
    {
        return Mathf.Abs(transform.position.x - oponent.transform.position.x);
    }

    public void UpdateAIInput()
    {
        animator.SetBool("defending", defending);
       // animator.SetBool("invulnerable", invulnerable);
        animator.SetBool("enable", enable);

        animator.SetBool("oponent_attacking", oponent.attacking);
        animator.SetFloat("distanceToOponent", getDistanceToOponent());

        if (Time.time - randomSetTime > 1)
        {
            random = Random.value;
            randomSetTime = Time.time;
        }
        animator.SetFloat("random", random);
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("health", healtPercent);

        if (oponent != null)
        {
            animator.SetFloat("oponent_health", oponent.healtPercent);
        }
        else
        {
            animator.SetFloat("oponent_health", 1);
        }
        if(enable)
        {
            if (player == PlayerType.HUMAN)
            {
                UpdateHumanInput();
            }
            else
            {
                UpdateAIInput();
            }
        }
        

        if (healt <= 0 && currentState != FighterState.DEAD)
        {
            animator.SetTrigger("DEAD");
        }
    }

    public void playSound(AudioClip sound)
    {
        GameUtils.PlaySound(sound, audioPlayer);
    }

    public virtual void hurt(float damge, FighterState state)
    {
        if (!invulnerable)
        {
            if (defending)
            {
                damge *= 0.2f;
            }
            if (healt >= damge)
            {
                healt -= damge;
            }
            else
            {
                healt = 0;
            }

            if (healt > 0)
            {
                animator.SetTrigger("TAKE_HIT");
            }
        }
    }

    public bool invulnerable
    {
        get
        {
            return currentState == FighterState.TAKE_HIT
                || currentState == FighterState.TAKE_HIT_DEFEND
                || currentState == FighterState.DEAD;
        }
    }

    public bool defending
    {
        get
        {
            return currentState == FighterState.DEFEND
                || currentState == FighterState.TAKE_HIT_DEFEND;
        }
    }

    public bool attacking
    {
        get
        {
            return currentState == FighterState.ATTACK;
        }
    }

    public float healtPercent
    {
        get
        {
            return healt / MAX_HEALTH;
        }
    }

    public Rigidbody body
    {
        get
        {
            return this.rig;
        }
    }
}
