﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

    public Fighter player_1;
    public Fighter player_2;

    public Text leftName;
    public Text rightName;

    public Scrollbar leftBar;
    public Scrollbar rightBar;

    public Text timerText;

    public BattleController battle;

	// Use this for initialization
	void Start () {
        leftName.text = player_1.fighterName;
        rightName.text = player_2.fighterName;
	}
	
	// Update is called once per frame
	void Update ()
    {
        timerText.text = battle.roundTime.ToString();

        if (leftBar.size > player_1.healtPercent)
        {
            leftBar.size -= 0.01f;
        }

        if (rightBar.size > player_2.healtPercent)
        {
            rightBar.size -= 0.01f;
        }
    }
}
