﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class WaitingPlayer : MonoBehaviour {

    public ControlManager controlManager;
    void OnApplicationfocus(bool pauseStatus)
    {
        if (pauseStatus)
        {
            string mess = avUIManager.ChangeLanguage("31");
            controlManager.MessageGame(mess);
            controlManager.mess.btnMess.onClick.AddListener(controlManager.CanncelMatchGame);
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            string mess = avUIManager.ChangeLanguage("31");
            controlManager.MessageGame(mess);
            controlManager.pbBoder.SetActive(true);
            controlManager.controlMulti.LeaveMatchGame();
            controlManager.mess.btnMess.onClick.AddListener(controlManager.OpenMenu);
        }
    }
}
