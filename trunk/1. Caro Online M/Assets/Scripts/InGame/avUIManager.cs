﻿using UnityEngine;
using System.Collections;
using System;

public static class avUIManager
{
    public static AssetBundle assetBundle;

    public static ConfigString_Normal LoadString_Normal()
    {
        TextAsset config = Resources.Load("Config/ConfigString_Normal") as TextAsset;
        ConfigString_Normal csv = new ConfigString_Normal();
        csv.Load(config);

        return csv;
    }

    public static string ChangeLanguage(string stringID)
    {

		if (PlayerPrefs.GetString("FirstLanguage") == "") 
		{
			PlayerPrefs.SetString("Language", "English");
			PlayerPrefs.SetString ("FirstLanguage", "1");
		}

        string text = "";
        string language = PlayerPrefs.GetString("Language");

        switch (language)
        {
            case "English":
                text = avUIManager.LoadString_Normal().Find_ID(stringID).EN;
                break;
            case "VietNam":
                text = avUIManager.LoadString_Normal().Find_ID(stringID).VN;
                break;
        }

        return text;
    }
}
