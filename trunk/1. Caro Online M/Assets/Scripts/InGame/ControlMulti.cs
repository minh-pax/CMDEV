﻿using UnityEngine;
using System.Collections;
using SocketIO;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class ControlMulti : MonoBehaviour {

    public SocketIOComponent socket;
    public ControlManager controlManager;
    public Room playerInRoom;
    public Dictionary<string, string> dataPlayer;
    public Dictionary<string, string> dataEnemy;

    // Use this for initialization

    void Start () {
        dataPlayer = new Dictionary<string, string>();
        dataEnemy = new Dictionary<string, string>();
        playerInRoom = new Room();
        socket.On("USER_CONNECTED", OnUserConntected);
        socket.On("PLAY", OnUserPlay);
        socket.On("CREATE_ROOM", OnCreateRoom);
        socket.On("USER_DISCONNECTED", OnUserDisconnected);
        socket.On("MOVE", OnUserMove);
        socket.On("RESULT", OnUserResult);
    }
	
	public void ConnectToServer()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            string mess = avUIManager.ChangeLanguage("29");
            controlManager.MessageGame(mess);
            controlManager.mess.btnMess.onClick.AddListener(controlManager.CanncelMessage);
            return;
        }

        MatchGameServer();
        socket.Emit("USER_CONNECT");

    }

    public void MatchGameServer()
    {
        if (controlManager.matchGame.txtName.text != "")
        {
            string device = SystemInfo.deviceUniqueIdentifier;
            dataPlayer["device"] = device;
            Dictionary<string, string> data = new Dictionary<string, string>();
            dataPlayer["name"] = controlManager.matchGame.txtName.text;
            data["name"] = dataPlayer["name"];
            data["device"] = device;
            socket.Emit("PLAY", new JSONObject(data));
            controlManager.OpenWaitingPlayer();
        }
        else
        {
            string mess = avUIManager.ChangeLanguage("30");
            controlManager.matchGame.txtName.text = mess;
        }
    }

    private void OnUserConntected(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserConntected ");
    }

    private void OnUserPlay(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserPlay ");
    }

    private void OnUserDisconnected(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserDisconnected ");
        string mess = avUIManager.ChangeLanguage("31");
        controlManager.MessageGame(mess);
        controlManager.mess.btnMess.onClick.AddListener(controlManager.OpenMenu);
    }

    private void OnCreateRoom(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " CreateRoom ");

        bool checkOpenInGame = false;
        string playerName1 = JsonToString(evt.data.GetField("player1").ToString(), "\"");
        string playerName2 = JsonToString(evt.data.GetField("player2").ToString(), "\"");
        string playerName1Device = JsonToString(evt.data.GetField("player1Device").ToString(), "\"");
        string playerName2Device = JsonToString(evt.data.GetField("player2Device").ToString(), "\"");
        string room = JsonToString(evt.data.GetField("room").ToString(), "\"");
        
        if (controlManager.matchGame.txtName.text == playerName1 && dataPlayer["device"] == playerName1Device)
        {

            dataPlayer["id"] = "1";
            dataEnemy["id"]  = "2";
            dataEnemy["device"] = playerName2Device;
            checkOpenInGame = true;
        }
        else 
        {
            dataPlayer["id"] = "2";
            dataEnemy["id"] = "1";
            dataEnemy["device"] = playerName1Device;
            checkOpenInGame = true;
        }

        if (checkOpenInGame)
        {
            if (dataPlayer["name"] == playerName1)
                dataEnemy["name"] = playerName2;
            else
                dataEnemy["name"] = playerName1;

            playerInRoom.player1 = playerName1;
            playerInRoom.player2 = playerName2;
            playerInRoom.room = room;
            
            checkOpenInGame = false;
            controlManager.OpenInGameMulti();
            controlManager.ingameMulti.StartGame();
        }

        Dictionary<string, string> data = new Dictionary<string, string>();
        data["player1"] = playerName1;
        data["player2"] = playerName2;
        data["player1Device"] = playerName1Device;
        data["player2Device"] = playerName2Device;
        socket.Emit("ADD_ENEMY", new JSONObject(data));
    }

    private void OnUserMove(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserMove ");
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["playerID"] = JsonToString(evt.data.GetField("playerID").ToString(), "\"");
        data["position"] = JsonToString(evt.data.GetField("position").ToString(), "\"");
        data["room"]     = JsonToString(evt.data.GetField("room").ToString(), "\"");
        controlManager.ingameMulti.EnemyMove(data);
    }

    public void OnSendMove(GameObject obj)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["playerID"] = dataPlayer["id"];
        data["position"] = obj.name;
        data["room"] = playerInRoom.room;
        socket.Emit("MOVE", new JSONObject(data));
    }

    private void OnUserResult(SocketIOEvent evt)
    {
        Debug.Log("Get the message from server is: " + evt.data + " OnUserResult ");
        string usernameWin = JsonToString(evt.data.GetField("name").ToString(), "\"");
        string device = JsonToString(evt.data.GetField("device").ToString(), "\"");
        if (usernameWin == dataPlayer["name"] && device == dataPlayer["device"])
        {
            controlManager.OpenWin();
            controlManager.win.Win(dataPlayer["name"]);
        }
        else
        {
            controlManager.OpenLose();
            controlManager.lose.Lose(dataPlayer["name"]);
        }

        controlManager.ingameMulti.playerClick = false;
    }

    public void WinLoseGame(Dictionary<string, string> dataUser)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["name"] = dataUser["name"];
        data["room"] = playerInRoom.room;
        data["device"] = dataUser["device"];
        socket.Emit("RESULT", new JSONObject(data));
    }

    public void LeaveRoom()
    {
        socket.Emit("LEAVE_ROOM");  
    }

    public void LeaveMatchGame()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["name"] = dataPlayer["name"];
        socket.Emit("LEAVE_MATCH", new JSONObject(data));
    }

    string JsonToString(string target, string s)
    {
        string[] newString = Regex.Split(target, s);
        return newString[1];
    }
}
