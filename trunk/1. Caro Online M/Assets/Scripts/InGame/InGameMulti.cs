﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PosButton
{
    public int x;
    public int y;

}

public class InGameMulti : MonoBehaviour {

    public GameObject[] clickCheck;
    public GameObject playerCheck;
    public GameObject enemyCheck;
    public GameObject buttonGroup;
    public GameObject checkX;
    public GameObject checkO;
    public GameObject vs;
    public GameObject txtTime;
    public ControlManager controlManager;
    public Text playerName_1, playerName_2;
    public GameObject boder;
    public int[,] listButton;
    public int maxX = 11;
    public int maxY = 11;

    private int countColum = 1;
    private int countRow = 1;
    private int countLeft = 1;
    private int countRight = 1;
    private int countGame = 10;
    private float time = 0;
    public bool playerClick;
    private GameObject objNewMove;

    void OnApplicationfocus(bool pauseStatus)
    {
        if (pauseStatus)
        {
            string mess = avUIManager.ChangeLanguage("31");
            controlManager.MessageGame(mess);
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            controlManager.OpenLose();
            controlManager.lose.Lose(controlManager.controlMulti.dataPlayer["name"]);
            controlManager.controlMulti.WinLoseGame(controlManager.controlMulti.dataEnemy);
        }
    }

    void Start()
    {
        objNewMove = new GameObject();
        objNewMove.name = "99-99";
    }

    public void StartGame()
    {
        if (controlManager.controlMulti.dataPlayer["id"] == "1")
        {
            playerCheck = clickCheck[0];
            enemyCheck = clickCheck[1];
            controlManager.controlMulti.dataPlayer["check"] = "X";
            controlManager.controlMulti.dataEnemy["check"] = "O";
            checkX.GetComponent<Animator>().enabled = true;
            checkO.GetComponent<Animator>().enabled = false;
            playerClick = true;
            boder.SetActive(false);
        }
        else if (controlManager.controlMulti.dataPlayer["id"] == "2")
        {
            playerCheck = clickCheck[1];
            enemyCheck = clickCheck[0];
            controlManager.controlMulti.dataPlayer["check"] = "O";
            controlManager.controlMulti.dataEnemy["check"] = "X";
            checkX.GetComponent<Animator>().enabled = false;
            checkO.GetComponent<Animator>().enabled = true;
            playerClick = false;
            boder.SetActive(true);
        }

        playerName_1.text = controlManager.controlMulti.playerInRoom.player1;
        playerName_2.text = controlManager.controlMulti.playerInRoom.player2;

        listButton = new int[maxX, maxY];

        for (int i = 0; i < maxX; i++)
        {
            for (int j = 0; j < maxY; j++)
            {
                listButton[i, j] = 0;
            }
        }

        for (int i = 0; i < buttonGroup.transform.childCount; i++)
        {
            buttonGroup.transform.GetChild(i).gameObject.GetComponent<Button>().enabled = true;
            if (buttonGroup.transform.GetChild(i).GetChild(0).childCount > 0)
            {
                Debug.Log(buttonGroup.transform.GetChild(i).GetChild(0).GetChild(0).gameObject.name);
                Destroy(buttonGroup.transform.GetChild(i).GetChild(0).GetChild(0).gameObject);
            }
        }

        countGame = 10;
    }

    private void Update()
    {
        if (playerClick)
        {
            time += 0.02f;

            if (controlManager.controlMulti.dataPlayer["check"] == "X")
            {
                checkX.GetComponent<Animator>().enabled = true;
                checkO.GetComponent<Animator>().enabled = false;
            }
            else
            {
                checkX.GetComponent<Animator>().enabled = false;
                checkO.GetComponent<Animator>().enabled = true;
            }

            vs.SetActive(false);
            txtTime.SetActive(true);

            if (time >= 1)
            {
                countGame--;
                txtTime.GetComponent<Text>().text = countGame.ToString();
                time = 0;
            }

            if(countGame <= 0)
            {
                controlManager.controlMulti.OnSendMove(objNewMove);
                boder.SetActive(true);
                playerClick = false;
            }
        }
        else
        {
            checkX.GetComponent<Animator>().enabled = false;
            checkO.GetComponent<Animator>().enabled = false;
            checkX.GetComponent<Image>().color = Color.white;
            checkO.GetComponent<Image>().color = Color.white;
            vs.SetActive(true);
            txtTime.SetActive(false);
            countGame = 10;
            txtTime.GetComponent<Text>().text = countGame.ToString();
        }
    }

    public void OnClick(GameObject obj)
    {
        string[] posObj = obj.name.Split('-');
        int x = int.Parse(posObj[0]);
        int y = int.Parse(posObj[1]);
        listButton[x, y] = int.Parse(controlManager.controlMulti.dataPlayer["id"]); ;

        GameObject otherPlayer = GameObject.Instantiate(playerCheck.gameObject, playerCheck.transform.position, Quaternion.identity) as GameObject;
        otherPlayer.transform.SetParent(obj.transform.GetChild(0).gameObject.transform);
        otherPlayer.transform.localPosition = Vector3.zero;
        otherPlayer.transform.localScale = Vector3.one;
        obj.gameObject.GetComponent<Button>().enabled = false;
        boder.SetActive(true);
        playerClick = false;

        controlManager.controlMulti.OnSendMove(obj);
        CheckButtonInGame(obj, controlManager.controlMulti.dataPlayer);
    }

    public void EnemyMove(Dictionary<string, string> data)
    {
        boder.SetActive(false);
        playerClick = true;

        GameObject obj = buttonGroup.transform.Find(data["position"]).gameObject;
        if (obj != null && data["position"] != "99-99")
        {
            string[] posObj = obj.name.Split('-');
            int x = int.Parse(posObj[0]);
            int y = int.Parse(posObj[1]);

            listButton[x, y] = int.Parse(controlManager.controlMulti.dataEnemy["id"]);

            GameObject otherPlayer = GameObject.Instantiate(enemyCheck.gameObject, playerCheck.transform.position, Quaternion.identity) as GameObject;
            otherPlayer.transform.SetParent(obj.transform.GetChild(0).gameObject.transform);
            otherPlayer.transform.localPosition = Vector3.zero;
            otherPlayer.transform.localScale = Vector3.one;
            obj.gameObject.GetComponent<Button>().enabled = false;
            CheckButtonInGame(obj, controlManager.controlMulti.dataEnemy);
        }
    }

    public void CheckButtonInGame(GameObject obj, Dictionary<string, string> dataUser)
    {
        string[] posObj = obj.name.Split('-');
        int x = int.Parse(posObj[0]);
        int y = int.Parse(posObj[1]);

        Colum(x, y, int.Parse(dataUser["id"]));
        Row(x, y, int.Parse(dataUser["id"]));

        CrossLeft(x, y, int.Parse(dataUser["id"]));
        CrossRight(x, y, int.Parse(dataUser["id"]));
        //Debug.Log(countColum +", "+ countRow + ", "+ countLeft + ", " + countRight);
        if (countColum == 5 || countRow == 5 || countLeft == 5 || countRight == 5 && controlManager.controlMulti.dataPlayer["id"] == dataUser["id"])
        {
            controlManager.controlMulti.WinLoseGame(dataUser);
        }

        countColum = countRow = countLeft = countRight = 1;
    }

    public void Colum(int x, int y, int playerID)
    {
        for (int i = 0; i < x; i++)
        {
            //Debug.Log("Colum 1: " + (x - (i + 1)) + ", " + y + " - " + listButton[x - (i + 1), y] + " == " + playerID);
            if (x - (i + 1) >= 0 && listButton[x - (i + 1), y] == playerID)
            {
                countColum++;
            }
            else
            {
                break;
            }
        }

        for (int i = x; i < maxX; i++)
        {
            // Debug.Log("Colum 2: " + (i + 1) + ", " + y + " - " + listButton[i + 1, y] + " == " + playerID);
            if (i + 1 < maxX && listButton[i + 1, y] == playerID)
            {
                countColum++;
            }
            else
            {
                break;
            }
        }

        //Debug.Log("Colum: " + countTrue);
    }

    public void Row(int x, int y, int playerID)
    {
        for (int i = 0; i < y; i++)
        {
            // Debug.Log("Row 1: " + x + ", " + (y - (i + 1)) + " - " + listButton[x, y - (i + 1)] + " == " + playerID);
            if (y - (i + 1) >= 0 && listButton[x, y - (i + 1)] == playerID)
            {
                countRow++;
            }
            else
            {
                break;
            }
        }

        for (int i = y; i < maxY; i++)
        {
            //  Debug.Log("Row 2: " + x + ", " + (i + 1 < 7) + " - " + listButton[x, i + 1] + " == " + playerID);
            if (i + 1 < maxY && listButton[x, i + 1] == playerID)
            {
                countRow++;
            }
            else
            {
                break;
            }
        }

        //Debug.Log("Row: " + countTrue);
    }

    public void CrossLeft(int x, int y, int playerID)
    {
        bool check_1 = true;
        bool check_2 = true;
        int i = 0;

        while (check_1 == true)
        {
            // Debug.Log("CrossLeft 1: " + (x - (i + 1)) + ", " + (y - (i + 1)));
            if (x - (i + 1) >= 0 && y - (i + 1) >= 0)
            {
                if (listButton[x - (i + 1), y - (i + 1)] == playerID)
                {
                    countLeft++;
                }
                else
                {
                    check_1 = false;
                }
            }
            else
            {
                check_1 = false;
            }
            i++;
        }

        i = 1;

        while (check_2 == true)
        {
            //Debug.Log("CrossLeft 2: " + (x + i) + ", " + (y + i));
            if (x + i < maxX && y + i < maxY)
            {
                if (listButton[x + i, y + i] == playerID)
                {
                    countLeft++;
                }
                else
                {
                    check_2 = false;
                }
            }
            else
            {
                check_2 = false;
            }

            i++;
        }

        //Debug.Log("CrossLeft: " + countTrue);
    }

    public void CrossRight(int x, int y, int playerID)
    {
        bool check_1 = true;
        bool check_2 = true;
        int i = 0;
        while (check_1 == true)
        {
            //Debug.Log("CrossRight 1: " + (x - (i + 1)) + ", " + (y + (i + 1)));
            if (x - (i + 1) >= 0 && y + (i + 1) < maxY)
            {
                if (listButton[x - (i + 1), y + (i + 1)] == playerID)
                {
                    countRight++;
                }
                else
                {
                    check_1 = false;
                }
            }
            else
            {
                check_1 = false;
            }

            i++;

        }

        i = 0;

        while (check_2 == true)
        {
            //Debug.Log("CrossRight 2 " + (x + (i + 1)) + ", " + (y - (i + 1)));
            if (x + (i + 1) < maxX && y - (i + 1) >= 0)
            {
                if (listButton[x + (i + 1), y - (i + 1)] == playerID)
                {
                    countRight++;
                }
                else
                {
                    check_2 = false;
                }
            }
            else
            {
                check_2 = false;
            }

            i++;
        }

        //Debug.Log("CrossRight: " + countTrue);
    }
}
