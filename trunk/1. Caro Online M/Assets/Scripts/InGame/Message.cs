﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Message : MonoBehaviour {

    public Text mess;

    public Button btnMess;

    public delegate void WhateverType(); // declare delegate type

    protected WhateverType callbackFct; // to store the function

    public void CallMessage(string name, WhateverType myCallback)
    {
        mess.text = name;
        callbackFct = myCallback;
    }

    public void OnMessage(string name)
    {
        mess.text = name;
    }
}
