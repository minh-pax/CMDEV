﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetIPGame : MonoBehaviour {

    private string bundUrl;
    public GameObject objControlMul;
    public GameObject objPlayGame;
    public Text ip;

    AssetBundle assetBundle;

    ConfigString_IP configString_IP;

    private void Awake()
    {
        LoadConfig();
    }

    IEnumerator LoadAssetBundle()
    {
#if UNITY_STANDALONE
        bundUrl = "https://www.dropbox.com/s/1jmeonqi617btns/assetbundleconfig?dl=1";
#elif UNITY_ANDROID
        bundUrl = "https://www.dropbox.com/s/iylkgeroh1jr191/assetbundleconfig?dl=1";
#elif UNITY_IOS
        bundUrl = "https://www.dropbox.com/s/a9xinuvn3g8ysqn/assetbundleconfig?dl=1";
#endif

        WWW www = new WWW(bundUrl);
        yield return www;
        assetBundle = www.assetBundle;
        ip.text += "\n LoadAssetBundle";
        LoadConfig();
    }

    public void LoadConfig()
    {
        if (!assetBundle)
        {
            StartCoroutine(LoadAssetBundle());
            return;
        }

        StartCoroutine(ShowIP());
    }

    IEnumerator ShowIP()
    {
        ip.text += "\n ShowIP";
        AssetBundleRequest assetConfig = assetBundle.LoadAssetAsync<Object>("ConfigString_IP");
        yield return assetConfig;
        //TextAsset config = configString;
        TextAsset config = assetConfig.asset as TextAsset;
        configString_IP = new ConfigString_IP();
        configString_IP.Load(config);
        objControlMul.SetActive(true);
        StartCoroutine(ShowPlayButton());
    }

    IEnumerator ShowPlayButton()
    {
        yield return new WaitForSeconds(2f);
        objPlayGame.SetActive(true);
    }

    public string ChangeIP(int count)
    {
        string text = "";

        switch (count)
        {
            case 0:
                text = configString_IP.Find_ID(count.ToString()).IP;
                break;
            case 1:
                text = configString_IP.Find_ID(count.ToString()).IP;
                break;
        }
        ip.text += "\n" + configString_IP.Find_ID(count.ToString()).IP;
        return text;
    }
}
