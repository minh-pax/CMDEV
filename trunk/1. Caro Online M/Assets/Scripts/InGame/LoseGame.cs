﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoseGame : MonoBehaviour
{
    public Text txtStatus;

    public void Lose(string playerLose)
    {
        string mess = string.Format("{0}{1}", playerLose, avUIManager.ChangeLanguage("33"));
        txtStatus.text = mess;
    }
}
