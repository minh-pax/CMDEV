﻿using UnityEditor;

public class AssetBundles : Editor {

    [MenuItem("AssetBundle/Build AssetBundle StandaloneWindows")]
    static void BuildAssetBundleWIN()
    {
        BuildPipeline.BuildAssetBundles("AssetBundles StandaloneWindows", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }

    [MenuItem("AssetBundle/Build AssetBundle Android")]
    static void BuildAssetBundleANDROI()
    {
        BuildPipeline.BuildAssetBundles("AssetBundles Android", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    [MenuItem("AssetBundle/Build AssetBundle IOS")]
    static void BuildAssetBundleIOS()
    {
        BuildPipeline.BuildAssetBundles("AssetBundles IOS", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }
}
